# COMPTOIR-srv

(formerly kown as "manivelle")

This is the installation of Comptoir-SRV, part of "Comptoir du Libre" that provides the engine with its API.
(The web interface is provided by the project Comptoir-WEB.)

## Installation

see [Documentation/INSTALL.md](Documentation/INSTALL.md)

## Update composer dependencies

```sh
composer update
```


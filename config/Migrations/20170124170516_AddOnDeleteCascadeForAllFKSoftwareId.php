<?php
use Migrations\AbstractMigration;

class AddOnDeleteCascadeForAllFKSoftwareId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $relationships_softwares_usersTable = $this->table('relationships_softwares_users');

        $relationships_softwares_usersTable->dropForeignKey("software_id");

        $relationships_softwares_usersTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);


        $relationships_softwaresTable = $this->table('relationships_softwares');

        $relationships_softwaresTable->dropForeignKey("software_id");

        $relationships_softwaresTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);


        $reviewsTable = $this->table('reviews');

        $reviewsTable->dropForeignKey("software_id");

        $reviewsTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);


        $softwares_statisticsTable = $this->table('softwares_statistics');

        $softwares_statisticsTable->dropForeignKey("software_id");

        $softwares_statisticsTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);


        $raw_metrics_softwaresTable = $this->table('raw_metrics_softwares');

        $raw_metrics_softwaresTable->dropForeignKey("software_id");

        $raw_metrics_softwaresTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);


        $screenshotsTable = $this->table('screenshots');

        $screenshotsTable->dropForeignKey("software_id");

        $screenshotsTable->addForeignKey('software_id', 'softwares', 'id', [
            'update' => 'CASCADE',
            'delete' => 'CASCADE'
        ]);

        //UPDATE
        $relationships_softwares_usersTable->update();
        $relationships_softwaresTable->update();
        $reviewsTable->update();
        $softwares_statisticsTable->update();
        $raw_metrics_softwaresTable->update();
        $screenshotsTable->update();
    }
}

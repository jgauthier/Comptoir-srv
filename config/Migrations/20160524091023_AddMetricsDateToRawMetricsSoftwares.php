<?php
use Migrations\AbstractMigration;

class AddMetricsDateToRawMetricsSoftwares extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('raw_metrics_softwares');
        $table->addColumn('metrics_date', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}

<?php
use Migrations\AbstractMigration;

class AddActivityToRawMetricsSoftwares extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('raw_metrics_softwares');
        $table->addColumn('commit_activity_one_month', 'float', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('commit_activity_twelve_month', 'float', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}

<?php
use Migrations\AbstractMigration;

class AddDeltaCommitOneMonthToRawMetricsSoftwares extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('raw_metrics_softwares');
        $table->addColumn('delta_commit_one_month', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->update();
    }
}

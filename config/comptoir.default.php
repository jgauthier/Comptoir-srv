<?php

use Cake\Core\Configure;

return [

    'OpenGraph' => [
        'url' => Configure::read('App.fullBaseUrl'),
        'type' => 'website',
        'title' => 'Comptoir du libre',
        'description' => __d("Layout", "opengraph.description"),
        'image' => Configure::read('App.fullBaseUrl') . DS . 'img/logos/Logo-CDL.png',
        'card' => 'summary_large_image'
    ],
    'Mail'=> [
        'Contact' => [
            'UserToUser' => [
                'Prefix' => '[Comptoir du Libre] : '
            ],
        ]
    ],
//    Only use for the index page
    'Categories'=>[
        'PickOfTheMonth'=>[
                27, // Authentik
                49, // Maarch Courrier
                9,  // Asqatasun
                23 // OpenADS
        ]
    ],
//    Some kind of ACL for views
    'ACL' => [
        'Users' => [
            'add' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => false,
            ],
        ],
        'Softwares' => [
            'edit' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'add' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'usersSoftware' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => false,
                'Unknown' => true,
            ],
            'addReview' => [
                'Administration' => true,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
            'servicesProviders' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'deleteUsersSoftware' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'deleteServicesProviders' => [
                'Administration' => true,
                'Association' => true,
                'Person' => true,
                'Company' => true,
                'Unknown' => true,
            ],
            'workswellSoftwares' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
            'alternativeTo' => [
                'Administration' => false,
                'Association' => false,
                'Person' => false,
                'Company' => false,
                'Unknown' => true,
            ],
        ]
    ],
    "Piwik" => <<<EOF

EOF

];

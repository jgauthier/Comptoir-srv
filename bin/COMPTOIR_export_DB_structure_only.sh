#!/usr/bin/env bash

DUMP_PATH=/tmp
TIMESTAMP=$(date +%Y-%m-%d-%Hh%Mm%S)

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Structure_only.sql"

pg_dump  -U comptoir -W --schema-only --create --clean --if-exists  -f ${SAVE_SQL_FILE} comptoir
bzip2 ${SAVE_SQL_FILE}

#!/usr/bin/env bash

DUMP_PATH=/tmp
TIMESTAMP=$(date +%Y-%m-%d-%Hh%Mm%S)
SRV_PATH=/home/comptoir/Comptoir-srv

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"

echo "============================================================================="
echo "Exporting SQL data"
pg_dump -U comptoir -W --data-only -f ${SAVE_SQL_FILE}
bzip2 ${SAVE_SQL_FILE}

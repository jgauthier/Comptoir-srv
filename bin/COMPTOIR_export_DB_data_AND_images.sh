#!/usr/bin/env bash

DUMP_PATH=/tmp
TIMESTAMP=$(date +%Y-%m-%d-%Hh%Mm%S)
SRV_PATH=/home/comptoir/Comptoir-srv

SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"
SAVE_WEBROOT_FILES_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Dir_Files.tar.bz2"

echo "============================================================================="
echo "Exporting SQL data"
pg_dump -U comptoir -W --data-only -f ${SAVE_SQL_FILE}

echo "============================================================================="
echo "Exporting Binary files"
bzip2 ${SAVE_SQL_FILE}
cd ${SRV_PATH}/webroot/img/
tar cvfj ${SAVE_WEBROOT_FILES_FILE} files/

#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d: -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo "usage: ./${0} [OPTIONS]..."
    echo ''
    echo '  -d <comptoir-srv-dir>   (MANDATORY) Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_SRV_DIR=

while true; do
    case "$1" in
        -d ) COMPTOIR_SRV_DIR="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [ "${COMPTOIR_SRV_DIR}" = "" ]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Actual job
# #############################################################################

sudo -u postgres psql -f "${COMPTOIR_SRV_DIR}/config/SQL/COMPTOIR_DB_create_database_and_set_ownsership.sql"

#!/usr/bin/env bash

set -o errexit

# #############################################################################
# Option management
# #############################################################################

TEMP=`getopt -o d:t: -- "$@"`

if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi

usage () {
    echo 'Erase the actual content of Comptoir DB, then import SQL data and binary files.'
    echo ''
    echo 'usage: ./COMPTOIR_import_DB_data_AND_images.sh [OPTIONS]...'
    echo ''
    echo '  -d <comptoir-srv-dir>   (MANDATORY) Absolute directory to Comptoir-srv, *without* trailing slash, eg "/home/comptoir/comptoir-srv"'
    echo '  -t <timestamp>          (MANDATORY) Timestamp of the files to import, eg "2016-06-28-15h06m42"'
    echo ''
    exit 2
}

# Note the double quotes around $TEMP: they are essential!
eval set -- "${TEMP}"

declare COMPTOIR_SRV_DIR=
declare TIMESTAMP=

while true; do
    case "$1" in
        -d ) COMPTOIR_SRV_DIR="$2"; shift 2 ;;
        -t ) TIMESTAMP="$2"; shift 2 ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# check mandatory options
if [ "${COMPTOIR_SRV_DIR}" = "" -o "${TIMESTAMP}" = "" ]
then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# #############################################################################
# Variables
# #############################################################################

DUMP_PATH=/tmp
SAVE_SQL_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Data_only.sql"
SAVE_WEBROOT_FILES_FILE="${DUMP_PATH}/SAVE_COMPTOIR_${TIMESTAMP}_Dir_Files.tar.bz2"
DIR_WEBROOT_FILES_PARENT="${COMPTOIR_SRV_DIR}/webroot/img"
DIR_WEBROOT_FILES="${DIR_WEBROOT_FILES_PARENT}/files"
DIR_WEBROOT_FILES_TMP="${DIR_WEBROOT_FILES_PARENT}/files-tmp"

# #############################################################################
# Actual import
# #############################################################################

# #############################################################################
# Part 1: importing SQL data
echo "Importing SQL data..."
psql -U comptoir -W -f "${COMPTOIR_SRV_DIR}/config/SQL/COMPTOIR_DB_truncate_tables.sql"
if [ -e  "${SAVE_SQL_FILE}.bz2" ]; then
    bunzip2 "${SAVE_SQL_FILE}.bz2"
fi
psql -U comptoir -W -f "${SAVE_SQL_FILE}"

# #############################################################################
# Part 2: importing binary files

echo "Importing files..."
if [ -e "${DIR_WEBROOT_FILES_TMP}" ]; then
    echo "Removing old backup"
    sudo rm -rf "${DIR_WEBROOT_FILES_TMP}"
fi
if [ -e "${DIR_WEBROOT_FILES}" ]; then
    echo "Creating new backup"
    sudo mv "${DIR_WEBROOT_FILES}" "${DIR_WEBROOT_FILES_TMP}"
fi

echo "Extracting files"
cd "${DIR_WEBROOT_FILES_PARENT}"
sudo tar xvfj "${SAVE_WEBROOT_FILES_FILE}"
sudo chown -R www-data.comptoir files/
find "${DIR_WEBROOT_FILES}" -type f -exec sudo chmod 664 {} \;
find "${DIR_WEBROOT_FILES}" -type d -exec sudo chmod 775 {} \;

echo ""
echo "Please, verify in the app everything is OK"
echo "then do: sudo rm -rf \"${DIR_WEBROOT_FILES_TMP}/\""
echo ""

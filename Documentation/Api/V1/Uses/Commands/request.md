# [GET] Request/name

## Objective

Returns all data matching with a user research on multiples forges.

## Ressource url

* `http://my-host/api/v1/request?"`

## Ressource informations

* Response formats : JSON
* Requires authentication? : No
* Rate limited ? : No yet
* limit results : Yes

##  Input

* name : an alphanumeric value for the research;
* order : to order the research. Only `desc` and `asc` values are allowed;
* limit : to limit the results on each forge. `Default 25`;
* offset : give an offset for the research.
    * maxValueForOffset  = numberOfResult - limit;
    * If the offset specified is > to the max value then max value is the new offset.


## Output
The request command returns a json composed of  :
* results : Ordered and sorted array;
* forges : Explicative array about forges;
* meta_datas : Global informations about the research. Like the total count, response status, etc ...  

## Examples

An example using the request action has its minimum :

`http://api/v1/request?name="élection"`

Results :
`"results": [
        {
            \"forge_id": 0,
            "name": "ypokelections",
            "description": "YPOK d\u00e9pose sur adullact.net son module sous licence GPL de mise en ligne des \u00e9lections qui permet l\u2019affichage des r\u00e9sultats de tous types de scrutins, avec des animations Flash dynamiques g\u00e9n\u00e9rant des graphiques en camembert ou en barres.",
            "url": "http:\/\/adullact.net\/projects\/ypokelections\/"
        },
        {
            "forge_id": 1,
            "forge": "GitHub",
            "name": "538model",
            "url": "https:\/\/github.com\/jseabold\/538model",
            "creationDate": "2012-10-22T16:43:39Z",
            "lastUpdatedDate": "2016-01-13T10:34:16Z",
            "description": "538 Election Forecasting Model"
        },
        {
            "forge_id": 0,
            "name": "openelec",
            "description": "openElec permet la gestion compl\u00e8te des \u00e9lections politiques, de l\u0027inscription d\u0027un \u00e9lecteur jusqu\u0027\u00e0 l\u0027\u00e9dition de sa carte \u00e9lectorale, des listes d\u0027\u00e9margement, le transfert \u00e0 l\u0027INSEE et bien plus encore... [http:\/\/www.openmairie.org\/]",
            "url": "http:\/\/adullact.net\/projects\/openelec\/"
        },....
        "forges": [
        {
            "forge_id": 0,
            "forge_name": "Adullact",
            "url_connector": "http:\/\/adullact.net\/",
            "url_connector_api": "http:\/\/adullact.net\/",
            "forge_results_total_count": 5
        },
        {
            "forge_id": 1,
            "forge_name": "GitHub",
            "url_connector": "https:\/\/github.com\/",
            "url_connector_api": "https:\/\/api.github.com\/",
            "forge_results_total_count": 30
        }
    ],
    "meta_datas": {
        "result_total_count": 35,
        "incomplete_results": null
    }
}`
## Error codes

* 400 : for all bad request encountered
    * No use of name parameter
    * Incorrect use of parameter(s)  
    * No respect of the syntax.
* 500
    * Internal server error

## Notes

Nothing to say. If you need some help please contact mickael.pastor@adullact.org

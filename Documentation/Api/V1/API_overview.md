# Summary of the API

/api/v1

## Administration API

Equivalences between CRUD operations & HTTP methods

| HTTP METHOD | CRUD operation |
|:------------|:---------------|
| POST        | CREATE         |
| GET         | READ           |
| PUT         | UPDATE         |
| DELETE      | DELETE         |


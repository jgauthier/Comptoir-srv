# COMPTOIR API: Tags functionality


## Créer un tag

* rules : le name du tag doit être unique. La différence est faites entre les minuscules et les majuscules.

* Validation :  le name du tag = doit être composé des lettres a-z et/ou A-Z et/ou des chiffres 0-9, les caractères – et _ sont aussi autorisés.
 	            Limite du tag à 100 caractères.

## List for tags

* http://comptoir-srv.local/api/v1/tags/
* http://comptoir-srv.local/api/v1/tags/add
* http://comptoir-srv.local/api/v1/tags/edit/8

* On peut éditer un tag en fonction de son id(modifier le name, toujours limiter à 100 caractères et ajouter ou supprimer des logiciels).
	Suppression d’un tag par son id. (Quand un tag est supprimé il n’apparaît plus sur les logiciels auxquels il était associés.)


## List for softwares

* rules :
    Un software ne peut avoir que 5 tags différents.

* Ajout de la fonction tagged dans softwaresController qui permet de lister les logiciels qui ont un même ‘name’ tag.
    A la création et l'édition d'un software, le champ tag est ajouté.

* http://comptoir-srv.local/api/v1/softwares/tagged.json
* http://comptoir-srv.local/api/v1/softwares/edit/29
* http://comptoir-srv.local/api/v1/softwares/add


## List for softwares-tags

* rules :
 	Le couple tag-id et softwar_id doit être unique. Donc un logiciel ne peut avoir qu’une fois le même tag.

* http://comptoir-srv.local/api/v1/softwares-tags/

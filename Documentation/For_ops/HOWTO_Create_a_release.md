# Create a release for Comptoir-srv

## 1. Change version in source code

* Create an issue "prepare vX.Y.Z" and the associated branch + merge request.
* On this branch, change the version in: `config/bootstrap.php`
* Eventually make other adjustments

## 2. Merge and tag Develop

* Merge the branch into develop
* Place an RC tag on develop: `vX.Y.Z-rc.1`

## 3. Test on pre-prod server

* Deploy on http://comptoir1404-srv.dev.adullact.lan (following the UPGRADE instructions)
* If something's missing: repeat from step 1 ; else go next step

## 4. Merge into Master

```sh
git checkout master
git merge develop --no-ff -m "Comptoir-srv vX.Y.Z"
```

## 5. Tag Master

Create the tag:

```sh
git checkout master
MYTAG="vX.Y.Z"
git tag -a $MYTAG -m "$MYTAG"
```

Push the tag and the merge:

```sh
git push origin master
git push origin $MYTAG
```
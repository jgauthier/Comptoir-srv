# COMPTOIR-SRV install

<!-- =============================================================================================================== -->
<!-- ===== Prerequisites =========================================================================================== -->
<!-- =============================================================================================================== -->

## Prerequisites: Ubuntu packages

```shell
apt-get install \
    libicu55 \
    zlib1g \
    git \
    apache2 \
    php \
    php-intl \
    libapache2-mod-php \
    postgresql \
    php-pgsql \
    php7.0-sqlite3 \
    php7.0-xml \
    php7.0-mbstring

```

## Prerequisites: PHP > timezone + max_upload

```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done
```

## Prerequisites: Install Composer

```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## Prerequisites: Remove useless packages

```shell
apt-get purge --auto-remove \
    libicu55 \
    zlib1g
```

## Prerequisites: Install Git-lfs

```shell
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
    sudo apt install git-lfs
    sudo apt autoremove
```

## Postgres authentication

In `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

## Create system user

```shell
useradd -d /home/comptoir/ -m -s /bin/bash comptoir
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## Installation Comptoir **SRV**

Let say:

* we are user `superdupont`,
* we want to have our Comptoir project files into `/home/superdupont/Project/Comptoir-srv/`,
* we can do `git` command as authenticated user (i.e. ssh key is placed on the Gitlab server)

As `superdupont`, do:

```shell
cd /home/superdupont/Project/
git clone git@gitlab.adullact.net:Comptoir/Comptoir-srv.git
```

Now configure code for `comptoir` user:

```shell
su - comptoir
cd /home/comptoir
ln -s /home/superdupont/Project/Comptoir-srv Comptoir-srv
```

## POSTGRESQL Create user

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER comptoir WITH PASSWORD 'comptoir';
```

## POSTGRESQL Create DB and set ownership

As `root` user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_database_and_set_ownership.sh -d /home/comptoir/Comptoir-srv
```

Test it:

```shell
psql -U comptoir -W comptoir
```

## POSTGRESQL Create tables and procedures

As any user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_tables_and_procedures.sh -d /home/comptoir/Comptoir-srv
```

## POSTGRESQL set-up for debug-kit

as user `root` postgresql, do :

```postgresql
CREATE DATABASE debug_kit WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8';
ALTER DATABASE debug_kit OWNER TO comptoir;
```

<!-- =============================================================================================================== -->
<!-- ===== Configuration============================================================================================ -->
<!-- =============================================================================================================== -->

## POSTGRESQL Insert content + binary files

Let say the archived content is stored in the two following files:

* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Data_only.sql.bz2`
* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Dir_Files.tar.bz2`

To import content, run as `root` user:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d /home/comptoir/Comptoir-srv \
    -t 2017-01-10-18h05m44
```

/!\ Caution:

* Files **must** be in `/tmp' folder.
* The `-t` parameter is the **exact** timestamp copied/pasted from the filenames.

## BINARY FILES adjust permissions

Let's assume the current user is "superdupont". We create a "comptoir" group, and add "superdupont" to it.
Then, we set permissions.

```shell
sudo groupadd comptoir
sudo usermod -aG comptoir superdupont
sudo chown -R www-data:comptoir /home/comptoir/Comptoir-srv/webroot/img/files
```
Test it:

```shell
    grep -E "^comptoir:" /etc/group
```
Comptoir's group must contain 2 users : comptoir, superdupont.


## POSTGRESQL sanitize content

We bury emails to avoid mistakes:

```shell
psql -U comptoir -W comptoir -c "update users set email='fake-email@comptoir-du-libre.org';"
```

## Composer install Comptoir **SRV**

```shell
cd /home/comptoir/Comptoir-srv/
/usr/local/bin/composer install
```


## APP.PHP Configure logs for CRUD actions

In `/home/comptoir/Comptoir-srv/config/app.php`, section `Log`, add the "crud" stanza:

```php
/**
 * Configures logging options
 */
    'Log' => [
        'debug' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'debug',
            'levels' => ['notice', 'info', 'debug'],
        ],
        'error' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'error',
            'levels' => ['warning', 'error', 'critical', 'alert', 'emergency'],
        ],
        'crud' => [
            'className' => 'Cake\Log\Engine\FileLog',
            'path' => LOGS,
            'file' => 'crud',
            'scopes' => ['users', 'softwares','reviews','screenshots','relationships_softwares_users'],
            'levels' => ['notice', 'info', 'error'],
        ],
    ],
```

## APP.PHP Define Email Transport

In `/home/comptoir/Comptoir-srv/config/app.php`, section `EmailTransport`, edit the "default" stanza:

```php
'EmailTransport' => [
           'default' => [
               'className' => 'Smtp',
               // The following keys are used in SMTP transports
               'host' => 'smtp.adullact.org',
               'port' => 25,
               'timeout' => 30,
               'username' => 'app.comptoirdulibre@adullact.org',
               'password' => 'my-password',
               'client' => null,
               'tls' => true
           ],
       ],
```

Replace `my-password` with suitable value.

## APP.PHP Configure database credentials

Adjust Postgres credentials in `config/app.php`, search for 'Datasources'
and 'default'. Now adjust:

```php
'default' => [
            'className' => 'Cake\Database\Connection',
            'driver' => 'Cake\Database\Driver\Postgres',
            'persistent' => false,
            'host' => 'localhost',
            //'port' => 'non_standard_port_number',
            'username' => 'comptoir',
            'password' => 'comptoir',
            'database' => 'comptoir',
            'encoding' => 'utf8',
            'timezone' => 'Europe/Paris',
            'cacheMetadata' => true,
            'log' => false,
```
/!\ You should comment the 'test' section of Datasources.


## APP.PHP Configure debug-kit database credentials

After the 'test' commented, add datasource for debug-kit

```php
    'debug_kit' => [
                'className' => 'Cake\Database\Connection',
                'driver' => 'Cake\Database\Driver\Postgres',
                'persistent' => false,
                'host' => 'localhost',
                'username' => 'comptoir',
                'password' => 'comptoir',
                'database' => 'debug_kit',
                'encoding' => 'utf8',
                'timezone' => 'Europe/Paris',
                'cacheMetadata' => true,
                'quoteIdentifiers' => false,
            ],
```


## COMPTOIR.PHP Configure 

Copy default parameters of `comptoir.default.php` into `comptoir.php`
```shell
cd /home/comptoir/Comptoir-srv
cp config/comptoir.default.php config/comptoir.php
```

### Categories.PickOfTheMonth

In `comptoir.php`, the array `Categories => PickOfTheMonth` contain the four ids of softwares you want see in the section `Pick of the month` on the home page.

Default ids [   27, // Authentik
                49, // Maarch Courrier
                9,  // Asqatasun
                23  // OpenADS ]
                
                
## Load Debug kit for Comptoir **SRV**

As user `superdupont`, do:

```shell
bin/cake plugin load DebugKit
```

                
## Set UNIX permissions

As user `root` do:

```shell
cd /home/comptoir
chmod -R o-w Comptoir-srv/*
for i in webroot logs tmp; do
    chown -R www-data.comptoir "Comptoir-srv/${i}"
    find "Comptoir-srv/$i" -type d -exec sudo chmod 775 {} \;
    find "Comptoir-srv/$i" -type f -exec sudo chmod 664 {} \;
done
```

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost preparation

As `root`, do:

```
echo "127.0.0.1	comptoir-srv.local" >> /etc/hosts
```

## APACHE Vhost creation "comptoir-srv"

Create the file `/etc/apache2/sites-available/comptoir-srv.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName comptoir-srv.local
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-srv/webroot/

    ErrorLog ${APACHE_LOG_DIR}/comptoir-srv.local.log
    CustomLog ${APACHE_LOG_DIR}/comptoir-srv.local.log combined

    <Directory "/home/comptoir/Comptoir-srv/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite comptoir-srv.conf
service apache2 reload
```

## 5. Test

* Comptoir-SRV with: [http://comptoir-srv.local/api/v1/softwares.json](http://comptoir-srv.local/api/v1/softwares.json)

## 7. Functionnal Testing

* Cf: [Functionals tests](FunctionalsTests/Init.md)

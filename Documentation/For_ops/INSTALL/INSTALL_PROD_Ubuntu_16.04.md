# COMPTOIR-SRV install

<!-- =============================================================================================================== -->
<!-- ===== Prerequisites =========================================================================================== -->
<!-- =============================================================================================================== -->

## Prerequisites: Ubuntu packages

As `root` user, do:

```shell
apt-get install \
    libicu55 \
    zlib1g \
    unzip \
    git \
    apache2 \
    php \
    php-intl \
    libapache2-mod-php \
    postgresql \
    php-pgsql \
    php-mbstring
```

## Prerequisites: PHP > timezone + max_upload

As `root` user, do:

```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php/7.0/${i}/conf.d/comptoir_du_libre.ini
done
```

## Prerequisites: Install Composer

As `root` user, do:

```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## Prerequisites: Remove useless packages

As `root` user, do:

```shell
apt-get purge --auto-remove \
    libicu \
    zlib1g
```

## Prerequisites: configure locale en_US and fr_FR

As `root` user, do:

```shell
dpkg-reconfigure locales
```

...and select the following locales:

* `fr_FR.UTF8`
* `en_US.UTF8`

## Postgres authentication

As `root` user, in `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

## Create system user

As `root` user, do:

```shell
useradd -d /home/comptoir/ -m -s /bin/bash comptoir
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## Installation Comptoir **SRV** 

As `root` user, do:

```shell
su - comptoir
cd /home/comptoir/
git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git
cd Comptoir-srv && git checkout v1.0.0
cd ..
cd Comptoir-web && git checkout v1.0.0
```

## POSTGRESQL Create user

As `root` user, do:

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER comptoir WITH PASSWORD 'comptoir';
```

## POSTGRESQL Create DB and set ownership

As `root` user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_database_and_set_ownership.sh -d /home/comptoir/Comptoir-srv
```

Test it:

```shell
psql -U comptoir -W comptoir
```

## POSTGRESQL Create tables and procedures

As any user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_tables_and_procedures.sh -d /home/comptoir/Comptoir-srv
```

<!-- =============================================================================================================== -->
<!-- ===== Configuration============================================================================================ -->
<!-- =============================================================================================================== -->

## POSTGRESQL Insert content + binary files

Let say the archived content is stored in the two following files:

* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Data_only.sql.bz2`
* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Dir_Files.tar.bz2`

To import content, run as `root` user:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d /home/comptoir/Comptoir-srv \
    -t 2017-01-10-18h05m44
```

/!\ Caution:

* Files **must** be in `/tmp' folder.
* The `-t` parameter is the **exact** timestamp copied/pasted from the filenames.


## Comptoir **SRV**: Composer install

```shell
cd /home/comptoir/Comptoir-srv/ \
&& /usr/local/bin/composer install --no-dev
```

## Comptoir **SRV**: app.php

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/ \
&& sed -i "s/'debug' => true/'debug' => false/" config/app.php
```

Then edit `config/app.php` and set the values for:

* Section `EmailTransport`: `password`
* Section `Datasource/Default`: DB credential

## Comptoir **SRV**: comptoir.php

```shell
cd /home/comptoir/Comptoir-srv/ \
&& cp config/comptoir.default.php config/comptoir.php \
```

Then edit `config/comptoir.php` and set the values for:

* the Piwik stanza (or any analytics HTML code)

### Categories.PickOfTheMonth

The array `Categories => PickOfTheMonth` contain the four ids of softwares you want see in the section `Pick of the month` on the home page.

Default ids [   27, // Authentik
                49, // Maarch Courrier
                9,  // Asqatasun
                23  // OpenADS ]

## Set UNIX permissions

As user **root** do:

```shell
cd /home/comptoir/
for j in Comptoir-srv; do
    chmod -R o-w "${j}"
    for i in webroot logs tmp; do
        chown -R www-data.comptoir "${j}/${i}"
        find "${j}/${i}" -type d -exec sudo chmod 775 {} \;
        find "${j}/${i}" -type f -exec sudo chmod 664 {} \;
    done
done
```

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost creation "comptoir-srv"

Let say want to have Comptoir-SRV available at URL `srv.comptoir-du-libre.org`

Create the file `/etc/apache2/sites-available/comptoir-srv.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName srv.comptoir-du-libre.org
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-srv/webroot/

    ErrorLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log
    CustomLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log combined

    <Directory "/home/comptoir/Comptoir-srv/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite srv.comptoir-du-libre.org comptoir-du-libre.org
service apache2 reload
```

## 5. Test

* Comptoir-SRV with: [http://srv.comptoir-du-libre/api/v1/softwares.json](http://srv.comptoir-du-libre/api/v1/softwares.json)

## 6. Functionnal Testing

@@@TODO

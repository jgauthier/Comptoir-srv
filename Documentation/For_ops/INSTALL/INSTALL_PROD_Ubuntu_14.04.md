# COMPTOIR-SRV install

<!-- =============================================================================================================== -->
<!-- ===== Prerequisites =========================================================================================== -->
<!-- =============================================================================================================== -->

## Prerequisites: configure locale en_US and fr_FR

As `root` user, do:

```shell
locale-gen "en_US.UTF-8" "fr_FR.UTF-8"
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
```

And **do not forget** to logout / re-login

## Prerequisites: Ubuntu packages

As `root` user, do:

```shell
apt-get install \
    libicu52 \
    libicu-dev \
    zlib1g \
    zlib1g-dev \
    unzip \
    git \
    apache2 \
    php5 \
    php5-intl \
    libapache2-mod-php5
```

### Prerequisites: Postgres from Postgres PPA

```shell
sudo echo "deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main" >/etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
    sudo apt-key add -
sudo apt-get update
apt-get install \
    postgresql-9.5 \
    php5-pgsql
```

## Prerequisites: PHP > timezone + max_upload

As `root` user, do:

```shell
for i in apache2 cli; do
    echo "date.timezone = \"Europe/Paris\"" >> /etc/php5/${i}/conf.d/comptoir_du_libre.ini
    echo "upload_max_filesize = 2M" >> /etc/php5/${i}/conf.d/comptoir_du_libre.ini
done
```

## Prerequisites: Install Composer

As `root` user, do:

```shell
php -r "readfile('https://getcomposer.org/installer');" \
    | php -- --install-dir=/usr/local/bin --filename=composer
```

## Postgres authentication

As `root` user, in `/etc/postgresql/9.5/main/pg_hba.conf`, replace

```
       local   all             all                                     peer
```

by

```
       local   all             all                                     password
```

and restart Postgres

```shell
sudo service postgresql restart
```

## Create system user

As `root` user, do:

```shell
useradd -d /home/comptoir/ -m -s /bin/bash comptoir
```

<!-- =============================================================================================================== -->
<!-- ===== Installation ============================================================================================ -->
<!-- =============================================================================================================== -->

## Installation Comptoir **SRV** and **WEB**

As `root` user, do:

```shell
su - comptoir
cd /home/comptoir/
git clone https://gitlab.adullact.net/Comptoir/Comptoir-srv.git
git clone https://gitlab.adullact.net/Comptoir/Comptoir-web.git
cd Comptoir-srv && git checkout v1.0.1
cd ..
cd Comptoir-web && git checkout v1.0.1
```

## POSTGRESQL Create user

As `root` user, do:

```shell
sudo -u postgres psql
```

Adjust password to your needs:

```postgresql
CREATE USER comptoir WITH PASSWORD 'comptoir';
```

## POSTGRESQL Create DB and set ownership

As `root` user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_database_and_set_ownership.sh -d /home/comptoir/Comptoir-srv
```

Test it:

```shell
psql -U comptoir -W comptoir
```

## POSTGRESQL Create tables and procedures

As any user, do:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_create_DB_tables_and_procedures.sh -d /home/comptoir/Comptoir-srv
```

<!-- =============================================================================================================== -->
<!-- ===== Configuration============================================================================================ -->
<!-- =============================================================================================================== -->

## POSTGRESQL Insert content + binary files

Let say the archived content is stored in the two following files:

* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Data_only.sql.bz2`
* `/tmp/SAVE_COMPTOIR_2017-01-10-18h05m44_Dir_Files.tar.bz2`

To import content, run as `root` user:

```shell
/home/comptoir/Comptoir-srv/bin/COMPTOIR_import_DB_data_AND_images.sh \
    -d /home/comptoir/Comptoir-srv \
    -t 2017-01-10-18h05m44
```

/!\ Caution:

* Files **must** be in `/tmp' folder.
* The `-t` parameter is the **exact** timestamp copied/pasted from the filenames.

## Comptoir **SRV**: paths.php

As user `comptoir`, edit `config/paths.php` and set the values for:

* `COMPTOIR_WEB_HOST` (Warning: this is the *WEB* that is needed, not the *SRV* host)

## Comptoir **SRV**: Composer install

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/ \
&& /usr/local/bin/composer install --no-dev
```

## Comptoir **SRV**: app.php

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-srv/ \
&& sed -i "s/'debug' => true/'debug' => false/" config/app.php
```

Then edit `config/app.php` and set the values for:

* Section `EmailTransport`: `password`
* Section `Datasource/Default`: DB credentials

## Comptoir **WEB**: app.php + paths.php

As user `comptoir`, do:

```shell
cd /home/comptoir/Comptoir-web/ \
&& cp config/app.default.php config/app.php \
&& sed -i "s/'debug' => true/'debug' => false/" config/app.php \
&& cp config/paths.default.php config/paths.php \
&& cp config/comptoir.default.php config/comptoir.php
```

## Comptoir **WEB**: comptoir.php

As user `comptoir`, edit `config/comptoir.php` and set the values for:

* the Piwik stanza (or any analytics HTML code)

## Comptoir **WEB**: Composer install

```shell
cd /home/comptoir/Comptoir-web/ \
&& /usr/local/bin/composer install --no-dev
```

## Set UNIX permissions

As user **root** do:

```shell
cd /home/comptoir/
for j in Comptoir-srv Comptoir-web; do
    chmod -R o-w "${j}"
    for i in webroot logs tmp; do
        chown -R www-data.comptoir "${j}/${i}"
        find "${j}/${i}" -type d -exec sudo chmod 775 {} \;
        find "${j}/${i}" -type f -exec sudo chmod 664 {} \;
    done
done
```

<!-- =============================================================================================================== -->
<!-- ===== Virtual hosts =========================================================================================== -->
<!-- =============================================================================================================== -->

## APACHE Vhost creation "comptoir-srv"

Let say want to have Comptoir-SRV available at URL `srv.comptoir-du-libre.org`

Create the file `/etc/apache2/sites-available/comptoir-srv.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName srv.comptoir-du-libre.org
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-srv/webroot/

    ErrorLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log
    CustomLog ${APACHE_LOG_DIR}/srv.comptoir-du-libre.org.log combined

    <Directory "/home/comptoir/Comptoir-srv/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## Configure Comptoir-SRV hostname for Comptoir-web

```shell
cd /home/comptoir/Comptoir-web/config/
cp paths.default.php paths.php
```

Edit `paths.php` and, at the very end of the file, adjust values:

* `COMPTOIR_SRV_HOST`
* `COMPTOIR_SRV_PATH`

## APACHE Vhost creation "comptoir-web"

Let say want to have Comptoir-SRV available at URL `comptoir-du-libre.org`

Create the file `/etc/apache2/sites-available/comptoir-web.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName comptoir-du-libre.org
    ServerAlias www.comptoir-du-libre.org
    
    ServerAdmin webmaster@localhost
    DocumentRoot /home/comptoir/Comptoir-web/webroot/

    ErrorLog ${APACHE_LOG_DIR}/comptoir-du-libre.org.log
    CustomLog ${APACHE_LOG_DIR}/comptoir-du-libre.org.log combined

    <Directory "/home/comptoir/Comptoir-web/webroot/">
        AllowOverride All
    </Directory>

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

## APACHE enable mod_rewrite and new vhosts

As `root` user, do:

```shell
a2enmod rewrite
a2ensite comptoir-srv comptoir-web
service apache2 reload
```

## 5. Test

* Comptoir-SRV with: [http://srv.comptoir-du-libre/api/v1/softwares.json](http://srv.comptoir-du-libre/api/v1/softwares.json)
* Comptoir-WEB with: [http://www.comptoir-du-libre.org/](http://www.comptoir-du-libre.org/)

## 6. Functionnal Testing

@@@TODO

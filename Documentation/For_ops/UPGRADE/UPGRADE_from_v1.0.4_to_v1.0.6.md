# How to upgrade from v1.0.4 to v1.0.6

## Remove temporary i18n files

```shell
sudo rm -f \
    /home/comptoir/Comptoir-srv/tmp/cache/persistent/* \
    /home/comptoir/Comptoir-web/tmp/cache/persistent/*
```
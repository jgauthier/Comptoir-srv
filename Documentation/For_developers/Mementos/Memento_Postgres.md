# Memento Postgresql

## Connecting to db

```
psql -W -U my-user my-db
```

## Se connecter en admin

* You need to be connected as root

```shell
sudo -u postgres psql
```

* `-U <username>` (followed by the username)
* `-W` forces password to be asked.
* `-d <database-name>` to include which database to access.

## Create a user and give him the necessary privileges

```postgresql
CREATE USER username WITH PASSWORD 'myPassword'; 
CREATE DATABASE comptoir; 
GRANT ALL PRIVILEGES ON DATABASE comptoir to username; 
ALTER USER username WITH SUPERUSER;
```

# Execute SQL commands from a file

```shell
psql -W -U my-user -f my-file.sql my-db
```

## Export database

```shell
pg_dump -U username -f mydb.sql
```

## Import database

```shell
psql -U username -d database_name -f myfile.sql
```

## list all tables in all dbs

```postgresql
\dt *.*
```

## list all databases

```postgresql
\l
```

## list all tables

```postgresql
\dt
```

## list all functions

```postgresql
\df
```

## Change owner of a database

```postgresql
ALTER DATABASE comptoir OWNER TO comptoir;
```

## Supprimer une base de donnée

```postgresql
DROP DATABASE comptoir;
```

Beaucoup plus de choses sur http://www.thegeekstuff.com/2009/04/15-practical-postgresql-database-adminstration-commands/

# Init functionals tests 

## Prerequisites: Comptoir-srv and Comptoir-web

* cf [INSTALL_DEV_UBUNTU](../INSTALL_DEV_Ubuntu_16.04.md)
    * Clean the database
    * Use the following database dump [databaseData](../../config/SQL/COMPTOIR_DB_For_Tests.sql)
    * Use the following files [files.tar.gz](../../tests/TestFiles/FunctionalsTests/files.tar.gz)

## Functional Tests

* Install the Selenium IDE firefox plugin thanks to this [link](https://addons.mozilla.org/fr/firefox/addon/selenium-ide/)
* Run Selenium IDE 
* At first open and run the test [Users](../../../Comptoir-web/tests/FunctionalsTests/Users)
* [SignIn](../../../Comptoir-web/tests/FunctionalsTests/SignIn)
* [EditProfile](../../../Comptoir-web/tests/FunctionalsTests/EditProfile)
* [Search](../../../Comptoir-web/tests/FunctionalsTests/Search)
* [Softwares](../../../Comptoir-web/tests/FunctionalsTests/Software)
* [Relationships](../../../Comptoir-web/tests/FunctionalsTests/Relationships)

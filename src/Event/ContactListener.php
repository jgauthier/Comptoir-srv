<?php
namespace App\Event;

use Cake\Event\Event;

use Cake\Event\EventListenerInterface;

use Cake\Mailer\MailerAwareTrait;


class ContactListener implements EventListenerInterface
{
    use MailerAwareTrait;


    public function implementedEvents()
    {
        return
            [
                'Model.User.contact' => 'contact',
            ];
    }

    public function contact(Event $event, $entity, $options)
    {
        $this->getMailer('User')->send('contactViaForm', [$event]);
    }
}

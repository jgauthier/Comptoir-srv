<?php if ( $this->request->param('controller') !== 'Users' || ($this->request->param('controller') === 'Users' && $this->request->param('action') === 'providers')  ): ?>
    <li class="">
    <?= $this->Html->link(__d("ElementNavigation", "All users"),
        ["prefix"=>false,'controller' => 'Users', 'action' => 'index'],
        [
            'escape' => false,
            "role" => "button",
        ]
    ) ?>
    <?php elseif ($this->request->param('controller') === 'Users' &&  $this->request->param('action') !== 'providers'): ?>
    <li class="current">
        <span class=" text-center"><?= __d("ElementNavigation", "All users") ?></span>
    <?php endif; ?>

</li>

<div class="navbar">
    <div class="container-fluid">
        <div class="row">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <?php if ($this->isHere("pages", "index")): ?>
                    <span class="navbar-brand cdl-navbar-logo ">
                    <?= "<h1 class = 'cdl-navbar-logo'> ". $this->Html->image(
                        "logos/Logo-CDL.png",
                        ["alt" => __d("ElementNavigation", "Comptoir du libre")]) . "</h1>"?>
                    </span>
                <?php elseif ($this->request->here != $this->request->base): ?>
                    <span class="navbar-brand cdl-navbar-logo-link">
                    <?=  $this->Html->link(
                        $this->Html->image("logos/Logo-CDL.png",
                            ["alt" => __d("ElementNavigation", "Comptoir du libre - back home")]),
                        ["prefix"=>false, "controller" => "Pages", 'action'=>'index'],
                        ['escape' => false, "class" => "cdl-navbar-logo-link"]) ?>
                    </span>

                <?php endif; ?>
            </div>

            <!-- /!\ without id = no button working -->
            <div id="navbar" class="navbar-collapse collapse">
                <nav role = "navigation" class="row">

                    <?php // ===== dropdown menu ======================================================================== ?>
                    <ul class="nav navbar-nav navbar-left nav-pull-down">
                        <?php if ($this->isHere("Softwares")): ?>
                    <li class="current">
                    <span class=" text-center"><?= __d("ElementNavigation", "All softwares") ?></span>
                    <?php elseif (!$this->isHere("Softwares")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "All softwares"),
                                ['prefix'=>false, 'controller'=>'softwares'],
                                ['escape' => false]) ?>
                            <?php endif; ?>
                        </li>

                        <?= $this->element("Navigation/navbarUser") ?>

                        <?php if ($this->isHere("Users", "providers")): ?>

                        <li class="current">
                            <span class=" text-center"><?= __d("ElementNavigation", "Services providers") ?></span>
                            <?php elseif (!$this->isHere("Users", "providers")): ?>
                        <li class="">
                            <?= $this->Html->link(__d("ElementNavigation", "Services providers"),
                                ['prefix'=>false,'controller'=>'users','action'=>'providers'],
                                ['escape' => false]) ?>
                            <?php endif; ?>
                        </li>

                    </ul>

                    <?php // ===== lang + auth + search ================================================================= ?>
                    <?php if (!$this->request->session()->read('Auth.User.username')) : ?>
                    <ul class = " nav navbar-nav navbar-right nav-pull-down-right">
                        <li>
                            <?= $this->element("Pages/SearchForm") ?>
                        </li>
                        <li>
                            <?= $this->Html->link(__d("Layout", "Sign up"),
                                ["prefix"=>false,'controller' => 'Users', 'action' => 'add'],
                                [
//                                'class'=>"btn btn-default",
                                    'escape' => false]) ?>
                        </li>
                        <li class="clean-nav">
                            <?= $this->Html->link(__d("Layout", "Sign in"),
                                ["prefix"=>false,'controller' => 'Users', 'action' => 'login'],
                                [
//                                'class'=>"btn btn-default",
                                    'escape' => false]) ?>
                        </li>
                        <?php elseif ($this->request->session()->read('Auth.User.username')) : ?>
                        <ul class = "nav navbar-nav navbar-right nav-pull-down-right">
                            <li>
                                <?= $this->element("Pages/SearchForm") ?>
                            </li>
                            <li class="clean-nav">
                                <?= $this->Html->link($this->request->session()->read('Auth.User.username').'<span class ="caret"></span>',
                                    ["prefix"=>false,'controller' => 'Users', 'action' => $this->request->session()->read('Auth.User.id')],
                                    [
//                                    'class'=>"btn btn-default",
                                        'escape' => false]) ?>
                                <ul class = "">
                                    <li class = "">
                                        <?= $this->Html->link(
                                            __d("Layout","Profile setting"),
                                            ["prefix"=>false,'controller' => 'Users', 'action' => 'edit',$this->request->session()->read('Auth.User.id')],
                                            ['class'=>"",'escape' => false]) ?>
                                    </li>
                                    <li class = "__item">
                                        <?= $this->Html->link(__d("Layout", "Logout"),
                                            ["prefix"=>false,'controller' => 'Users', 'action' => 'logout'],
                                            ['class'=>"",'escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                            <?php endif; ?>
                        </ul>

                </nav>
            </div>
        </div>
        </div>
</div>

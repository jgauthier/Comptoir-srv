<?php

/* * *
 * This template merge all templates containing metrics about a software
 * RISCOSS
 * QSOS
 * OPENHUB
 * Others like a custom one
 */
?>

    <div class="metric-score align">
        <?php
        /**
         * If metrics are in the database
         */
        ?>

        <h2>
            <?= __d("Softwares", "Metrics for {0}", $software->softwarename) ?>
        </h2>
            <?= $this->Score->note(isset($software->softwares_statistic->score) ? $software->softwares_statistic->score : null, ['title' => __d("Softwares", "The score is {0}", isset($software->softwares_statistic->score) ? round($software->softwares_statistic->score, 0) : "NA")]) ?>
    </div>

<?php //if (isset($software) && !empty($software->raw_metrics_softwares) && !empty($software->softwares_statistic) && isset($software->softwares_statistic->score)): ?>
<?php if (!empty($software->raw_metrics_softwares)): ?>
    <p>
        <?= __d("Softwares", "Last calculation of the score on {0}", $this->Time->format(strtotime($software->raw_metrics_softwares[0]->modified), [\IntlDateFormatter::SHORT, -1])) ?>
    </p>
<?php else: ?>
    <p>
        <?= __d("Softwares", "The mark was not calculated."); ?>
    </p>
    <p>
        <?= __d("Softwares", "Possible reason:"); ?>
    </p>
    <ul class="">
        <li>
            <?= __d("Softwares", "An incorrect link for the repository was given."); ?>
        </li>
        <li>
            <?= __d("Softwares", "The score has not been calculated, yet."); ?>
        </li>
    </ul>
<?php endif; ?>

    <div class="expandCollapse">
        <div>
            <h3>
                <?php echo __d("Softwares", "Automated metrics"); ?>
            </h3>
            <table class="table table-hover table-bordered panel">
                <thead>
                <tr>
                    <th scope="col">
                        <?php echo __d("Softwares", "Indicator"); ?>
                    </th>
                    <th scope="col" class="datacellnumber">
                        <?php echo __d("Softwares", "Raw value"); ?>
                    </th>
                    <th scope="col" class="datacellnumber">
                        <?php echo __d("Softwares", "Points"); ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <?php echo __d("Softwares", "project_age") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php
                            $age = "";
                            if(isset($software->softwares_statistic->score)) {
                                if ($software->raw_metrics_softwares[0]->project_age / 365 >= 1 ){
                                    $year = (int)($software->raw_metrics_softwares[0]->project_age / 365) > 1 ? " ans " : " an";
                                    $day = ($software->raw_metrics_softwares[0]->project_age % 365) > 1 ? " jours " : " jour";
                                    $age = (int)($software->raw_metrics_softwares[0]->project_age / 365) . $year
                                        . ($software->raw_metrics_softwares[0]->project_age % 365) . $day;
                                }else{
                                    $day = ($software->raw_metrics_softwares[0]->project_age % 365) > 1 ? " jours " : " jour";
                                    $age = ($software->raw_metrics_softwares[0]->last_commit_age % 365) . $day;
                                }
                            }
                            echo (!isset($software->softwares_statistic->score)) ? "NA" : $age;
                        ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->project_age; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo __d("Softwares", "last_commit_age") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php
                            $age = "";
                            if(isset($software->softwares_statistic->score)) {
                                if ($software->raw_metrics_softwares[0]->last_commit_age / 365 >= 1 ){
                                    $year = (int)($software->raw_metrics_softwares[0]->last_commit_age / 365) > 1 ? " ans " : " an";
                                    $day = ($software->raw_metrics_softwares[0]->last_commit_age % 365) > 1 ? " jours " : " jour";
                                    $age = (int)($software->raw_metrics_softwares[0]->last_commit_age / 365) . $year
                                        . ($software->raw_metrics_softwares[0]->last_commit_age % 365) . $day;
                                }else{
                                    $day = ($software->raw_metrics_softwares[0]->last_commit_age % 365) > 1 ? " jours " : " jour";
                                    $age = ($software->raw_metrics_softwares[0]->last_commit_age % 365) . $day;
                                }
                            }
                            echo (!isset($software->softwares_statistic->score)) ? "NA" : $age;
                        ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->last_commit_age; ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php echo __d("Softwares", "high_committer_percent") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : round($software->raw_metrics_softwares[0]->high_committer_percent,0); ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->high_committer_percent; ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php echo __d("Softwares", "number_of_contributors") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->raw_metrics_softwares[0]->number_of_contributors; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->number_of_contributors; ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php echo __d("Softwares", "delta_commit_one_month") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : round($software->raw_metrics_softwares[0]->delta_commit_one_month,0); ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->delta_commit_one_month; ?>
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php echo __d("Softwares", "delta_commit_twelve_month") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->raw_metrics_softwares[0]->delta_commit_twelve_month; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->delta_commit_twelve_month; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div>
            <h3>
                <?php echo __d("Softwares", "Usage metrics"); ?>
            </h3>
            <table class="table table-hover table-bordered panel">
                <thead>
                <tr>
                    <th scope="col">
                        <?php echo __d("Softwares", "Indicator"); ?>
                    </th>
                    <th scope="col" class="datacellnumber">
                        <?php echo __d("Softwares", "Raw value"); ?>
                    </th>
                    <th scope="col" class="datacellnumber">
                        <?php echo __d("Softwares", "Points"); ?>
                    </th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>
                        <?php echo __d("Softwares", "declared_users") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->raw_metrics_softwares[0]->declared_users; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->declared_users; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo __d("Softwares", "average_review_score") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->raw_metrics_softwares[0]->average_review_score+0; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->average_review_score; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo __d("Softwares", "screenshots") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->raw_metrics_softwares[0]->screenshots; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->screenshots; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo __d("Softwares", "code_gouv_label") ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score) || $software->raw_metrics_softwares[0]->code_gouv_label == null) ? "NA" : $software->raw_metrics_softwares[0]->code_gouv_label; ?>
                    </td>
                    <td class="datacellnumber">
                        <?php echo (!isset($software->softwares_statistic->score)) ? "NA" : $software->softwares_statistic->code_gouv_label; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div>
            <?php if (isset($software) && !empty($software->raw_metrics_softwares) && !empty($software->softwares_statistic) && isset($software->softwares_statistic->score)): ?>
            <h3>
                <?= __d("Softwares", "Score calculation"); ?>
            </h3>
            <div class="panel">
                <p>
                    <?php
                    $total = 0;
                    $total = $software->softwares_statistic->last_commit_age + $software->softwares_statistic->project_age
                        + $software->softwares_statistic->delta_commit_one_month + $software->softwares_statistic->delta_commit_twelve_month
                        + $software->softwares_statistic->number_of_contributors + $software->softwares_statistic->high_committer_percent
                        + $software->softwares_statistic->declared_users + $software->softwares_statistic->average_review_score
                        + $software->softwares_statistic->screenshots + $software->softwares_statistic->code_gouv_label;
                    echo __d("Softwares", "The formula for the score is:") . "<br />";
                    echo "( " . $software->softwares_statistic->last_commit_age . " + " . $software->softwares_statistic->project_age . " + " .
                        $software->softwares_statistic->delta_commit_one_month . " + " . $software->softwares_statistic->delta_commit_twelve_month . " + " .
                        $software->softwares_statistic->number_of_contributors . " + " . $software->softwares_statistic->high_committer_percent . " + " .
                        $software->softwares_statistic->declared_users . " + " . $software->softwares_statistic->average_review_score . " + " .
                        $software->softwares_statistic->screenshots . " + " . $software->softwares_statistic->code_gouv_label;
                    echo " )&nbsp;/&nbsp;28 = " . $total . "&nbsp;/&nbsp;28 = ";
                    ?>

            <span>
                <?= $this->Score->prefix(["class" => "", "tag" => "span", "text" => ""]) ?>
                <?= $this->Score->note(isset($software->softwares_statistic->score) ? $software->softwares_statistic->score : null, ['title' => __d("Softwares", "Score: {0} %", round($software->softwares_statistic->score, 0))]) ?>
                <?= $this->Score->suffix(["template" => "suffix", "class" => "score_percent", "tag" => "span", "text" => "%"]) ?>
            </span>
                </p>
                <?php endif; ?>

                <?= $this->Html->link(__d("Softwares", "How to calculate the mark"), ['controller' => 'Softwares', 'action' => 'formula'], ['escape' => false]) ?>

            </div>
        </div>
    </div>

<?= $this->Html->script('accordion.js', ['async' => true]) ?>
<?= $this->Form->create(null, ['class' => "form-inline", 'title' => __d("Forms", "Search.Users.Title") ,'type' => 'get']) ?>

<?php

echo '<div class="input-group">';
echo '<label class = "control-label" for="isUserOf">' . __d('Forms', "Search.Label.Users.isUserOf") . '</label>';
echo $this->Form->select('isUserOf', $isUserOf, ["name" => "isUserOf", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="hadReview">' . __d('Forms', "Search.Label.Users.hadReview") . '</label>';
echo $this->Form->select('hadReview', $hadReview, ['name'=>'hadReview',"class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="IsSerivicesProvider">' . __d('Forms', "Search.Label.Users.IsSerivicesProvider") . '</label>';
echo $this->Form->select('IsSerivicesProvider', $isSerivicesProvider, ["name" => "IsSerivicesProvider", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="user_type">' . __d('Forms', "Search.Label.Users.userType") . '</label>';
echo $this->Form->select('user_type', $userType, ["name"=>'userType',"class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="order">' . __d('Forms', "Search.Label.Sort") . '</label>';
echo $this->Form->select('order', $order, ["class" => 'form-control']);
echo '</div>';

?>

<div class="input-group">
    <?= $this->Form->input('search', [
        'title' => 'search',
        'label' => false,
        "type" => "text",
        "class" => "hidden"]) ?>
</div>

<div class="form-group">
    <?= $this->Form->button(__d("ElementNavigation", "Filter"), ['class' => 'btn btn-default submit-form filter']) ?>
</div>

<?= $this->Form->end() ?>


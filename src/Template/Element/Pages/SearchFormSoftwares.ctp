<?= $this->Form->create(null, ['class' => "form-inline", 'title' => __d("Forms", "Search for softwares and users"),'method'=>"GET"]) ?>

<?php
echo '<div class="input-group">';
echo '<label class = "control-label" for="reviewed">' . __d('Forms', "Search.Label.Review") . '</label>';
echo $this->Form->select('reviewed', $reviewed, ["class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="screenCaptured">' . __d('Forms', "Search.Label.Screenshot") . '</label>';
echo $this->Form->select('screenCaptured', $screenCaptured, ["class" => 'form-control']);
echo '</div>';
echo '<div class="input-group">';
echo '<label class = "control-label" for="hasUser">' . __d('Forms', "Search.Label.User") . '</label>';
echo $this->Form->select('hasUser', $used, ["name" => "hasUser", "class" => 'form-control']);
echo '</div>';

echo '<div class="input-group">';
echo '<label class = "control-label" for="hasServiceProvider">' . __d('Forms', "Search.Label.ServiceProvider") . '</label>';
echo $this->Form->select('hasServiceProvider', $hasServiceProvider, ["name" => "hasServiceProvider", "class" => 'form-control']);
echo '</div>';


echo '<div class="input-group">';
echo '<label class = "control-label" for="order">' . __d('Forms', "Search.Label.Sort") . '</label>';
echo $this->Form->select('order', $order, ["class" => 'form-control']);
echo '</div>';
?>

<div class="input-group">
    <?= $this->Form->input('search', [
        'title' => 'search',
        'label' => false,
        "type" => "text",
        "class" => "hidden"]) ?>
</div>

<div class="form-group">
    <?= $this->Form->button(__d("ElementNavigation", "Filter"), ['class' => 'btn btn-default submit-form filter']) ?>
</div>

<?= $this->Form->end() ?>


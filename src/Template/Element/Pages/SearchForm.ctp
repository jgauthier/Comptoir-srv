
<?= $this->Form->create(null, ['url' => ['prefix'=>false,'controller' => "Pages", 'action' => "search"], 'class' => "form-inline", 'id' => 'navbar-search','title' => __d("Forms", "Search for softwares and users")]) ?>

        <div class="input-group">
            <?= $this->Form->input('search', [
                'title' => 'search',
                'label' => false,
                "type" => "text",
                "maxLength" => 64,
                "class" => "form-control"]) ?>
            <div class="input-group-btn">
                <?= $this->Form->button(__d("ElementNavigation", "Search"), ['class' => 'btn btn-default submit-form search']) ?>
            </div>
        </div>

<?= $this->Form->end() ?>

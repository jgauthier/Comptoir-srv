<?php
/*
 * This is the base of all pages
 */
?>

<!DOCTYPE html>
<html lang="<?=$selectedLanguage?>">
  <?= $this->element("Pages/header") ?>
    <body class="backgroundbody">

        <?= $this->element("Pages/header-Adullact") ?>

        <?= $this->element("Navigation/navbarFixedTop") ?>
        <div class="container-fluid" id="super-main">
            <main>
                <div class = "row">
                    <?= $this->Flash->render() ?>
                </div>
                <?= $this->fetch("content")?>
            </main>
                <?= $this->element("Pages/footer") ?>
        </div>
        <?= \Cake\Core\Configure::read("Piwik") ?>
    </body>
</html>

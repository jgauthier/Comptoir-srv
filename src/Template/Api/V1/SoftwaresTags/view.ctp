<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Softwares Tag'), ['action' => 'edit', $softwaresTag->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Softwares Tag'), ['action' => 'delete', $softwaresTag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $softwaresTag->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Softwares Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Softwares Tag'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Softwares'), ['controller' => 'Softwares', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Software'), ['controller' => 'Softwares', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="softwaresTags view large-9 medium-8 columns content">
    <h3><?= h($softwaresTag->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Software') ?></th>
            <td><?= $softwaresTag->has('software') ? $this->Html->link($softwaresTag->software->softwarename, ['controller' => 'Softwares', 'action' => 'view', $softwaresTag->software->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tag') ?></th>
            <td><?= $softwaresTag->has('tag') ? $this->Html->link($softwaresTag->tag->name, ['controller' => 'Tags', 'action' => 'view', $softwaresTag->tag->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($softwaresTag->id) ?></td>
        </tr>
    </table>
</div>

<?php

$this->layout = 'base';
$this->assign('title', __d("Softwares",'Softwares.index.title'));

?>
<?php if (!empty( $softwares ) ): ?>

    <section>
        <?php
        echo $this->Lists->block(
            $softwares,
            [
                "type" => "software",
                "linkParticipate" => [
                    "id" => null,
                    "text" => __d("Softwares", "Add a software"),
                ],
                "form" => $this->element("Pages/SearchFormSoftwares"),
                "title" => "<h1>" . __d("Softwares","Software list for Comptoir du libre ({0})",count($softwares)) . "</h1>",
                "titleAddMore" => __d("Softwares", "Add a software in Comptoir du libre"),
                "emptyMsg" => __d("Softwares", "No software in {0}, yet.", "Comptoir du libre"),

            ],
            false
        ); ?>

        <div class="paginator cdl-paginator">
            <ul class="pagination">
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
            </ul>
            <p><?= $this->Paginator->counter([
                    'format' => 'range'
                ]) ?></p>
        </div>
    </section>



<?php endif; ?>

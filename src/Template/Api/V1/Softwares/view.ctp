<?php
$this->layout('base');

$this->assign('title', __d("Softwares", "{0}", $software->softwarename));
?>
<!-- ------------------------------------------ Overview ----------------------------------------------------------- -->
<section class="row clearfix">
    <?php echo $this->element("Softwares/overview") ?>
</section>
<!-- ------------------------------------------------ End overview ------------------------------------------------ -->

<section class="clearfix">


    <section>
        <?php echo $this->Lists->block(
            $software->userssoftwares,
            [
                "type" => "user",
                "title" => "<h2>" . __d("Softwares", "Users of {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "softwares/usersSoftware/$software->id",
                    "title" => __d("Softwares", "See all declared users of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} users)", count($software->userssoftwares)),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "usersSoftware",
                    "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
                ],
                "linkFallBack" => [
                    "id" => $software->id,
                    "action" => "deleteUsersSoftware",
                    "text" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage", $software->softwarename),
                ],
                "indicator" => [
                    "idTooltip" => "usersOfListId",
                    "indicatorMessage" => __d("Softwares", "softwares.usersOfList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Softwares.Users.DelcareAs.user.addMessage", $software->softwarename),
                "titleFallBack" => __d("Softwares", "Softwares.Users.DelcareAs.user.removeMessage.", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No user for {0}", $software->softwarename)
            ]
        ); ?>
    </section>

    <section class="clearfix">
        <?php
        echo $this->Lists->block(
            $software->reviews,
            [
                "type" => "review",
                "title" => "<h2>" . __d("Softwares", "Reviews for {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "softwares/$software->id/reviews",
                    "title" => __d("Softwares", "See all reviews of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} reviews)", count(\Cake\Utility\Hash::extract($software->reviews, '{n}'))),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "addReview",
                    "text" => __d("Softwares", "Sofwares.Review.addMessage"),
                    "method" => "GET",
                ],
                "indicator" => [
                    "idTooltip" => "reviewsListId",
                    "indicatorMessage" => __d("Softwares", "softwares.reviewsList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Sofwares.Review.addMessage", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No review for {0}.", $software->softwarename)
            ]
        ); ?>
    </section>

    <section>
        <?php echo $this->Lists->block(
            $software->providerssoftwares,
            [
                "type" => "user",
                "title" => "<h2>" . __d("Softwares", "Service providers for {0}", $software->softwarename) . "</h2>",
                "linkSeeAll" => [
                    "url" => "softwares/servicesProviders/$software->id",
                    "title" => __d("Softwares", "See all service providers of {0}", $software->softwarename),
                    "tooManyMsg" => __d("Layout", "See all ({0} services providers)", count($software->providerssoftwares)),
                ],
                "linkParticipate" => [
                    "id" => $software->id,
                    "action" => "servicesProviders",
                    "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
                ],
                "linkFallBack" => [
                    "id" => $software->id,
                    "action" => "deleteServicesProviders",
                    "text" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.removeMessage", $software->softwarename),
                ],
                "indicator" => [
                    "idTooltip" => "serviceProvidersOfListId",
                    "indicatorMessage" => __d("Softwares", "softwares.serviceProvidersOfList", $software->softwarename)
                ],
                "titleAddMore" => __d("Softwares", "Softwares.Users.DelcareAs.serviceProvider.addMessage", $software->softwarename),
                "emptyMsg" => __d("Softwares", "No service provider for {0}.", $software->softwarename)
            ]
        ); ?>
    </section>

    <?php echo $this->Lists->block(
        $software->screenshots,
        [
            "type" => "screenshot",
            "title" => "<h2>" . __d("Softwares", "Screenshots of {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "softwares/$software->id/screenshots",
                "title" => __d("Softwares", "See all screenshots of {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} screenshots)", count($software->screenshots)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "addScreenshot",
                "text" => __d("Softwares", "Add a screenshot for {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "sreenshotsListId",
                "indicatorMessage" => __d("Softwares", "softwares.sreenshotsList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Softwares.Screenshots.addMessage", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No screenshot for {0}.", $software->softwarename)
        ]
    ); ?>
</section>

<section>
    <?php echo $this->Lists->block(
        $software->workswellsoftwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Softwares", "Working well with {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "softwares/workswellSoftwares/$software->id",
                "title" => __d("Softwares", "See all softwares working well with {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} softwares)", count($software->workswellsoftwares)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "",
                "text" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "softwaresWorkingWellWithListId",
                "indicatorMessage" => __d("Softwares", "softwares.softwaresWorkingWellWithList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Add a software that works well with {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "There are no project for {0}", $software->softwarename)
        ]
    ); ?>
</section>


<section>
    <?php echo $this->Lists->block(
        $software->alternativeto,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Softwares", "Alternative to {0}", $software->softwarename) . "</h2>",
            "linkSeeAll" => [
                "url" => "softwares/alternativeTo/$software->id",
                "title" => __d("Softwares", "See all alternatives to {0}", $software->softwarename),
                "tooManyMsg" => __d("Layout", "See all ({0} softwares)", count($software->alternativeto)),
            ],
            "linkParticipate" => [
                "id" => $software->id,
                "action" => "",
                "text" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
                "class" => "btn btn-default inactive-button",
            ],
            "indicator" => [
                "idTooltip" => "softwaresAlternativeToListId",
                "indicatorMessage" => __d("Softwares", "softwares.softwaresAlternativeToList", $software->softwarename)
            ],
            "titleAddMore" => __d("Softwares", "Add a software that is an alternative to {0}.", $software->softwarename),
            "emptyMsg" => __d("Softwares", "No alternative to {0}.", $software->softwarename)
        ]
    ); ?>
</section>



<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
<li><?= $this->Html->link(__('New Software'), ['action' => 'add']) ?></li>
<li><?= $this->Html->link(__('List Licenses'), ['controller' => 'Licenses', 'action' => 'index']) ?></li>
<li><?= $this->Html->link(__('New License'), ['controller' => 'Licenses', 'action' => 'add']) ?></li>
</ul>
</nav>
<div class="softwares index large-9 medium-8 columns content">
    <h3><?= __('Softwares') ?></h3>
</div>

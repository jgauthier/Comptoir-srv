<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Edit {0}',$software->softwarename, isset($message) ? " - ".$message : "") );
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="form col-xs-offset-3 col-xs-6 columns content">
        <div class="softwares form large-9 medium-8 columns content">
            <?= $this->Form->create($software, ['type' => 'file',"enctype" => "multipart/form-data"]) ?>
            <fieldset>
                <legend><?= "<h1>" .  __d("Forms",'Edit {0}' ,$software->softwarename)."</h1>"  ?></legend>

                    <?= $this->Form->input(
                        'softwarename',
                        [
                            "class" => "form-control",
                            "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} Software name: ", '<span class = "asterisk">*</span>') ],
                            "required"=>"required",
                            "value" => isset($software->softwarename) ? $software->softwarename : null,
                            "escape"=>false,
                        ]); ?>

                    <?= $this->Form->input(
                        'url_website',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} URL of official web site", '<span class = "asterisk">*</span>') ],
                            "required"=>"required",
                            "value" => isset($software->url_website) ? $software->url_website : null,
                            "escape"=>false,
                        ]) ?>

                    <?= $this->Form->input(
                        'url_repository',
                        [
                            "type"=>"url",
                            "class" => "form-control",
                            "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} URL of source code repository", '<span class = "asterisk">*</span>')],
                            "required"=>"required",
                            "escape"=>false,
                        ]) ?>

                    <?= $this->Form->input('description', [
                            "type"=>"textarea",
                            "class" => "form-control",
                            "value" => isset($software->description) ? $software->description : null,
                            "label" => ["class"=>"control-label","text"=>__d("Forms", "Description: ") ],
                            "escape"=> false,
                        ])?>

                    <?= $this->Form->input(
                        'photo',
                        [
                            'type' => 'file',
                            "label" => ["class"=>"control-label","text"=>__d("Forms", "Logo: ") ],
                            "escape"=>false,
                        ]) ?>

                </div>
                    <?php $help = '<div class="help-block">
                            <ul>';
                    !isset($software->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                    !isset($software->photo->fileBelowMaxWidth) || !isset($software->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                    !isset($software->photo->fileBelowMaxSize) ? $help .=  '<li>'.__d("Forms","Maximum weight: 1{0}.",__d('Forms',"<abbr title='Megabit'>MB</abbr>")).'</li>' : "";
                    $help .='</ul>
                        </div>';
                    echo $help;
                    ?>

                    <?= $this->Form->input(
                        'licence_id',
                        ['options' => $licenses,
                            'empty' => false,
                            "class" => "form-control",
                            "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} Licenses: ", '<span class = "asterisk">*</span>') ],
                            "escape"=>false,
                        ]) ?>
                    <?=
                    $this->Form->input(
                        'tags',
                        [
                            "value" => $software->tag_string,
                            'name' => "tag_string",
                            'error'=> __d("Tags","Tags.Validator.Error.message"),
                            "class" => "form-control",
                            "label" => ["class"=>"control-label","text"=>__d("Tags", "Tags : ") ],
                            ['type' => 'text'],

                        ]
                    );
                    ?>
                    <?php
                        $help ='<div class="help-block">
                                    <ul>';
                        $help .=        '<li>'.__d("Tags", "Tags.Validator.Help.Message.Add.Alphanumeric").'</li>' ;
                        $help .=        '<li>'.__d("Tags", "Tags.Validator.Help.Message.Add.construct").'</li>' ;
                        $help .=        '<li>'.__d("Tags", "Tags.Validator.Help.Message.Add.limit").'</li>' ;
                        $help .=    '</ul>
                                </div>';
                        echo $help;
                    ?>
            </fieldset>
            <?= $this->Form->button(__d("Forms",'Submit'),["class"=>"btn btn-default addmore"]) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>

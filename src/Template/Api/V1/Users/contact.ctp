<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Users.ContactForm.title',isset($user->username) ? $user->username : null));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->Form->create(null,['url' => ['_method'=>'post','prefix'=>false,'controller' => 'Users', 'action' => 'contact',$this->request->param('id')]]) ?>
        <fieldset>
            <legend><?= "<h1>" .  __d("Forms",'Users.ContactForm.legend',isset($user->username) ? $user->username : null )."</h1>"  ?></legend>
            <div  class = "form-group">
            <?= $this->Form->input('subject',
                [
                    "class"=>"form-control",
                    'label'=>["class"=>"control-label","text"=>__d("Forms", "Users.ContactForm.subject",'<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "type"=>"text",
                    "escape" => false
                ]);?>

                <?= $this->Form->input('text',
                    [
                        "type"=>"textarea",
                        "class"=>"form-control",
                        "label"=>__d("Forms","Users.ContactForm.text",'<span class = "asterisk">*</span>'),
                        "escape" => false,
                    ]); ?>


                <?= $this->Form->input('email',
                [
                    "class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms","Users.ContactForm.email", '<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "value"=>$currentUser->email,
                    "escape"=>false,
                ]) ?>

            <div class='form-group'>
                <?= $this->Form->input('pigeon',["type"=>"checkbox",'type'=>'hidden']); ?>
            </div>

        </fieldset>
        <?= $this->Form->button(__d("Forms","Users.ContactForm.submit"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php
$this->layout("base");

$this->assign('title', __d("Forms",'Create an account{0}', isset($message) ? " - ".__d("Forms",$message) : ""));
?>

<div class="row">
    <div class = "warning-form bg-warning col-xs-offset-3 col-xs-6">
            <?= __d("Forms","Fields marked with an asterisk ({0}) are required.",'<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->Form->create($user,['type' => 'file',"enctype"=>"multipart/form-data"]) ?>
        <fieldset>
            <legend><?= "<h1>" . __d("Forms",'Create an account') . "</h1>" ?></legend>
                <div  class = "form-group">
                    <label class = "control-label" for="user_type_id"><?=__d("Forms","{0} User type: ", '<span class = "asterisk">*</span>')?></label>
                    <div class="radio ">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-1" value="2" required="required">
                            <?= __d("Forms","Administration")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-2" value="4" required="required">
                            <?= __d("Forms","Person")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-3" value="5" required="required">
                            <?= __d("Forms","Company")?>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="user_type_id" id="user_type_id-4" value="6" required="required">
                            <?= __d("Forms","Association")?>
                        </label>
                    </div>
                </div>
                <?= $this->Form->input('username',
                    [
                        "class"=>"form-control",
                        'label'=>["class"=>"control-label","text"=>__d("Forms", "{0} Name: ",'<span class = "asterisk">*</span>')],
                        "required"=>"required",
                        "type"=>"text",
                        "escape" => false
                    ]);?>

                <?= $this->Form->input('url',
                    [
                        "class"=>"form-control",
                        "label" => ["class"=>"control-label","text"=>__d("Forms", "Website URL: ")],
                        'type'=>'url',
                        "escape" => false
                    ]);?>

                <?= $this->Form->input('email',
                    [
                        "class"=>"form-control",
                        "label" => ["class"=>"control-label","text"=>__d("Forms","{0} Email: ", '<span class = "asterisk">*</span>')],
                        "required"=>"required",
                        "escape"=>false,
                    ]) ?>

            <div class='form-group'>
                <?= $this->Form->input('description',["type"=>"textarea","class"=>"form-control","label"=>__d("Forms","Description : ")]); ?>
            </div>
            <div class='form-group'>
                <?= $this->Form->input('role',['value' => 'User','type'=>'hidden',"required"=>"required"]); ?>
            </div>

                <?php echo  $this->Form->input(
                    'password',
                    [
                        "class"=>"form-control",
                        "label" => ["class"=>"control-label","text"=>__d("Forms"," {0} Password : ", '<span class = "asterisk">*</span>')],
                        "required"=>"required",
                        "escape"=>false,
                    ]
                ); ?>

                <?= $this->Form->input('password',
                    ["class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms"," {0} Confirm password : ", '<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "name" => "confirm_password",
                    "id" => "confirm_password" ,
                    "escape"=>false,
                    ]
                ); ?>

                <?= $this->Form->input(
                    'photo',
                    [
                        'type' => 'file',
                        "label" => ["class"=>"control-label","text"=>__d("Forms", " {0} Avatar: ", '<span class = "asterisk">*</span>') ],
                        "escape"=>false,

                    ]) ?>

                <?php $help = '<div class="help-block">
                        <ul>';
                        !isset($user->photo->file) ?  $help .=  '<li>'.__d("Forms","Accepted formats JPEG, PNG, GIF, SVG.").'</li>' : "";
                        !isset($user->photo->fileBelowMaxWidth) || !isset($user->photo->fileBelowMaxHeigth) ?  $help .=  '<li>'.__d("Forms","Maximum size: 350x350px.").'</li>' : "";
                        !isset($user->photo->fileBelowMaxSize) ? $help .=  '<li>'.__d("Forms","Maximum weight: 1{0}.",__d('Forms',"<abbr title='Megabit'>MB</abbr>")).'</li>' : "";
                $help .='</ul>
                    </div>';
                echo $help;
                ?>
        </fieldset>
        <?= $this->Form->button(__d("Forms","Sign up"),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>
    </div>

</div>

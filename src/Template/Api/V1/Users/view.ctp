<?php
$this->layout('base');
$this->assign('title', __d("Users", $user->username));

?>
<section class="row">
    <div class=" col-sm-6 col-md-4 col-lg-4 hidden-xs">
        <div class="size-logo-overview">
            <?php if (!$this->User->hasLogo($user)) : ?>
                <?= $this->Html->link($this->Html->image("logos/User_placeholder.jpg",
                    ["alt" => $user->username, "class" => "img-responsive"]),
                    ['prefix' => false, 'controller' => 'Users', 'action' => '', $user->id],
                    ['escape' => false]) ?>
            <?php else : ?>
                <?= $this->Html->link($this->Html->image($user->logo_directory . DS . $user->photo,
                    ["alt" => $user->username, "class" => "img-responsive"]),
                    isset($user->url) ? $user->url : "#",
                    ['escape' => false]) ?>
            <?php endif; ?>
        </div>

    </div>

    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
        <ul class="list-unstyled">
            <li>
                <h1>
                    <?= $user->username ?>
                </h1>
            </li>
            <li>
                <?php echo $user->url != "" ? $this->Html->link($user->url) : null ?>
            </li>
            <li>
                <p class="text-overflow">
                    <?= $user->description ?>
                </p>
            </li>
            <li>
                <?= $this->Html->link(__d("Users", "Contact"),
                    ['prefix' => false, 'controller' => $this->request->controller, 'action' => "contact", $user->id],
                    ['class' => 'btn btn-default contact-user inline-block',],
                    ['escape' => false]) ?>
            </li>
        </ul>
    </div>
</section>
<!-- END of OVERVIEW-->
<?php /** TODO : Refactor contributor's section
 * <section>
 * <?php echo $this->Lists->softwareBlock(
 * \Cake\Utility\Hash::extract($user->contributionssoftwares, '{n}.software'),
 * [
 * "title" => __d("Users", "Contributor for"),
 * "link" => [
 * "id" => $user->id,
 * "action" => "contributionsSoftwares",
 * "participate" => __d("Users", " Add"),
 * ],
 * "tooManyMsg" => __d("Layout", "See all"),
 * "titleSeeAll" => __d("Users", "See all softwares which {0} has contributed to.", $user->username),
 * "titleAddMore" => __d("Users", "Declare a software that {0} has contributed to.", $user->username),
 * "emptyMsg" => __d("Users", "{0} do not contribute to any project, yet.", $user->username)
 * ]
 * ); ?>
 * </section>
 **/ ?>
<section>
    <?php
    $softwareBlock = "";

    $user->user_type->name != 'Company' ?
        $softwareBlock = $this->Lists->block(
            $user->usedsoftwares,
            [
                "type" => "software",
                "title" => "<h2>" . __d("Users", "User of") . "</h2>",
                "linkSeeAll" => [
                    "url" => "users/usedSoftwares/$user->id",
                    "title" => __d("Users", "See all softwares used by {0}", $user->username),
                    "tooManyMsg" => __d("Layout", "See all"),
                ],
                "indicator" => [
                    "idTooltip" => "softwareListUserOfId",
                    "indicatorMessage" => __d("Users", "users.softwareListUserOf", $user->username)
                ],
                "titleAddMore" => __d("Users", "Declare a software that is used by {0}.", $user->username),
                "emptyMsg" => __d("Users", "{0} do not used a software, yet.", $user->username)
            ]
        )
        : "";
    echo $softwareBlock;
    ?>
</section>
<section>
    <?php
    echo $this->Lists->block(
        $user->providerforsoftwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Users", "Services provider for") . "</h2>",
            "linkSeeAll" => [
                "url" => "users/providerforSoftwares/$user->id",
                "title" => __d("Users", "See all softwares which {0} is a service provider for.", $user->username),
                "tooManyMsg" => __d("Layout", "See all"),
            ],
            'LinkParticipate'=>[],
            "indicator" => [
                "idTooltip" => "ServiceProviderForSoftwareListId",
                "indicatorMessage" => __d("Users", "users.ServiceProviderForSoftwareList", $user->username)
            ],
            "titleAddMore" => __d("Users", "Declare a software that {0} is a service provider for.", $user->username),
            "emptyMsg" => __d("Users", "{0} do not provide a software, yet.", $user->username)
        ],
        true
    ); ?>
</section>
<section class="clearfix">
    <?php
    echo $this->Lists->block(
        $user->reviews,
        [
            "type" => "review",
            "title" => "<h2>" . __d("Users", "Users.View.Reviews.Title", count($user->reviews)) . "</h2>",
            "linkSeeAll" => [
                "url" => "users/$user->id/reviews",
                "title" => __d("Users", "Users.View.Reviews.Message.SeeAll", $user->username),
                "tooManyMsg" => __d("Layout", "See all ({0} reviews)", count($user->reviews)),
            ],
            'LinkParticipate'=>[],
            "indicator" => [
                "idTooltip" => "reviewsListId",
                "indicatorMessage" => __d("Users", "Users.View.Reviews.IndicatorMessage", $user->username)
            ],
            "titleAddMore" => __d("Users", "", $user->username),
            "emptyMsg" => __d("Users", "Users.View.Reviews.NoOne", $user->username)
        ]
    ); ?>
</section>

<?php
$this->layout("base");
$this->assign('title', __d("Forms",'Reset your password', isset($message) ? " - ".__d("Forms",$message) : ""));
?>


<div class="row">
    <div class = "col-xs-offset-3 col-xs-6">

        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __d("Forms",'Reset your password') ?></legend>

                <?php echo  $this->Form->input(
                    'email',
                    [
                        "class"=>"form-control",
                        "label"=>["class"=>"control-form","text"=>__d("Forms","Users.lostPassword.email",'<span class = "asterisk">*</span>')],
                        "required"=>"required",
                        "escape"=>false,
                    ]
                ); ?>
            </div>
        </fieldset>
        <?= $this->Form->button(__d("Forms","Send to me an email to reset my password."),["class"=>"btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

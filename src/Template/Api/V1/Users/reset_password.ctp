<?php
$this->layout("base");
$this->assign('title', __d("Forms", 'Reset your password {0}', isset($message) ? " - " . $message : ""));
?>

<div class="row">
    <div class="warning-form bg-warning col-xs-offset-3 col-xs-6">
        <?= __d("Forms", "Fields marked with an asterisk ({0}) are required.", '<span class = "asterisk">*</span>'); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-offset-3 col-xs-6">

        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __d("Forms", 'Change your password') ?></legend>

            <?php echo  $this->Form->input(
                'password',
                [
                    "class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms"," {0} Password : ", '<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "name" => "new_password",
                    "value"=>"",
                    "escape"=>false,
                ]
            ); ?>
            <?= $this->Form->input('confirm_password',
                ["class"=>"form-control",
                    "label" => ["class"=>"control-label","text"=>__d("Forms"," {0} Confirm password : ", '<span class = "asterisk">*</span>')],
                    "required"=>"required",
                    "name" => "confirm_password",
                    "type" => "password",
                    "value"=>"",
                    "escape"=>false,
                ]
            ); ?>

        </fieldset>
        <?= $this->Form->button(__d("Forms", "Save"), ["class" => "btn btn-default addmore"]) ?>
        <?= $this->Form->end() ?>
    </div>
</div>

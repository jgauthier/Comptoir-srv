<?php

$this->assign('title', __d("Softwares", "Review of {0} for {1}", [$review->user->username, $review->software->softwarename]));
$this->layout('base');


?>

<section class="row clearfix">
    <?php echo $this->element("Softwares/overview", ["software" => $review->software]) ?>
</section>

<section class="clearfix">
<ol class="list-unstyled">
    <?php

    echo $this->Review->display(
        $review,
        [
            'limit' => false,
            'aside' => 'software',
        ]
    ); ?>
</ol>

</section>

<?php
/**
 * Created by IntelliJ IDEA.
 * User: mpastor
 * Date: 26/07/16
 * Time: 15:00
 */
$this->assign('title', __d("Softwares", "Reviews for {0}", $software->softwarename));

?>

<?php
$this->layout('base');
?>



<section>
    <?php

    echo $this->Lists->block(
        $reviews,
        [
            "type" => "review",
            "title" => "<h1>" . __d("Softwares", "Reviews for {0}", $software->softwarename) ."</h1>",
            "link" => [
                "id" => $software->id,
                "action" => "reviewsSofware",
                "participate" => __d("Softwares", "Add a review for {0}.", $software->softwarename),
            ],
            "titleAddMore" => __d("Softwares", "Add a review for {0}.", $software->softwarename),
        ],
        false
    ); ?>
</section>

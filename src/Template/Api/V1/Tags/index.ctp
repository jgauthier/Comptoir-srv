<?php

$this->layout = 'base';
$this->assign('title', __d("Tags","TitleTagsIndex"));
?>

<section class="row">
    <h1><?= __d("Tags","TitleTagsIndexh1",count($tags)) ?></h1>
</section>


<?= $this->Form->create(null, ['class' => "form-inline", 'title' => __d("Forms", "Search for softwares and users"),'method'=>"GET"]) ?>

<?php

echo '<div class="input-group">';
echo '<label class = "control-label" for="order">' . __d('Forms', "Search.Label.Sort") . '</label>';
echo $this->Form->select('order', $order, ["class" => 'form-control']);
echo '</div>';
?>

<div class="input-group">
    <?= $this->Form->input('search', [
        'title' => 'search',
        'label' => false,
        "type" => "text",
        "class" => "hidden"]) ?>
</div>

<div class="form-group">
    <?= $this->Form->button(__d("ElementNavigation", "Filter"), ['class' => 'btn btn-default submit-form filter']) ?>
</div>

<?= $this->Form->end() ?>



<section class="row">
    <ul class="list-inline">
        <?php
        echo $this->Tag->display(
            $tags,
            [
                "displayNumber" => true,
                "class"=>"col-xs-12 col-sm-4 col-md-3 col-lg-2 tagCell",
            ]
        );

        ?>
    </ul>
</section>

<?php
$this->layout('base');
$this->assign('title', $tag->name);

?>
<section class="clearfix row">
    <ul class="list-unstyled col-xs-12">

        <li>
            <h1><?= __d("Tags", "Tag") . " " . $tag->name ?></h1>
        </li>
        <li>
            <?= $this->Html->link(__d("Tags", "TitleTagsIndex"),
                ['prefix' => false, 'controller' => $this->request->controller, 'action' => "index"],
                ['class' => 'btn btn-default edit-software inline-block',],
                ['escape' => false]) ?>
        </li>
    </ul>
</section>

<section class="clearfix">
    <?php
    echo $this->Lists->block(
        $tag->softwares,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Tags", "SoftwareListTag") . $tag->name . " (" . count($tag->softwares) . ")" . "</h2>",
            "titleAddMore" => "",
            "emptyMsg" => __d("Tags", "tags.view.error", $tag->name),

        ],
        false

    ); ?>

</section>

<?php
$this->layout = 'base';
$this->assign('title', __d("Home", "Comptoir du libre"));
$this->assign('header', $this->element("Pages/home-header"));
?>
    <div class="row">
        <div class="jumbotron-cdl col-xs-12">
            <h2><?= __d("Pages","Home.message.title") ?></h2>
            <p>
                <?= __d("Pages", "Home.message.content.numbers", [$countSoftwares, $countReviews, $countUsers]) ?>
                <br/>
                <?= __d("Pages", "Home.message.content.phrase") ?>
            </p>
        </div>
    </div>

<?php if (isset($softwaresSelected)) : ?>
    <?php
    echo $this->Lists->block(
        $softwaresSelected,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Home", "Pick of the month") . "</h2>",
            "link" => [
                "participate" => "",
            ],
            "tooManyMsg" => "",
            "titleSeeAll" => "",
            "titleAddMore" => "",
            "emptyMsg" => "",
        ],
        true
    ); ?>


<?php endif; ?>


<?php if (isset($softwaresLastAdded)) : ?>
    <?php

    echo $this->Lists->block(
        $softwaresLastAdded,
        [
            "type" => "software",
            "title" => "<h2>" . __d("Home", "Last added") . "</h2> ",
            "linkSeeAll" => [
                "url" => "softwares?limit=12&sort=created&direction=desc",
                "title" => __d("Softwares", "Softwares.SeeAll.lastAdded"),
                "tooManyMsg" => __d("Softwares", "Softwares.SeeAll.lastAdded"),
            ],
        ],
        true
    ); ?>
<?php endif; ?>

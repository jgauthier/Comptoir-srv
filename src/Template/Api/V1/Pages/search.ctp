<?php

$this->layout = 'base';

$this->assign('title', __d("Pages","{0} search.title {1}" ,[count($softwares)+count($users),$searchQuery]));

?>



<?php if ( (!empty( $softwares )) || (!empty( $users ) )): ?>
    <?= "<h1>" . __d("Pages",
        "search.results",
        [count($softwares)+count($users)]).'<span class="text-muted"> "'.$searchQuery.'"</span>'. "</h1>"?>

    <section>
        <?php
        echo $this->Lists->block(
            $softwares,
            [
                "type" => "software",
                "linkParticipate" => [
                    "id" => null,
                    "text" => __d("Softwares", "Add a software"),
                    "action" => "add",
                    'controller' => "Softwares",
                ],
                "indicator" => [
                    "idTooltip" => "softwaresListId",
                    "indicatorMessage" => __d("Pages","search.softwaresList")
                ],
                "form" => $this->element("Pages/SearchFormSoftwares"),
                "title" => "<h2>". __d("Pages","search.softwares",count($softwares)) .'<span class="text-muted"> "'.$searchQuery.'"</span>'. "</h2>",
                "titleAddMore" => __d("Softwares", "Add a software in Comptoir du libre"),
                "emptyMsg" => __d("Softwares", "No software matching with your request in Comptoir du libre, yet."),

            ],
            false
        ); ?>

    </section>

    <section>

        <?php

        echo $this->Lists->block($users,
            [
                "type" => "user",
                "title" => "<h2>" . __d("Pages","search.users",count($users)) .'<span class="text-muted"> "'.$searchQuery.'"</span>'."</h2>",
                "link" => [
                    "action" => "UsedSoftwares",
                    "participate" => __d("Users", "participate"),
                ],
                "linkParticipate" => [
                    "id" => null,
                    "action" => "add",
                    "text" => __d("Forms", "Sign up"),
                    "authorized" => true,
                ],
                "indicator" => [
                    "idTooltip" => "usersListId",
                    "indicatorMessage" => __d("Pages","search.usersList")
                ],
                "titleAddMore" => "" ,
                "emptyMsg" => __d("Users", "No user matching with your request in Comptoir du libre, yet."),

            ],
            false
        ); ?>
    </section>



<?php endif; ?>

<?php

namespace App\View\Helper;


use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

/**
 * Class ListItemHelper
 *
 * @package App\View\Helper
 */
class ListItemHelper extends Helper
{

    use StringTemplateTrait;

    public $helpers = ['Html','Text', 'Time','Form'];

    /**
     * Return TRUE if the software has a logo , FALSE otherwise.
     * @param $software
     * @return \PHPUnit_Framework_Constraint_FileExists
     */
    public function hasLogo($item)
    {
        return file_exists(WWW_ROOT . 'img' . DS . $item->logo_directory . DS . $item->photo);
    }


}

<?php

namespace App\View\Helper;

use Cake\View\StringTemplateTrait;

/**
 * Class ScreenshotHelper
 * Build structure to display a screenshot
 * @package App\View\Helper
 */
class ScreenshotHelper extends ListItemHelper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'screenshot' =>
                '<li {{attrsColumn}}>
                    <div {{attrsBlock}}> 
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                    </div>
                </li>',
        ]

    ];

    public $helpers = ['Html', 'Text', 'Time','Form'];


    /**
     * Return the Html structure of a screenshot populate with data in the parameter $screenshot
     * @param $screenshot
     * @return string
     */
    public function display($screenshot,$options = ["limit"=>false])
    {
        $result = null;
        if (!empty($screenshot)) {
            $result .= $this->formatTemplate('screenshot', [
                'attrsColumn' => $this->templater()->formatAttributes(['class' => "col-xs-12 col-sm-6 col-md-3 col-lg-3 "]),
                'attrsBlock' => $this->templater()->formatAttributes(['class' => "size-title screenShot"]),
                'LinkImage' =>  $this->Html->link($this->Html->image($screenshot->url_directory . DS . $screenshot->photo,
                    ['alt' => __d("Screenshot name : {0}", $screenshot->photo), 'class' => 'img-responsive ']),
                    "img". DS . $screenshot->url_directory . DS . $screenshot->photo,
                    ['escape' => false]),
            ]);
        }
        return $result;
    }

}

<?php

namespace App\View\Helper;

use Cake\View\StringTemplateTrait;

/**
 * Class SoftwareHelper
 * Build Html structure for a software item
 * @package App\View\Helper
 */
class SoftwareHelper extends ListItemHelper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'software' =>
                '<li {{attrsColumn}}>
                    <div {{attrsBlock}}> 
                        {{LinkItem}}
                        {{header}}{{headerEnd}}
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                        {{softwareName}}
                        <p {{attrsDescription}}>{{softwareDescription}}</p>
                        <div class="rating-unit">{{review_average}}</div>
                    </div>
                </li>',
        ]
    ];

    public $helpers = ['Html', 'Text', 'Rating', 'Form'];

    public function display($software, $options = ["limit"=>false])
    {
        $result = null;
        if (!empty($software)) {

            $logo = $this->displayLogoLink($software);

            $attrsColumn = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']);
            $attrsBlock = $this->templater()->formatAttributes(['class' => 'software-unit-home backgroundUnit']);

            $result .= $this->formatTemplate('software', [
                'attrsColumn' => $attrsColumn,
                'attrsBlock' => $attrsBlock,
                'review_average' => $this->Rating->display($software->average_review),
                'header' => $options['limit'] == true && ($this->request->param('controller') != "Pages") ? '<h3 class = "size-title">' : '<h2 class = "size-title">',
                'headerEnd' => $options['limit'] ? "</h3>" : "</h2>",
                'softwareName' =>
                    $this->Html->link(
                        $this->Text->truncate($software->softwarename, 35, ['ellipsis' => '...', 'exact' => false]),
                        ['prefix' => false, 'controller' => 'softwares', "action" => $software->id],
                        ['escape' => false, 'title' => __d("Softwares", "Software name : ") . $software->softwarename]
                    ),
                'LinkImage' => $logo,
                'attrsDescription' => $this->templater()->formatAttributes(['class' => 'text-overflow project-description']),
                'softwareDescription' => $this->Text->truncate($software->description, 100, ['ellipsis' => '...', 'exact' => false]),
                ['class' => 'text-overflow project-description'],
                'LinkItem' => $this->Html->link("",
                    ['prefix'=>false,'controller' => 'Softwares', 'action' => $software->id],
                    ["class" => "linkItem", 'escape' => false,]),
            ]);
        }
        return $result;
    }



    /**
     * Return a link with containing the software'logo if it get one, placeholder otherwise
     * @param $software
     * @return mixed
     */
    public function displayLogoLink($software)
    {

        return !$this->hasLogo($software) ?

            $this->Html->link($this->Html->image("logos/Softwarelogo_placeholder.jpg",
                ["alt" => __d("Softwares", "Go to the {0}'s page", $software->softwarename), "class" => "img-responsive"]),
                ['prefix' => false, 'controller' => 'softwares', "action" => $software->id],
                ['escape' => false, "title" => __d("Softwares", "Software name : ") . $software->softwarename])
            :
            $this->Html->link($this->Html->image($software->logo_directory . DS . $software->photo,
                ["alt" => $software->softwarename, "class" => "img-responsive"]),
                ['prefix' => false, 'controller' => 'softwares', "action" => $software->id],
                ['escape' => false, "title" => __d("Softwares", "Software name : ") . $software->softwarename]);
    }
}

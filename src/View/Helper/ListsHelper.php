<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

/**
 * Class ListsHelper
 * Build list of items passed
 * @package App\View\Helper
 */
class ListsHelper extends Helper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'header' => '<div class="align">
                            {{sectionTitle}}
                            {{indicator}}
                            <span class="allSee" {{attribs}}>
                                {{link}} 
                            </span>
                            {{addMore}}
                        </div>',
            'searchForm' => '<div {{attrsform}}>
                                {{form}}
                            </div>',
            'indicator' => ' <i {{attribsIndicator}}></i>
                            <div {{attribsToolTip}} >{{indicatorMessage}}</div>',
            'headerScreenShots' => '<div class="align">
                            <span class="allSee" {{attribs}}>
                                {{link}} 
                            </span>
                            {{addMore}}
                        </div>',
            'addMore' => '{{link}}',
            'seeMore' => '<li {{attrsColumn}}><div {{attribs}}>
                            <p>
                             {{link}}
                            </p>
                        </div></li>',

        ]
    ];

    public $helpers = ['Html', 'Text', 'Time','Form','Review','Software','User','Screenshot','Action'];


    /**
     * Create an ordered list of items
     * @param array $items
     * @param $name
     * @param bool $limit
     * @return null
     */
    private function items($items, $options, $limit = true)
    {

        $result = null;
        $seeMore = null;


        $type = $options['type'];
        ($type != "screenshot" && $type != "review")
        && ($this->request->param('controller') == "Softwares" && $this->request->action == "view")
            ? shuffle($items) : null;

        $itemsToDisplay = ($limit == true) && $this->request->param('controller') != "Pages" ? array_slice($items, 0, Configure::read("MAX_DISPLAY")) : $items;
        if (!empty($itemsToDisplay)) {
            foreach ($itemsToDisplay as $item) {
                $typeHelper = ucfirst($type);
                $result .= $this->isRelationship($item, $type) ? $this->$typeHelper->display($item->$type) : $this->$typeHelper->display($item, ['limit'=>$limit]);
            }

            $seeMore = $this->itemSeeMore(count($items), $options);
            $seeMore ? $result .= $seeMore : "";

        }

        return null !== $result ? $this->Html->tag('ol', $result, ['class' => 'row list-unstyled']) : null;
    }

    public function block($items, array $options, $limit = true)
    {

        $seeAll = "";
        $itemsDisplay = "";
        if (!empty($items)) {
            $itemsDisplay = $this->items($items, $options, $limit);
            $seeAll = $this->linkSeeMore(count($items), $options);
        }
        $participateLink =  ($this->userIsInList($items, $options) && $this->request->session()->read('Auth.User.user_type') !== null) ?
            $this->Action->fallBack(isset($options["linkFallBack"]) ? $options["linkFallBack"] : []) :
            $this->Action->participate(isset($options['linkParticipate']) ? $options['linkParticipate'] : []);

        $header = $this->formatTemplate(
            'header',
            [
                'sectionTitle' => $options["title"],

                'indicator' => isset($options["indicator"]) ? $this->formatTemplate('indicator', [
                        "attribsIndicator" => $this->templater()->formatAttributes(["class" => "fa fa-question-circle fa indicator", "aria-hidden" => true, "aria-describedby" => $options["indicator"]["idTooltip"]]),
                        'indicatorMessage' => $options["indicator"]["indicatorMessage"],
                        'attribsToolTip' => $this->templater()->formatAttributes(["role" => "tooltip", "id" => $options["indicator"]["idTooltip"], "class" => "warning-form bg-warning"])]
                ) : null,


                'spanAttr' => $this->templater()->formatAttributes($options),
                'link' => $seeAll,
                'addMore' => $participateLink,
            ]);

        $searchForm = isset($options["form"]) ? $this->formatTemplate(
            'searchForm',
            [
                'form' => $options["form"],
                'attrsform' => $this->templater()->formatAttributes(["class" => "searchFormFilters"]),
            ]
        ) : null;

        if (strpos($itemsDisplay, '<li') == false) {
            $itemsDisplay .= $this->Html->tag(
                'p',
                $options["emptyMsg"]
            );
        }
        return $header . $searchForm . $itemsDisplay;
    }



    private function isRelationship($item, $type)
    {
        return isset($item->$type);
    }

    private function itemSeeMore($countItems, $options)
    {

        return $countItems > Configure::read("MAX_DISPLAY") && $this->request->action == 'view' ?
            $this->formatTemplate(
                'seeMore',
                [
                    'attrsColumn' => $attrsBlock = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']),
                    'attribs' => $this->templater()->formatAttributes(
                        ["class" =>
                            $options["type"] .
                            "-unit-home unit-see-more "
                            . ($options["type"] == "review" ? "blockReview" : "backgroundUnit")
                        ]),
                    'link' => $this->Html->link("",
                        $options["linkSeeAll"]["url"],
                        ['title' => $options["linkSeeAll"]["title"], 'class' => 'fa fa-chevron-right'],
                        ['escape' => false])
                ]) :
            null;
    }

    private function linkSeeMore($itemsCount, $options = [])
    {
        $seeAll = null;
        if ( $itemsCount > Configure::read("MAX_DISPLAY") && isset ($options["linkSeeAll"])  ) {
            $seeAll = $this->Html->link($options["linkSeeAll"]["title"],
                $options["linkSeeAll"]["url"],
                ['title' => $options["linkSeeAll"]["title"]],
                ['escape' => false]);
        }

        return $seeAll;

    }


    /**
     * retunr TRUE if the current user is in the list, FALSE OTHERWISE
     */
    private function userIsInList($items, $options = [])
    {
        foreach ($items as $item) {
            if (($item->user_id == $this->request->session()->read('Auth.User.id'))) {
                    return true;
                }
            }
        return false;
    }
}

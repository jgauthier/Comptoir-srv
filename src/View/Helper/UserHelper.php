<?php

namespace App\View\Helper;



use Cake\View\StringTemplateTrait;

/**
 * Class UserHelper
 * Build the Html structure for the User item
 * @package App\View\Helper
 */
class UserHelper extends ListItemHelper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'user' => '<li {{attrsColumn}}>
                    <div {{attrsBlock}}>
                    {{LinkItem}}
                    <div {{CssBadge}}>
                        <div {{attrsBadge}}>
                            <span class="sr-only">{{UserType}}</span>
                        </div>
                    </div>
                        {{header}}
                        {{name}}
                        {{headerEnd}}
                        <div class="size-logo">
                            {{LinkImage}}
                        </div>
                    </div>
                </li>',
        ]
    ];


    /**
     * @param $user
     * @param bool $options['limit'] : true -> 4 columns else 6 columns
     * @return null
     */
    public function display($user, $options = ["limit"=>false])
    {
        $result = null;
        if (!empty($user)) {

            $logo = $this->displayLogoLink($user);


            $attrsColumn = $this->templater()->formatAttributes(['class' => 'col-xs-12 col-sm-6 col-md-3 col-lg-3']);
            $attrsBlock = $this->templater()->formatAttributes(['class' => 'user-unit-home backgroundUnit list-unstyled']);

            /**
             * Determine badge (community, asso or company, ) between UsersBlock's categories.
             */
            if ($user->user_type->name == "Administration") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-university fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgeAdministration"]);
            } else if ($user->user_type->name == "Company") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-briefcase fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => "stamp badge badgeCompany"]);
            }  else if ($user->user_type->name == "Association") {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-users fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgeAsso"]);
            } else {
                $attrsBadge = $this->templater()->formatAttributes(['class' => "fa fa-user fa-2x", 'title' => __d("Users", "User type : ") . $user->user_type->name]);
                $CssBadge = $this->templater()->formatAttributes(['class' => " stamp badge badgePerson"]);
            }

            $result .= $this->formatTemplate('user', [
                'UserType' => $user->user_type->name,
                'CssBadge' => $CssBadge,
                'attrsBadge' => $attrsBadge,
                'attrsColumn' => $attrsColumn,
                'attrsBlock' => $attrsBlock,
                'header' => $options['limit'] ? '<h3 class = "size-title">' : '<h2 class = "size-title">',
                'headerEnd' => $options['limit'] ? "</h3>" : "</h2>",
                'name' =>
                    strlen($user->username) > 35 ?
                        $this->Html->link($this->Text->truncate($user->username, 35, ['ellipsis' => '...', 'exact' => false]),
                            ['prefix'=>false,'controller' => 'Users', 'action' => $user->id],
                            ['escape' => false, "title" => __d("Users", "User name : ") . $user->username])
                        :
                        $this->Html->link($user->username,
                            ['prefix'=>false,'controller' => 'Users', 'action' => $user->id],
                            ['escape' => false,]
                        ),
//                'attrsTitle' => $this->templater()->formatAttributes(['class' => 'size-title']),
                'LinkImage' => $logo,
                'LinkItem' => $this->Html->link("",
                    ['prefix'=>false,'controller' => 'Users', 'action' => $user->id],
                    ["class" => "linkItem linkItemAllArea", 'escape' => false,]),
            ]);
        }
        return $result;
    }

    /**
     * Return a link with containing the user'logo if it get one, placeholder otherwise
     * @param $user
     * @return mixed
     */
    public function displayLogoLink($user)
    {

        return !$this->hasLogo($user) ?

            $this->Html->link($this->Html->image("logos/User_placeholder.jpg",
                ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]),
                ['prefix'=>false,'controller' => 'Users', 'action' => $user->id],
                ['escape' => false])
            :
            $this->Html->link($this->Html->image($user->logo_directory . DS . $user->photo,
                ["alt" => __d("Users", "Go to the {0}'s page", $user->username), "class" => "img-responsive"]),
                ['prefix'=>false,'controller' => 'Users', 'action' => $user->id],
                ['escape' => false]);
    }
}

<?php

namespace App\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;

/**
 * Class ActionHelper
 * Build forms needed to participate on Comptoir du libre.
 * @package App\View\Helper
 */
class ActionHelper extends Helper
{

    use StringTemplateTrait;

    public $helpers = ['Html', 'Text', 'Time', 'Form'];

    /**
     * @param array $options
     */
    public function fallBack(array $options)
    {

        $options["class"] = isset($options["class"]) ? $options['class'] : "btn btn-default removeOne";
        $options["method"] = isset($options["method"]) ? $options["method"] : "delete"; // default post method
        $options["action"] = isset($options["action"]) ? $options["action"] : null;

        return
            (!empty($options) &&
                $this->hasAccess($this->request->session()->read('Auth.User.user_type'), 'Softwares', $options["action"])) ?

                $this->Form->create(null,
                    [

                        "url" => ['prefix' => false, 'controller' => $this->request->controller, 'action' => $options["action"], $options["id"]],
                        "class" => "addmore-form",
                        "type" => $options['method'],
                        $options["id"]]
                ) .
                $this->Form->button($options["text"], ["class" => $options["class"]]) .
                $this->Form->end() :

                null;
    }

    /**
     * @param array $options
     * @return mixed
     */
    public function participate(array $options)
    {


        $userType = $this->request->session()->read('Auth.User.user_type') === null ?
            "Unknown"
            : $this->request->session()->read('Auth.User.user_type');

        $options["class"] = isset($options["class"]) ? $options['class'] : "btn btn-default addmore";
        $options["method"] = isset($options["method"]) ? $options["method"] : "post"; // default post method
        $options["action"] = isset($options["action"]) ? $options["action"] : "add";
        $options["controller"] = isset($options["controller"]) ? $options["controller"] : $this->request->controller;


        if (!empty($options) &&
            $this->hasAccess($userType, $options['controller'], $options["action"])
        ) {
            $result = $this->Form->create(null,
                    [

                        "url" => ['prefix' => false, 'controller' => $options['controller'], 'action' => $options["action"], $options["id"]],
                        "class" => "addmore-form",
                        "type" => $options["method"], // default post method
                        $options["id"]]
                ) .
                $this->Form->button($options["text"], ["class" => $options["class"]]) .
                $this->Form->end();
        } else {
            $result = isset ($options["text"]) ? $this->Form->button($options["text"], ["class" => 'btn btn-default inactive-button',"disabled"=>"disabled"]) : null;

        }

        return $result;
    }

    public function hasAccess($type, $controller, $action)
    {
        return Configure::read("ACL.$controller.$action.$type");
    }
}

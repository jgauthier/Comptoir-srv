<?php

namespace App\View\Helper;



use Cake\View\StringTemplateTrait;
use Cake\I18n\I18n;

/**
 * Class ReviewHelper
 * Build the Html structure for a Review item
 * @package App\View\Helper
 */
class ReviewHelper extends ListItemHelper
{

    use StringTemplateTrait;

    protected $_defaultConfig = [
        'templates' => [
            'review' => '
                        <li {{reviewLi}}>
                            <div {{attrsReview}}>
                            {{LinkReviewPage}}
                                <div class="row">
                                    {{aside}}
                                    <div {{AttrsBootstrap2}}>
                                        <h3>{{title}}</h3>
                                        <p>{{reviewComment}}</p>
                                    </div>
                                </div>
                                 
                            </div>
                        </li>',
            'aside' => '
                <div {{AttrsBootstrap1}}>
                    <div class="size-logo">
                    {{Avatar}}
                    </div>
                    <p {{Attrsname}}>{{name}}</p>
                    <p>{{stars}}</p>
                    <p>{{created}}</p>
                </div>',
        ]
    ];

    public $helpers = ['Html', 'Text', 'Time', 'Rating','User','Software'];


    public function display($review, $options = ["limit"=>true,'aside'=>'user'])
    {
        $result = "";
        if (!empty($review)) {

            $options['aside'] = isset($review->user) ? 'user' : 'software';

            $result .= $this->formatTemplate('review', [
                'reviewLi' => ($options["limit"] == true) ?
                    $this->templater()->formatAttributes(['class' => "col-xs-11 col-sm-12 col-md-6 col-lg-6"]) :
                    $this->templater()->formatAttributes(['class' => "col-md-offset-3 col-md-6"]),

                'aside' => $this->aside($review,$options['aside']),

                'LinkReviewPage' => $this->Html->link("",
                    ["prefix" => false, 'controller' => 'Softwares', 'action' => $review->software_id, "reviews", $review->id],
                    ["class" => "linkItem linkItemAllArea", 'escape' => false,]
                ),
                'attrsReview' => $this->templater()->formatAttributes(['class' => "blockReview"]),
                'title' => $this->Text->truncate($review->title, 50, ['ellipsis' => '...', 'exact' => false]),
                'AttrsBootstrap2' => $this->templater()->formatAttributes(['class' => "col-xs-9"]),

                'starDiv' => $this->templater()->formatAttributes(['class' => 'ratingStar']),
                'reviewComment' => ($options["limit"] == true) ? $this->Text->truncate($review->comment, 500, ['ellipsis' => '...', 'exact' => false]) : $review->comment,
            ]);
        }
        return $result;
    }


    public function aside($review,$type='user')
    {
        return
            ($type === 'user') ?
                $this->formatTemplate('aside', [
                    'AttrsBootstrap1' => $this->templater()->formatAttributes(['class' => "col-xs-3 AsideReview "]),
                    'Avatar'=>$this->User->displayLogoLink($review->user),
//                    'Attrsname' => $this->templater()->formatAttributes(['class' => "AsideReview "]),
                    'name' => $this->Html->link($this->Text->truncate($review->user->username, 35, ['ellipsis' => '...', 'exact' => false]),
                        ['prefix' => false, 'controller' => 'Users', "action" => $review->user->id],
                        ['escape' => false, "title" => __d("Users", "User name : ") . $review->user->username]),
                    'created' => $this->Time->format(strtotime($review->created),
                        [\IntlDateFormatter::SHORT, -1],
                        I18n::locale()
                    ),
                    'stars' => $this->Rating->display($review->evaluation),
                ]) :
                $this->formatTemplate('aside', [
                    'AttrsBootstrap1' => $this->templater()->formatAttributes(['class' => "col-xs-3 AsideReview "]),
                    'Avatar' => $this->Software->displayLogoLink($review->software),
                    'name' => $this->Html->link($review->software->softwarename,
                        ['prefix' => false, 'controller' => 'Softwares', "action" => $review->software->id],
                        ['escape' => false, "title" => __d("Softwares", "User name : ") . $review->software->softwarename]),
                    'created' => $this->Time->format(strtotime($review->created),
                        [\IntlDateFormatter::SHORT, -1],
                        I18n::locale()
                    ),
                    'stars' => $this->Rating->display($review->evaluation),
                ]);
    }
}

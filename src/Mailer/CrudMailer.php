<?php

namespace App\Mailer;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Mailer\Mailer;

class CrudMailer extends Mailer
{
    public function created(Event $event, $className)
    {
        $this
            ->to("barman@comptoir-du-libre.org")
            ->from("barman@comptoir-du-libre.org")
            ->subject(__d("Email","New {0} on Comptoir du libre : {1} ",[$className,Configure::read('App.fullBaseUrl')]))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
//            ->layout('custom')
            ->transport('default')
            ;

        $this->viewVars(
            [
                'message' =>
                    __d("Email","Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email","DATA : " . $event->data)]);
    }

    public function deleted(Event $event, $className)
    {
        $this
            ->to("barman@comptoir-du-libre.org")
            ->from("barman@comptoir-du-libre.org")
            ->subject(__d("Email",'A {0} was deleted on {1}', [$className,Configure::read('App.fullBaseUrl')]))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
//            ->layout('custom')
            ->transport('default')
        ;

        $this->viewVars(
            [
                'message' =>
                    __d("Email","Something happend on Comptoir du Libre on : {0} ",[Configure::read('App.fullBaseUrl')])
                    .
                    __d("Email","DATA : " . $event->data)]);
    }

    public function modifiedSoftware(Event $event, $userName = "a user",$className)
    {
        $this
            ->to("barman@comptoir-du-libre.org")
            ->from("barman@comptoir-du-libre.org")
            ->subject(__d("Email","Modified {0}: {1} by {2} on Comptoir du libre : {3} ",[$className,$event->data["softwarename"],$userName,Configure::read('App.fullBaseUrl')]))
            ->template('default')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
//            ->layout('custom')
            ->transport('default')
        ;

        $this->viewVars(
            [
                'message' =>
                    __d("Email","Something happend on Comptoir du Libre : " . Configure::read('App.fullBaseUrl') . " ")
                    .
                    __d("Email","DATA : " . $event->data)]);
    }

}

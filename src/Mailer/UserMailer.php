<?php

namespace App\Mailer;

use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Mailer\Mailer;

class UserMailer extends Mailer
{
    public function resetPassword($user)
    {
        $this
            ->to($user->email)
            ->from("barman@comptoir-du-libre.org")
            ->subject('Reset password')
            ->template('reset_password')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            ->set(
                [
                    'userName' => $user->username,
                    'message' => __d("Email","Please follow this link to reset your password : {0} \n",Configure::read('App.fullBaseUrl') . DS. "users".DS."changePassword".DS.$user->token),
                ]
            )
            ->transport('default');
    }



    /**
     * Send a email to an user in Comptoir du Libre
     * @param Event $event
     */
    public function contactViaForm(Event $event)
    {

        $this
            ->to($event->data['recipient'])
            ->from("barman@comptoir-du-libre.org")
            ->subject(Configure::read("Mail.Contact.UserToUser.Prefix").$event->data["subject"])
            ->replyTo($event->data['email'])
            ->template('contactViaForm')// Par défaut le template avec le même nom que le nom de la méthode est utilisé.
            ->set (
                [
                    'message' => $event->data["text"],
                ]
            )
            ->transport('default');
    }
}

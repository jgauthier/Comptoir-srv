<?php

namespace App\Network\Exception;

use Cake\Network\Exception\NotFoundException;

class RelationshipNotFoundException extends NotFoundException {

}

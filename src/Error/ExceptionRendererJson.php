<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Error;
use Cake\Error\ExceptionRenderer;
use App\Controller\ErrorsController;

class ExceptionRendererJson extends ExceptionRenderer {
    
   protected function _getController()
    {
        return new ErrorsController();
    }

}
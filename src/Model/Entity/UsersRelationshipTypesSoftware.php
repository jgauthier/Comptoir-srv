<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsersRelationshipTypesSoftware Entity.
 *
 * @property int $software_id
 * @property \App\Model\Entity\Software $software
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $relationship_type_id
 * @property \App\Model\Entity\RelationshipType $relationship_type
 * @property \Cake\I18n\Time $creadted
 * @property \Cake\I18n\Time $modified
 */
class UsersRelationshipTypesSoftware extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'software_id' => false,
        'user_id' => false,
    ];
}

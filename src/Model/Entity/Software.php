<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Collection\Collection;

/**
 * Software Entity.
 *
 * @property int $id
 * @property string $softwarename
 * @property string $url_repository
 * @property string $description
 * @property int $licence_id
 * @property \App\Model\Entity\License $license
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $logo_directory
 * @property $photo
 * @property \App\Model\Entity\Review[] $reviews
 * @property \App\Model\Entity\Screenshot[] $screenshots
 * @property \App\Model\Entity\Relationship[] $relationships
 */
class Software extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'tag_string'=>true,
    ];

    protected $_hidden = [
        '_joinData',
        'web_site_url'
    ];

    protected $_virtual = ['average_review','tag_string'];

    protected function _getTagString()
    {
        if (isset($this->_properties['tag_string'])) {
            return $this->_properties['tag_string'];
        }
        if (empty($this->tags)) {
            return '';
        }
        $tags = new Collection($this->tags);
        $str = $tags->reduce(function ($string, $tag) {
            return $string . $tag->name . ' ';
        }, '');

        return trim($str, ' ');
    }

    protected function _getAverageReview() {
        $total = 0 ;
        $average = 0;
        if (isset($this->_properties['reviews']) && !empty($this->_properties['reviews'])){
            foreach ($this->_properties['reviews'] as $review){
                $total += $review->evaluation;
            };
            $average = round($total / count($this->_properties['reviews']));
        }

        return $average ;
    }
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


/**
 * Tag Entity
 *
 * @property int $id
 * @property string $name
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Software[] $softwares
 */
class Tag extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];


    protected $_virtual =  [
        'usedTagNumber' // indicate the number of time where one tag is used on a software
    ];

    protected $_hidden = [
        '_joinData',
    ];

/**
 * TagsTable get: count the number of softwares related with a tag.
 *
 **/
    protected function _getUsedTagNumber() {
        return isset ($this->_properties['softwares']) ? count($this->_properties['softwares']): 0;
    }
}

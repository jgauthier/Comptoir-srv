<?php
namespace App\Model\Table;



use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Relationships Model
 *
 * @property \Cake\ORM\Association\BelongsTo $RelationshipTypes
 * @property \Cake\ORM\Association\HasMany $RelationshipsSoftwaresUsers
 * @property \Cake\ORM\Association\BelongsToMany $Softwares
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class RelationshipsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('relationships');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('RelationshipTypes', [
            'foreignKey' => 'relationship_type_id'
        ]);
        $this->hasMany('RelationshipsSoftwaresUsers', [
            'foreignKey' => 'relationship_id'
        ]);
        $this->belongsToMany('Softwares', [
            'foreignKey' => 'relationship_id',
            'targetForeignKey' => 'software_id',
            'joinTable' => 'relationships_softwares'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'relationship_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'relationships_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('cd');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['relationship_type_id'], 'RelationshipTypes'));
        return $rules;
    }
}

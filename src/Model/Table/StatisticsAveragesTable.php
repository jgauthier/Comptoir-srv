<?php
namespace App\Model\Table;



use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * StatisticsAverages Model
 *
 * @method \App\Model\Entity\StatisticsAverage get($primaryKey, $options = [])
 * @method \App\Model\Entity\StatisticsAverage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\StatisticsAverage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\StatisticsAverage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\StatisticsAverage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\StatisticsAverage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\StatisticsAverage findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StatisticsAveragesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('statistics_averages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('delta_commit_one_month')
            ->requirePresence('delta_commit_one_month', 'create')
            ->notEmpty('delta_commit_one_month');

        $validator
            ->numeric('delta_commit_twelve_month')
            ->requirePresence('delta_commit_twelve_month', 'create')
            ->notEmpty('delta_commit_twelve_month');

        $validator
            ->numeric('number_of_contributors')
            ->requirePresence('number_of_contributors', 'create')
            ->notEmpty('number_of_contributors');

        return $validator;
    }
}

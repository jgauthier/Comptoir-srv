<?php
namespace App\Model\Table;



use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


use Cake\Event\Event;
/**
 * Softwares Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Licenses
 * @property \Cake\ORM\Association\HasMany $RelationshipsSoftwaresUsers
 * @property \Cake\ORM\Association\HasMany $Reviews
 * @property \Cake\ORM\Association\HasMany $Screenshots
 * @property \Cake\ORM\Association\HasMany $Tags
 * @property \Cake\ORM\Association\HasMany $SoftwaresStatistics
 * @property \Cake\ORM\Association\BelongsToMany $Relationships
 */
class SoftwaresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        //----------------------------------------------------------------------
        //-------------------------Logique BDD--------------------------
        //----------------------------------------------------------------------


        $this->table('softwares');
        $this->displayField('softwarename');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        //For upload file -> avatar
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'photo' => [
                'fields' => [
                    'dir' => 'logo_directory',
                ],
                'filesystem' => [
                    'root' => WWW_ROOT. 'img'. DS,
                ],
                'path' => 'files{DS}{model}{DS}{field-value:softwarename}{DS}avatar',
            ],
        ]);

        $this->belongsTo('Licenses', [
            'foreignKey' => 'licence_id'
        ]);

        $this->hasMany('RelationshipsSoftwaresUsers', [
            'foreignKey' => 'software_id'
        ]);
        $this->hasMany('Reviews', [
            'foreignKey' => 'software_id'
        ]);
        $this->hasMany('Screenshots', [
            'foreignKey' => 'software_id'
        ]);
        $this->hasOne('SoftwaresStatistics', [
            'foreignKey' => 'software_id'
        ]);
        $this->belongsToMany('Relationships', [
            'foreignKey' => 'software_id',
            'targetForeignKey' => 'relationship_id',
            'joinTable' => 'relationships_softwares'
        ]);

        $this->belongsToMany('Tags', [
            'foreignKey' => 'software_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'softwares_tags'
        ]);

        //----------------------------------------------------------------------
        //-------------------------Logique applicative--------------------------
        //----------------------------------------------------------------------

        /**
         *
         */
        $this->hasMany(
            'Userssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => ['relationship_id' => 7,],
                'finder' => "usersForSoftwares"
            ]
        );

        /**
         * Backed softwares
         */
        $this->hasMany(
            'Backerssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => ['relationship_id' => 8],
                'finder' => "usersForSoftwares"
            ]
        );
        $this->hasMany(
            'Creatorssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => ['relationship_id' => 3],
                'finder' => "usersForSoftwares"
            ]
        );
        /**
         * Contributior to
         */
        $this->hasMany(
            'Contributorssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => [
                    'relationship_id' => 9
                ],
                'finder' => "usersForSoftwares"
            ]
        );
        /**
         * Provider For
         */
        $this->hasMany(
            'Providerssoftwares',
            [
                'className' => 'RelationshipsSoftwaresUsers',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'user_id',
                'joinTable' => 'relationships_softwares_users',
                'conditions' => ["OR"=> [["relationship_id"=> 4],["relationship_id" => 10]]],
                'finder' => "usersForSoftwares",
            ]

        );

        /**
         * Works Well with
         */
        $this->hasMany(
            'workswellsoftwares',
            [
                'className' => 'RelationshipsSoftwares',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'recipient_id',
                'joinTable' => 'relationships_softwares',
                'conditions' => [
                    'relationship_id' => 6
                ]
            ]
        );

        /**
         * Alternative to
         */
        $this->hasMany(
            'alternativeto',
            [
                'className' => 'RelationshipsSoftwares',
                'foreignKey' => 'software_id',
                'targetForeignKey' => 'recipient_id',
                'joinTable' => 'relationships_softwares',
                'conditions' => [
                    'relationship_id' => 5
                ]
            ]
        );



        //  #######################
        //  ##### For search ######
        //  #######################

                // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('id')

            ->add('search', 'Search.Like', [
                'before' => true,
                'after' => true,
                'mode' => 'OR',
                'comparison' => 'ILIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => [$this->aliasField('softwarename'), $this->aliasField('description')]
            ])
            ->add('screenCaptured', 'Search.Callback', [
                'callback' => function(Query $query, $args, $manager) {
                    if ($args["screenCaptured"]=="true"){
                        return $query->andWhere(["Softwares.id IN "=> $this->find("screenCaptured")->select("Screenshots.software_id")]);

                    }
                }
            ])
            ->add('reviewed', 'Search.Callback', [
                'callback' => function(Query $query, $args, $manager) {
                    if ($args["reviewed"]=="true") {
                        return $query->andWhere(["Softwares.id IN " => $this->find("reviewed")->select("Reviews.software_id")]);
                    }
                }
            ])
            ->add('hasServiceProvider', 'Search.Callback', [
                'callback' => function(Query $query, $args, $manager) {
                    if ($args["hasServiceProvider"]=="true") {
                        return $query->andWhere(["Softwares.id IN " => $this->find("hasServiceProvider")->select("RelationshipsSoftwaresUsers.software_id")]);
                    }
                }
            ])
            ->add('hasUser', 'Search.Callback', [
                'callback' => function(Query $query, $args, $manager) {
                    if ($args["hasUser"]=="true") {
                        return $query->andWhere(["Softwares.id IN " => $this->find("hasUser")->select("RelationshipsSoftwaresUsers.software_id")]);
                    }
                }
            ])
        ;
    }

    /*
     * Create a virtual field base on average of all reviews of a software
     */
    public function findAverage(\Cake\ORM\Query $query, array $options)
    {
         $query->select(
             ['average_review' => $query->func()->coalesce([$query->func()->avg('Reviews.evaluation'),0])]
        )
                ->leftJoinWith('Reviews')
                ->group(['Softwares.id',"Licenses.id","Reviews.id"])
                ->autoFields(true);
        return $query;
    }

    public function findScreenCaptured(Query $query, array $options)
    {
        $softwares = $this->find()
            ->innerJoinWith("Screenshots")
            ->where(["OR"=>[
                "Screenshots.photo IS NOT"=>NULL,
                "Screenshots.url_directory IS NOT"=>NULL]
            ])
            ;

        return $softwares->group(['Screenshots.software_id','Softwares.id']);
    }



    public function findReviewed(Query $query, array $options)
    {
        $softwares = $this->find()
            ->innerJoinWith("Reviews")
            ->where(["OR"=>
                [   "Reviews.title IS NOT "=>NULL,
                    "Reviews.evaluation IS NOT "=>NULL,
                    "Reviews.comment IS NOT "=>NULL]])
        ;

        return $softwares->group(['Reviews.software_id',"Softwares.id"]);
    }

    public function findHasServiceProvider(Query $query, array $options)
    {
        $softwares = $this->find()
            ->innerJoinWith("RelationshipsSoftwaresUsers")
            ->where(["OR"=> [["relationship_id"=> 4],["relationship_id" => 10]]])
        ;

        return $softwares->group(['RelationshipsSoftwaresUsers.software_id',"Softwares.id"]);
    }

    public function findHasUser(Query $query, array $options)
    {
        $softwares = $this->find()
            ->innerJoinWith("RelationshipsSoftwaresUsers")
            ->where(["relationship_id"=> 7])
        ;

        return $softwares->group(['RelationshipsSoftwaresUsers.software_id',"Softwares.id"]);
    }

    /**
     * Create an event after save an software
     * @param $created
     * @param array $options
     * @return True if created False otherwise
     */
    public function afterSave(Event $created, $options = array()) {

        if ($options->isNew()) {
            $event = new Event('Model.Software.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }else{
            $event = new Event('Model.Software.modified', $this, $options);
            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('softwarename')
            ->notEmpty("softwarename");

        $validator
            ->requirePresence('url_repository', 'create')
            ->notEmpty('url_repository')
            ->url('url_repository');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('logo_directory');

        $validator
            ->requirePresence('photo','create')
            ->allowEmpty('photo','update');

        $validator->provider('upload',new \Josegonzalez\Upload\Validation\DefaultValidation());

        $validator->add('photo','file',[
            'rule' => ['mimeType', ['image/jpeg', 'image/png','image/gif','image/svg+xml']],
            'message' => __d('Forms',"This format is not allowed. Please use one in this list: JPEG, PNG, GIF, SVG.")
        ]);

        $validator->add('photo', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 1048576],
            'message' => __d('Forms','The maximum weight allowed is 1MB, please use a file less than 1MB.'),
            'provider' => 'upload',
        ]);

        //Check that the file is below the maximum height requirement (checked in pixels)
        $validator->add('photo', 'fileBelowMaxHeight', [
            'rule' => ['isBelowMaxHeight', 350],
            'message' => __d('Forms','The size of your image is too big, please use an image fitting a 350x350px size.'),
            'provider' => 'upload',
            'on' => function ($context) {
                return in_array($context['data']['photo']['type'],['image/jpeg', 'image/png','image/gif']);
            },
            'last'=> true,
        ]);

        //Check that the file is below the maximum width requirement (checked in pixels)
        $validator->add('photo', 'fileBelowMaxWidth', [
            'rule' => ['isBelowMaxWidth', 350],
            'message' => __d('Forms','The size of your image is too big, please use an image fitting a 350x350px size.'),
            'provider' => 'upload',
            'on' => function ($context) {
                return in_array($context['data']['photo']['type'],['image/jpeg', 'image/png','image/gif']);
            }
        ]);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['softwarename'],__d("Forms","A software with this name already exists in Comptoir du libre.")));
        $rules->add($rules->isUnique(['url_repository'],__d("Forms","A software with this repository already exists in Comptoir du libre.")));
        $rules->add($rules->isUnique(['url_website'],__d("Forms","A software with this url already exists in Comptoir du libre.")));
        $rules->add($rules->existsIn(['licence_id'], 'Licenses'));
        $rules->add($rules->validCount('tags', 6, '<=', __d("Forms", 'You can\'t have more than 5 tags on a software')));

        return $rules;
    }

    /**
     * Returns a software's list which tagged whit parameters in 'array $options'
     */
    public function findTagged(Query $query, array $options)
    {
        $softwares = $this->find()
            ->select(['id', 'softwarename']);


        if (empty($options['tags'])) {
            $softwares->leftJoinWith('Tags', function ($q) {
                return $q->where(['Tags.name IS ' => null]);
            });
        } else {
            $softwares->innerJoinWith('Tags', function ($q) use ($options) {
                return $q->where(['Tags.name IN' => $options['tags']]);
            });
        }
        return $softwares->group(['Softwares.id']);
    }

    /**
     */
    public function beforeRules( $event, $entity, $options){
        if ($entity->tag_string) {
            $entity->tags = $this->_buildTags($entity->tag_string);
        }
    }

    /**
     * to parse the tag string and find/build the related entities
     *
     * @param $tagString
     * @return array
     */
    protected function _buildTags($tagString)
    {
        // Trim tags
        $newTags = array_map('trim', explode(' ', $tagString));
        // Remove all empty tags
        $newTags = array_filter($newTags);
        // Reduce duplicated tags
        $newTags = array_unique($newTags);

        $out = [];
        $query = $this->Tags->find()
            ->where(['Tags.name IN' => $newTags]);

        // Remove existing tags from the list of new tags.
        foreach ($query->extract('name') as $existing) {
            $index = array_search($existing, $newTags);
            if ($index !== false) {
                unset($newTags[$index]);
            }
        }
        // Add existing tags.
        foreach ($query as $tag) {
            $out[] = $tag;
        }
        // Add new tags.
        foreach ($newTags as $tag) {
            $out[] = $this->Tags->newEntity(['name' => $tag]);
        }
        return $out;
    }
}

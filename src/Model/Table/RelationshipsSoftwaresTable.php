<?php
namespace App\Model\Table;



use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RelationshipsSoftwares Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Softwares
 * @property \Cake\ORM\Association\BelongsTo $Relationships
 * @property \Cake\ORM\Association\BelongsToMany $Users
 */
class RelationshipsSoftwaresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('relationships_softwares');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Softwares', [
            'foreignKey' => 'software_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Relationships', [
            'foreignKey' => 'relationship_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Softwares', [
            'foreignKey' => 'recipient_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Users', [
            'foreignKey' => 'software_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'relationships_softwares_users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->existsIn(['relationship_id'], 'Relationships'));
        $rules->add($rules->existsIn(['recipient_id'], 'Softwares'));
        $rules->add($rules->isUnique(
            ['software_id','recipient_id','relationship_id'],
            __d("Forms",'You can not declare twice the same relationships.')
        ));
        return $rules;
    }
}

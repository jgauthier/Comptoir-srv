<?php
namespace App\Model\Table;



use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;


/**
 * Reviews Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Softwares
 */
class ReviewsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('reviews');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Softwares', [
            'foreignKey' => 'software_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->range('evaluation',[1,4]);

        return $validator;
    }

    /**
     * @param Event $created
     * @param array $options
     * @return bool
     */
    public function afterSave(Event $created, $options = array()) {

        if ($options->isNew()) {
            $event = new Event('Model.Review.created', $this, $options);

            $this->eventManager()->dispatch($event);

            return true;
        }
        return false;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->isUnique(['software_id','user_id'],__d("Forms","You can not post more than one review for a software.")));

        return $rules;
    }
}

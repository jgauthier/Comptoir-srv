<?php
namespace App\Model\Table;



use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SoftwaresStatistics Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Softwares
 */
class SoftwaresStatisticsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('softwares_statistics');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Softwares', [
            'foreignKey' => 'software_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('last_commit_age')
            ->allowEmpty('last_commit_age');

        $validator
            ->integer('project_age')
            ->allowEmpty('project_age');

        $validator
            ->integer('delta_commit_one_month')
            ->allowEmpty('delta_commit_one_month');

        $validator
            ->integer('delta_commit_one_year')
            ->allowEmpty('delta_commit_one_year');

        $validator
            ->integer('nb_contributors')
            ->allowEmpty('nb_contributors');

        $validator
            ->decimal('contributors_code_percent')
            ->allowEmpty('contributors_code_percent');

        $validator
            ->integer('declared_users')
            ->allowEmpty('declared_users');

        $validator
            ->decimal('average_review_score')
            ->allowEmpty('average_review_score');

        $validator
            ->integer('screenshots')
            ->allowEmpty('screenshots');

        $validator
            ->integer('codegouv_label')
            ->allowEmpty('codegouv_label');

        $validator
            ->decimal('score')
            ->allowEmpty('score');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        return $rules;
    }
}

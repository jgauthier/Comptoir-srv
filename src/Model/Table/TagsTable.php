<?php
namespace App\Model\Table;




use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Network\Exception\TagNotFoundException;


/**
 * Tags Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Softwares
 *
 * @method \App\Model\Entity\Tag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tag|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tag findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tags');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsToMany('Softwares', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'software_id',
            'joinTable' => 'softwares_tags'
        ]);
    }

//    /**
//     * to register name as lower-case and without special characters
//     *
//     * @param Event $event
//     * @param ArrayObject $data
//     * @param ArrayObject $options
//     */
//
//    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
//    {
//        if (isset($data['name'])) {
//            $data['name'] = mb_(\Cake\Utility\Text::transliterate($data['name'] ));
//        }
//    }

    /**
     * Default validation rules.
     * Is allow : alphanumeric character a->z and A->Z and "-" "_" only.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->maxLength('name', 100, "Provided value is too long (max : 100).");

        $validator->add('name',
            'alphaNumeric',
            [
                'rule' => [
                    'custom', '/^[a-zA-Z0-9_\-]+$/'],
                'message' => __d("Tags", "Name must be an alphaNumeric value. You provided an invalid value.")

            ]); // TODO I18n ?

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name'], __d("Tags", "A tag with this name already exists in Comptoir du libre."))); // TODO I18n ?
        return $rules;
    }


    /**
     * getTag search tag's data with an id. If this tag (id) does not exist error message will be : The Tag with the id $id does not exist.
     *
     * @override
     * @param $id
     * @return \Cake\Datasource\EntityInterface|mixed
     */
    public function get($id, $options = [])
    {
        try {
            $tag = parent::get($id, [
                'contain' => [
                    'Softwares' => [
                        'Tags',
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            throw new TagNotFoundException(__d("Tags", "The Tag with the id $id does not exist"));
        }
        return $tag;
    }

}

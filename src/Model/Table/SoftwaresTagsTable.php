<?php
namespace App\Model\Table;


use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SoftwaresTags Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Softwares
 * @property \Cake\ORM\Association\BelongsTo $Tags
 *
 * @method \App\Model\Entity\SoftwaresTag get($primaryKey, $options = [])
 * @method \App\Model\Entity\SoftwaresTag newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SoftwaresTag[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SoftwaresTag|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SoftwaresTag patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SoftwaresTag[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SoftwaresTag findOrCreate($search, callable $callback = null, $options = [])
 */
class SoftwaresTagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('softwares_tags');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Softwares', [
            'foreignKey' => 'software_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Tags', [
            'foreignKey' => 'tag_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     * we want single couple software_id and tag_id
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['software_id'], 'Softwares'));
        $rules->add($rules->existsIn(['tag_id'], 'Tags'));

        $rules->add($rules->isUnique(
            ['software_id','tag_id'],
            __d("Forms",'This relation between Tags and Softwares is already declared.') //TODO trad
        ));

        return $rules;
    }
}

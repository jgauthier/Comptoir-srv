<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * Reviews Controller
 *
 * @property \App\Model\Table\ReviewsTable $Reviews
 */
class ReviewsController extends AppController
{

    public function isAuthorized($user)
    {
        if ($this->Auth->user()) {
            if ($this->request->action === 'add') {
                $this->loadModel("Users");
                return $this->Users->isAdministration($this->Auth->user('id'));
            }
        }
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
//        $this->Auth->allow('add');
        $this->Auth->deny('delete');
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {

        if (isset($this->request->params['software_id'])) {
            $this->viewBuilder()->template("reviews_software");
            $software = $this->Reviews->Softwares->find("all")->select(["id", "softwarename"])->where(["id" => $this->request->params['software_id']])->firstOrFail();
            $this->set('software', $software);
            $this->set('_serialize', ['software']);

        }

        $this->paginate = [
            'conditions' => isset($this->request->params['software_id']) ? [
                'Softwares.id ' => $this->request->params['software_id']
            ] : [],
            'contain' => [
                'Users' => ["fields" => ["id", "username", 'logo_directory','photo',]],
                'Softwares' => ["fields" => ["id", "softwarename"]]
            ]
        ];

        $this->set('reviews', $this->paginate($this->Reviews));
        $this->set('_serialize', ['reviews']);
    }

    /**
     * View method
     *
     * @param string|null $id Review id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

        $review = $this->Reviews->get($id, [
            'contain' => [
                'Users' => [
                    'fields' => [
                        'id',
                        'username',
                        'logo_directory',
                        'photo',
                        'description'
                    ]
                ],
                'Softwares' => [
                    "Reviews",
                    'fields' => [
                        'id',
                        'softwarename',
                        'url_website',
                        'url_repository',
                        'logo_directory',
                        'photo',
                        'description',
                    ]
                ]
            ]
        ]);
        $this->set('review', $review);
        $this->set('_serialize', ['review']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post', 'get']);

        $review = $this->Reviews->newEntity();
        $message = "";
        if ($this->request->is('post')) {

            try {
                $software = $this->Reviews->Softwares->get($this->request->params['software_id']);
                $this->request->data['software_id'] = $software->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\SoftwareNotFoundException("The software with the id " . $this->request->params['software_id'] . " does not exist");
            }

            try {
                $user = $this->Reviews->Users->get($this->Auth->user('id'));
                $this->request->data['user_id'] = $user->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
            }


            $review = $this->Reviews->patchEntity($review, $this->request->data);

            if ($this->Reviews->save($review)) {
                $message = "Success";
                if (!$this->request->is('json')) {
                    $this->Flash->success(__d("Forms", "Your review has been posted"));
                    $this->redirect("softwares/" . $this->request->data['software_id']);
                }
            } else {
                if (!$this->request->is('json')) {
                    $this->Flash->error(__d("Forms", "You can not post more than one review for a software."));
                    $this->redirect("softwares/" . $this->request->data['software_id']);
                } else {
                    $message = "Error";
                    $review = $review->errors();
                }


            }
        }
        $this->set([
            'message' => $message,
            'review' => $review,
            '_serialize' => ['message', 'review']
        ]);

    }

    /**
     * Edit method
     *
     * @param string|null $id Review id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $review = $this->Reviews->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $review = $this->Reviews->patchEntity($review, $this->request->data);
            if ($this->Reviews->save($review)) {
                $this->Flash->success(__('The review has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The review could not be saved. Please, try again.'));
            }
        }
        $users = $this->Reviews->Users->find('list', ['limit' => 200]);
        $softwares = $this->Reviews->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('review', 'users', 'softwares'));
        $this->set('_serialize', ['review']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Review id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $review = $this->Reviews->get($id);
        if ($this->Reviews->delete($review)) {
            $this->Flash->success(__('The review has been deleted.'));
        } else {
            $this->Flash->error(__('The review could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

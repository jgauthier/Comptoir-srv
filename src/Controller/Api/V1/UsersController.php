<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\Core\Configure;
use App\Controller\Component\RulesComponent;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\Query;
use Cake\Utility\Text;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    /**
     * Manage all rights for the controllers' actions.
     * Returns true if the user can use the currrent action, FALSE otherwise.
     * Returns true for add a project if the user is connected
     * Returns true for edit and delete action if the user is owner.
     * @param Array $user
     * @return boolean
     */
    public function isAuthorized($user)
    {
        //Access to edit method only for the current user
        if ($this->request->param('action') === 'edit') {
            if ($this->Auth->user('id') == $this->request->params["pass"][0]) {
                return true;
            }
        }

        if ($this->request->param('action') === 'changePassword') {
            $tokenOrId = $this->request->params["pass"][0];

            $user = $this->Users->findById($tokenOrId, ["contain" => []])->first();

            if ($this->Auth->user('id') == $user->id || $this->Users->findByToken($tokenOrId, ["contain" => []])->first()) {
                return true;
            }
        }

        //If user is auth
        if ($this->request->param('action') === 'register') {
            if ($user) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    public function initialize()
    {
        parent::initialize();
        $this->paginate = [
            'limit' => Configure::read('LIMIT'),
            'maxLimit' => Configure::read('LIMIT'),
            'order' => [
                'Users.username' => Configure::read('ORDER'),
            ],
            'fields' => [
                'id', 'username', 'email', 'user_type_id', 'url', 'description', 'photo', 'logo_directory', 'UserTypes.name'
            ],
            'contain' => ['UserTypes']
        ];

    }

    /**
     * Manage access before filter by user's right
     * @param \Cake\Event\Event $event
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        $this->Auth->allow([
            'add',
            'providers',
            'administrations',
            'nonProfitUsers',
            'providerforSoftwares',
            'usedSoftwares',
            'forgotPassword',
            'changePassword',
            'contact',
        ]);
        $this->Auth->deny(['delete', 'edit', 'register']);
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if (!empty($this->request->query) && isset($this->request->query["order"])) {
            $sort = explode(".", $this->request->query["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }

        $users = $this->Users
            ->find('search',
                [
                    'sortWhitelist' => [
                        'username',
                        'average_review',
                        'created',
                        'modified',
                    ],
                    'search' => $this->request->query,
                    'contain' => ['UserTypes'],
                    'order' => isset ($this->request->query["sort"]) && isset($this->request->query["direction"]) ?
                        ['Users.'.$this->request->query["sort"] => $this->request->query["direction"]] :
                        ['Users.username' => Configure::read('ORDER')]
                    ,
                ]);

        //For filters
        $hadReview = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnReview"),
            "true" => __d("Forms", "Search.Filter.Users.WithReview"),
        ];

        $userType = [
            "all" => __d("Forms", "Search.Filter.Users.NoFilterOnUserType"),
            "Administration" => __d("Forms", "Search.Filter.Users.userTpye.Administration"),
            "Person" => __d("Forms", "Search.Filter.Users.userTpye.Person"),
            "Company" => __d("Forms", "Search.Filter.Users.userTpye.Company"),
            "Association" => __d("Forms", "Search.Filter.Users.userTpye.Association"),
        ];

        $isUserOf = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnIsUserOf"),
            "true" => __d("Forms", "Search.Filter.Users.IsUserOf"),
        ];

        $isSerivicesProvider = [
            "false" => __d("Forms", "Search.Filter.Users.NoFilterOnIsServicesProvider"),
            "true" => __d("Forms", "Search.Filter.Users.IsServicesProvider"),
        ];

        $order = [
            "username.asc" => __d("Forms", "Search.Sort.usernameAsc"),
            "username.desc" => __d("Forms", "Search.Sort.usernameDesc"),
            "created.asc" => __d("Forms", "Search.Sort.createdAsc"),
            "created.desc" => __d("Forms", "Search.Sort.createdDesc"),
            "modified.asc" => __d("Forms", "Search.Sort.modifiedAsc"),
            "modified.desc" => __d("Forms", "Search.Sort.modifiedDesc"),
        ];
        //End For filters

        $this->set('hadReview', $hadReview);
        $this->set('userType', $userType);
        $this->set('isUserOf', $isUserOf);
        $this->set('order', $order);
        $this->set('isSerivicesProvider', $isSerivicesProvider);


        $this->set([
            'users' => $this->paginate($users),
            '_serialize' => ['users']
        ]);

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [
                'UserTypes',
                'Reviews' => [
                    'strategy' => 'select',
                    'queryBuilder' => function ($q) {
                        return $q->order(['Reviews.created' => 'DESC']);
                    },
                    'Softwares' => [
                        'fields' => [
                            'id',
                            'softwarename',
                            'logo_directory',
                            'photo',
                        ]
                    ]
                ],
                'Usedsoftwares' => [
                    'strategy' => 'select',
                    'queryBuilder' => function (Query $q) {
                        return $q->order(['Softwares.softwarename' => 'ASC']);
                    },
                    'Softwares' => [
                        'Reviews' => [
                        ],
                        'fields' => [
                            'id',
                            'softwarename',
                            'logo_directory',
                            'photo',
                            'description',
                        ]
                    ]
                ],
                "Providerforsoftwares" => [
                    'strategy' => 'select',
                    'queryBuilder' => function ($q) {
                        return $q->order(['Softwares.softwarename' => 'ASC']);
                    },
                    'Softwares' => [
                        'Reviews' => [
                        ],
                        'fields' => [
                            'id',
                            'softwarename',
                            'logo_directory',
                            'photo',
                            'description'
                        ]
                    ],
                ],
            ]
        ]);

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => $user->username,
            "description" => $user->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => $user->username,
            "description" => $user->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $user->logo_directory . DS . $user->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        !empty($this->request->data) ? $user = $this->Users->newEntity($this->request->data) : $user = $this->Users->newEntity();

        $message = "";
        // Get the avatar before unset it to save the user.
        // The Upload plugin need an existing entity to attach a file to it.


        if ($this->request->is('post')) {

            if (isset ($this->request->data['photo']) && !$user->errors()) {
                $avatar = $this->request->data['photo'];
                $this->request->data['photo'] = "";
            }

            $user = $this->Users->patchEntity($user, $this->request->data);

            if ($this->Users->save($user)) {
                $user = $this->Users->get($user->id, ['contain' => []]);

                isset($avatar) ? $this->request->data['photo'] = $avatar : null;

                $user = $this->Users->patchEntity($user, $this->request->data);

                if ($this->Users->save($user)) {
                    $message = "Success";
                    $this->Flash->success(__d("Forms", "Your are registred on the Comptoir du Libre, welcome !"));
                    if (!$this->request->is('json')) {
                        $this->Auth->setUser($this->Auth->identify());
                        $this->redirect(["prefix" => false, "controller" => "Pages", "language" => $this->request->param("language")]);
                    }
                } else {
                    $message = "Error";
                }
            } else {
                $message = "Error";
                $this->Flash->error(__d("Forms", "Your registration failed, please follow rules in red."));
            }
            $message == "Error" ? $this->set('errors', $user->errors()) : null;
        }
        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message', 'errors']);

    }

    /**
     * Allow a user to request a password reset.
     * @return
     */
    function forgotPassword($token = null)
    {
        $user = $this->Users->newEntity();
        $message = "";
        if (!$token) {
            if (!empty($this->request->data)) {
                $user = $this->Users->find('byEmail', ['user' => $this->request->data["email"]])->first();

                if (!$user) {

                    $this->Flash->error(__d("Forms", 'Sorry, the email entered was not found.'));
                    $message = __d("Forms", 'Sorry, the email entered was not found.');
//                    $this->redirect('api/v1/users/forgot_password');

                } else {
                    $user = $this->__generatePasswordToken($user);

                    if ($this->Users->save($user) && $this->getMailer("User")->send("resetPassword", ["user" => $user])) {
                        $this->Flash->success('Password reset instructions have been sent to your email address.');
                        $this->redirect(["prefix" => false, 'controller' => "users", "action" => "login"]);
                    }
                }
            }
        }
        $this->set(compact('message', 'user'));
        $this->set('_serialize', ['message', 'user']);
    }

    /**
     * Generate a unique hash / token.
     * @param Object User
     * @return Object User
     */
    function __generatePasswordToken($user)
    {
        // Generate an 'token'
        $user->token_plain =
            Text::uuid()
        ;
        $user->token = $user->token_plain;
        return $user;
    }


    /**
     * @return array Returns an user entity or errors regarding the user entity, plus a message (Success or Error )
     */
    private function _changePasswordById($user)
    {
        $message = "";
        if ($this->request->is(['put', 'post'])) {

            if (!empty($this->request->data) && $user) {
                $user = $this->Users->patchEntity($user,
                    [
                        'old_password' => $this->request->data['old_password'],
                        'password' => $this->request->data['new_password'],
                        'confirm_password' => $this->request->data['confirm_password']
                    ],
                    ['validate' => 'password']
                );
                if ($this->Users->save($user)) {
                    $message = "Success";
                    $this->Flash->success(__d("Forms", "Your password has been changed."));
                } else {
                    $message = "Error";
                    $this->Flash->error(__d("Forms", "Your password can not be changed, please follow rules in red."));
                }

            }
        }
        return ["message" => $message, "user" => $user];

    }

    /**
     * Allow to an user to change his password using a token
     * @param $token A uniq token given via email
     * @return array
     */
    private function _changePasswordByToken($token)
    {
        $message = "";

        $user = $this->Users->newEntity();

        $this->viewBuilder()->template("reset_password");
        if ($this->request->is('POST') && $this->Users->findByToken($token, ["contain" => []])->first()) {
            $user = $this->Users->patchEntity($this->Users->findByToken($token, ["contain" => []])->first(),
                [
                    'password' => $this->request->data['new_password'],
                    'confirm_password' => $this->request->data['confirm_password']
                ]
            );
            if ($this->Users->save($user)) {
                $message = "Success";
                $this->Flash->success(__d("Forms", "Your password has been changed."));
            } else {
                $message = "Error";
                $this->Flash->error(__d("Forms", "Your password can not be changed, please follow rules in red."));
            }
        }
        return ["message" => $message, "user" => $user];

    }

    /**
     * Change the password for a current user.
     */
    public function changePassword($param = null)
    {
        $message = "";
        if ($this->Users->exists(["id" => $param])) {
            $user = $this->Users->get($param);
            $result = $this->_changePasswordById($user);
        } else {
            $result = $this->_changePasswordByToken($param);
        }
        $user = $result["user"];
        $result ? $message = $result["message"] : $message = "Error";

        $message == "Success" ? $this->redirect(["prefix" => false, "controller" => "Users", "action" => "edit", "language" => $this->request->param("language"), $user->id]) : null;


        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put']) && $this->request->is('json')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $message = "Success";
            } else {
                $message = "Error";
                $this->set('errors', $user->errors());
            }
        } else if ($this->request->is(['patch', 'post', 'put']) && !$this->request->is('json')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {

                $message = "success";

                $this->Auth->setUser($user); //update session

                $this->Flash->success(__d("Forms", "Edit.User.profile.success"));;
                $this->redirect(["prefix" => false, 'action' => 'index']);
            } else {
                $message = "Error";

                $this->Flash->error(__d("Forms", "Edit.User.profile.error"));
            }
        }

        $this->ValidationRules->config('tableRegistry', "Users");
        $rules = $this->ValidationRules->get();
        $userTypes = $this->Users->UserTypes->find('list', ['limit' => 200]);
        $this->set(compact('user', 'userTypes', 'rules', 'message'));
        $this->set('_serialize', ['user', 'userTypes', 'rules', 'message', 'errors']);
    }

    public function providers()
    {
        $this->viewBuilder()->template("users_list");
        $this->set("title", "List of service providers");

        try {
            $users = $this->Users->find("all")->contain(["UserTypes"])
                ->where(["UserTypes.cd = " => "Company"]);
            $this->set([
                'message' => "Success",
                'users' => $this->paginate($users),
                '_serialize' => ['message', 'users']
            ]);
        } catch (Exception $e) {

        }
    }


    /**
     * Return an user with all used softwares
     * @param integer $id
     */
    public function usedSoftwares($id = null)
    {

        $user = $this->Users->get($id,
            ["contain" =>
                [
                    'Usedsoftwares' =>
                        [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' =>
                                ['Reviews' => [
                                ],
                                    'fields' => [
                                        'id',
                                        'softwarename',
                                        'logo_directory',
                                        'photo',
                                        'description'
                                    ]
                                ]
                        ],
                    'UserTypes',
                ]
            ]);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    /**
     * Returns a user with a list of software as know as services provider
     * @param integer $id
     */
    public function providerforSoftwares($id = null)
    {

        $user = $this->Users->get($id,
            ["contain" =>
                [
                    'UserTypes',
                    'Providerforsoftwares' =>
                        [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'Reviews' => [
                                ],
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ]
                ]
            ]);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function contact($id)
    {

        $currentUser = $this->Auth->user('id') ? $this->Users->get($this->Auth->user('id')) : $this->Users->newEntity();
        $user = $this->Users->get($id, ["fields" => ["email", "username"]]);

        if (!$this->request->is('json') && $this->request->is('post') && !empty($this->request->data)) {
            $event = new Event('Model.User.contact', $this->Users->newEntity(), $this->request->data);
            $event->data["recipient"] = $user->email;

            if ($this->eventManager()->dispatch($event)) {
                $this->Flash->success(__d("Forms", "Users.contact.success"));
            } else {
                $this->Flash->error(__d("Forms", "Users.contact.error"));
            }
        }

        $this->set(compact(['currentUser', 'user']));
        $this->set('_serialize', ['user', 'currentUser']);


    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $user["user_type"] = $this->Users->UserTypes->get($user["user_type_id"])->get("name");
                $this->Auth->setUser($user);
                $this->Flash->success(__d("Forms", "You are logged"));
                return $this->redirect($this->Auth->redirectUrl("/"));
            }
            $this->Flash->error(__d("Forms", "You are not logged"));
        }
    }
}

<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;

/**
 * SoftwaresTags Controller
 *
 * @property \App\Model\Table\SoftwaresTagsTable $SoftwaresTags
 */
class SoftwaresTagsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Softwares', 'Tags']
        ];
        $softwaresTags = $this->paginate($this->SoftwaresTags);

        $this->set(compact('softwaresTags'));
        $this->set('_serialize', ['softwaresTags']);
    }

    /**
     * View method
     *
     * @param string|null $id Softwares Tag id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $softwaresTag = $this->SoftwaresTags->get($id, [
            'contain' => ['Softwares', 'Tags']
        ]);

        $this->set('softwaresTag', $softwaresTag);
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $softwaresTag = $this->SoftwaresTags->newEntity();
        if ($this->request->is('post')) {
            $softwaresTag = $this->SoftwaresTags->patchEntity($softwaresTag, $this->request->data);
            if ($this->SoftwaresTags->save($softwaresTag)) {
                $this->Flash->success(__d('The softwares tag has been saved.'));  //  TODO i18n

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('The softwares tag could not be saved. Please, try again.')); // TODO i18n
            }
        }
        $softwares = $this->SoftwaresTags->Softwares->find('list', ['limit' => 200]);
        $tags = $this->SoftwaresTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('softwaresTag', 'softwares', 'tags'));
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Softwares Tag id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $softwaresTag = $this->SoftwaresTags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $softwaresTag = $this->SoftwaresTags->patchEntity($softwaresTag, $this->request->data);
            if ($this->SoftwaresTags->save($softwaresTag)) {
                $this->Flash->success(__d('The softwares tag has been saved.')); // TODO i18n

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('The softwares tag could not be saved. Please, try again.')); // TODO i18n
            }
        }
        $softwares = $this->SoftwaresTags->Softwares->find('list', ['limit' => 200]);
        $tags = $this->SoftwaresTags->Tags->find('list', ['limit' => 200]);
        $this->set(compact('softwaresTag', 'softwares', 'tags'));
        $this->set('_serialize', ['softwaresTag']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Softwares Tag id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $softwaresTag = $this->SoftwaresTags->get($id);
        if ($this->SoftwaresTags->delete($softwaresTag)) {
            $this->Flash->success(__d('The softwares tag has been deleted.')); // TODO i18n
        } else {
            $this->Flash->error(__d('The softwares tag could not be deleted. Please, try again.')); // TODO i18n
        }

        return $this->redirect(['action' => 'index']);
    }
}

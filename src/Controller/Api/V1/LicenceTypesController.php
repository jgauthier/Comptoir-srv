<?php
namespace App\Controller\Api\V1;
use App\Controller\AppController;

/**
 * LicenceTypes Controller
 *
 * @property \App\Model\Table\LicenceTypesTable $LicenceTypes
 */
class LicenceTypesController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('licenceTypes', $this->paginate($this->LicenceTypes));
        $this->set('_serialize', ['licenceTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Licence Type id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $licenceType = $this->LicenceTypes->get($id, [
            'contain' => []
        ]);
        $this->set('licenceType', $licenceType);
        $this->set('_serialize', ['licenceType']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $licenceType = $this->LicenceTypes->newEntity();
        if ($this->request->is('post')) {
            $licenceType = $this->LicenceTypes->patchEntity($licenceType, $this->request->data);
            if ($this->LicenceTypes->save($licenceType)) {
                $this->Flash->success(__('The licence type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The licence type could not be saved. Please, try again.'));
                $errors = $licenceType->errors();
            }
        }
        $this->set(compact(['licenceType','errors']));
        $this->set('_serialize', ['licenceType','errors']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Licence Type id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $licenceType = $this->LicenceTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $licenceType = $this->LicenceTypes->patchEntity($licenceType, $this->request->data);
            if ($this->LicenceTypes->save($licenceType)) {
                $this->Flash->success(__('The licence type has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The licence type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('licenceType'));
        $this->set('_serialize', ['licenceType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Licence Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $licenceType = $this->LicenceTypes->get($id);
        if ($this->LicenceTypes->delete($licenceType)) {
            $this->Flash->success(__('The licence type has been deleted.'));
        } else {
            $this->Flash->error(__('The licence type could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Api\V1;
use App\Controller\AppController;

use Cake\Core\Configure;
use Cake\Event\Event;

use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

use Cake\Utility\Text;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{


    public function initialize()
    {
        parent::initialize();

        if ($this->request->action === 'search') {
            $this->loadComponent('Search.Prg');
        }

        $this->Softwares = TableRegistry::get("Softwares");
        $this->Users = TableRegistry::get("Users");

    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['contact','legal','participate','accessibility']);

        parent::beforeFilter($event);
    }

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }


    /**
     * Get all softwares matching with the name passed in parameter.
     */
    public function search() {

        $Softwares = TableRegistry::get("Softwares");

        //TO SORT
        if (isset($this->request->query["order"])) {
            $sort = explode(".", $this->request->query["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }

        $this->paginate = [
            "Softwares"=>[
                'sortWhitelist' => [
                    'softwarename',
                    'average_review',
                    'created',
                    'modified',
                ],
                'limit' => Configure::read('LIMIT'),
                'order' => ['softwarename' => Configure::read('ORDER')],
                'contain' => ['Reviews','Tags'],
            ],
            "Users"=>[
                'limit' => Configure::read('LIMIT'),
            ]
        ];

        $searchQuery = isset($this->request->data["search"]) ? $this->request->data["search"] : $this->request->query["search"];
        $this->set('searchQuery',$searchQuery);


        // keep spaces ,  uppercase  and transiterate accent characters to non-accent characters.
        $this->request->query['search'] = strtoupper( Text::slug( $searchQuery ) );
        $softwares = $Softwares
            ->find(
                'search',
                [
                    'search' => $this->request->query,
                    "contain" => ["Reviews"],
                ])->select(['average_review' => $Softwares->query()->func()->coalesce([$Softwares->query()->func()->avg('Reviews.evaluation'),0])])
            ->leftJoinWith('Reviews')
            ->group(['Softwares.id'])
            ->autoFields(true)
        ;

        $this->Users = $this->Users
            ->find('search',
                [
                    'search' => $this->request->query,
                    'contain' => ['UserTypes'],
                    'order' => [
                        'Users.username' => Configure::read('ORDER')
                    ],
                ]);

        $reviewed = [
            "false" => __d("Forms","Search.Filter.NoFilterOnReview"),
            "true"=>__d("Forms","Search.Filter.WithReview"),
        ];

        $screenCaptured = [
            "false" => __d("Forms","Search.Filter.NoFilterOnScreenshot"),
            "true"=>__d("Forms","Search.Filter.WithScreenshot"),
        ];

        $used = [
            "false" => __d("Forms","Search.Filter.NoFilterOnUser"),
            "true"=>__d("Forms","Search.Filter.WithUser"),
        ];

        $hasServiceProvider = [
            "false" => __d("Forms","Search.Filter.NoFilterOnServiceProvider"),
            "true"=>__d("Forms","Search.Filter.ServiceProvider"),
        ];

        $order = [
            "softwarename.asc"=>__d("Forms","Search.Sort.softwarenameAsc"),
            "softwarename.desc"=>__d("Forms","Search.Sort.softwarenameDesc"),
            "created.asc" => __d("Forms","Search.Sort.createdAsc"),
            "created.desc" => __d("Forms","Search.Sort.createdDesc"),
            "modified.asc"=>__d("Forms","Search.Sort.modifiedAsc"),
            "modified.desc"=>__d("Forms","Search.Sort.modifiedDesc"),
            "average_review.asc"=>__d("Forms","Search.Sort.averageReviewsAsc"),
            "average_review.desc"=>__d("Forms","Search.Sort.averageReviewsDesc"),
        ];

        $this->set('reviewed',$reviewed);
        $this->set('used',$used);
        $this->set('hasServiceProvider',$hasServiceProvider);
        $this->set('order',$order);
        $this->set('screenCaptured',$screenCaptured);
//        $this->set('sort',$this->request->data['sort']);
//        $this->set('direction',$this->request->data['direction']);

        //To populate SearchForm
        $this->request->data = $this->request->query;

        $this->set('softwares', $this->paginate($softwares));
        $this->set('users', $this->paginate($this->Users));

        $this->set('_serialize', ['softwares', 'users']);
    }


    public function softwaresSelected(){

        $softwares = $this->Softwares->find();

        $this->paginate = [
            'limit' => Configure::read('LIMIT_NUMBER_OF_SOFTWARES_HOME_PAGE'),
            'finder' => 'average',
            "conditions"=>["Softwares.id IN "=>Configure::read('Categories.PickOfTheMonth')],
            'contain' => ['Licenses', 'Reviews',]
        ];
        $this->set('softwaresSelected', $this->paginate($softwares));
        $this->set('_serialize', ['softwaresSelected']);
    }

    public function softwareLastAdded () {

        $softwares = $this->Softwares ->find();

        $this->paginate = [
            'limit' => Configure::read('LIMIT_NUMBER_OF_SOFTWARES_HOME_PAGE'),
            'finder' => 'average',

            'order' => [
                'Softwares.created' => "desc"
            ],
            'contain' => ['Licenses', 'Reviews', 'Screenshots', 'Relationships']
        ];
        $this->set('softwaresLastAdded', $this->paginate($softwares));
        $this->set('_serialize', ['softwaresLastAdded']);
    }


    public function index () {

        $this->softwareLastAdded();
        $this->softwaresSelected();

        $countReviews = TableRegistry::get("Reviews")->find("all")->count("id");
        $countUsers = TableRegistry::get("Users")->find("all")->count("id");
        $countSoftwares = $this->Softwares->find("all")->count("id");
        $this->set("countReviews",$countReviews);
        $this->set("countSoftwares",$countSoftwares);
        $this->set("countUsers",$countUsers);
        $this->set('_serialize', ['countReviews','countUsers','countSoftwares']);

    }

    public function contact() {

    }

    public function participate() {

    }

    public function legal() {

    }

    public function accessibility() {

    }


}

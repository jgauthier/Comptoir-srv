<?php
namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Tags Controller
 *
 * @property \App\Model\Table\TagsTable $Tags
 */
class TagsController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->paginate = [
            'sortWhitelist'=>[
              'used_tag_number',
              'name'
            ],
            'limit' => Configure::read('LIMIT'),
            'contain' => ['Softwares'],
            'order' => ['name' => "ASC"]
        ];
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        if (!empty($this->request->data) && isset($this->request->data["order"])) {
            $sort=explode(".",$this->request->data["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }


        $order = [
            "name.asc"=>__d("Forms","Search.Sort.tagNameAsc"),
            "name.desc"=>__d("Forms","Search.Sort.tagNameDesc"),
            "used_tag_number.asc"=>__d("Forms","Search.Sort.usedTagNumberAsc"),
            "used_tag_number.desc"=>__d("Forms","Search.Sort.usedTagNumberDesc"),
        ];



        $tags = $this->Tags->find("all",['Softwares'])

            ->select(['used_tag_number'=>$this->Tags->query()->func()->count('SoftwaresTags.tag_id')])
            ->innerJoinWith('Softwares')
            ->group(['Tags.id','SoftwaresTags.tag_id'])
            ->autoFields(true);

        $this->paginate($tags);
        $this->set(compact('tags'));
        $this->set('order',$order);
        $this->set('_serialize', ['tags']);
    }

    public function beforeFilter(Event $event)
    {
        $this->Auth->allow([
            'add',
            'edit',
            'delete'
        ]);
        parent::beforeFilter($event);
    }

    /**
     * View method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tag = $this->Tags->get($id);

        $this->set('tag', $tag);
        $this->set('_serialize',['tag']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tag = $this->Tags->newEntity();
        if ($this->request->is('post')) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            $this->log(var_export($tag, true));
            if ($this->Tags->save($tag)) {
                $message = "Success";
            } else {
                $this->Flash->error(__d('Tags',"error"));
                $message = "Error";
                $tag = $tag->errors();

            }
        }
        $softwares = $this->Tags->Softwares->find('list', ['limit' => 125]);
        $this->set(compact('tag', 'softwares', 'message'));
        $this->set('_serialize', ['tag','message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
            $tag = $this->Tags->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            if ($this->Tags->save($tag)) {
                $this->Flash->success(__d('Tags', 'The tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__d('Tags', 'The tag could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->Tags->Softwares->find('list', ['limit' => 160]);
        $this->set(compact('tag', 'softwares'));
        $this->set('_serialize', ['tag', 'message']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tag = $this->Tags->get($id);
        if ($this->Tags->delete($tag)) {
            $this->Flash->success(__d('Tags', 'The tag has been deleted.'));
        } else {
            $this->Flash->error(__d('Tags', 'The tag could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

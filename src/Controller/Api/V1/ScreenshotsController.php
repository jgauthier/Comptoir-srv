<?php
namespace App\Controller\Api\V1;
use App\Controller\AppController;

/**
 * Screenshots Controller
 *
 * @property \App\Model\Table\ScreenshotsTable $Screenshots
 */
class ScreenshotsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if (isset($this->request->params['software_id'])) {
            $this->viewBuilder()->template("screenshots_software");
            $software = $this->Screenshots->Softwares->find("all")->select(["id","softwarename"])->where(["id"=>$this->request->params['software_id']])->firstOrFail();
            $this->set('software',$software);
            $this->set('_serialize', ['software']);

        }

        $this->paginate = [
            'conditions' => isset($this->request->params['software_id']) ? [
                'Softwares.id ' => $this->request->params['software_id']
            ]: [],
            'contain' => [
                'Softwares'
            ]
        ];

        $this->set('screenshots', $this->paginate($this->Screenshots));
        $this->set('_serialize', ['screenshots']);
    }

    /**
     * View method
     *
     * @param string|null $id Screenshot id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $screenshot = $this->Screenshots->get($id, [
            'contain' => ['Softwares']
        ]);
        $this->set('screenshot', $screenshot);
        $this->set('_serialize', ['screenshot']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $screenshot = $this->Screenshots->newEntity();
        if ($this->request->is('post')) {
            $screenshot = $this->Screenshots->patchEntity($screenshot, $this->request->data);
            if ($this->Screenshots->save($screenshot)) {
                $this->Flash->success(__('The screenshot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The screenshot could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->Screenshots->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('screenshot', 'softwares'));
        $this->set('_serialize', ['screenshot']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Screenshot id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $screenshot = $this->Screenshots->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $screenshot = $this->Screenshots->patchEntity($screenshot, $this->request->data);
            if ($this->Screenshots->save($screenshot)) {
                $this->Flash->success(__('The screenshot has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The screenshot could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->Screenshots->Softwares->find('list', ['limit' => 200]);
        $this->set(compact('screenshot', 'softwares'));
        $this->set('_serialize', ['screenshot']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Screenshot id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $screenshot = $this->Screenshots->get($id);
        if ($this->Screenshots->delete($screenshot)) {
            $this->Flash->success(__('The screenshot has been deleted.'));
        } else {
            $this->Flash->error(__('The screenshot could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

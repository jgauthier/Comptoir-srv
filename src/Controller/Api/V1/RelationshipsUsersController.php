<?php
namespace App\Controller\Api\V1;
use App\Controller\AppController;

/**
 * RelationshipsUsers Controller
 *
 * @property \App\Model\Table\RelationshipsUsersTable $RelationshipsUsers
 */
class RelationshipsUsersController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Relationships']
        ];
        $this->set('relationshipsUsers', $this->paginate($this->RelationshipsUsers));
        $this->set('_serialize', ['relationshipsUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Relationships User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relationshipsUser = $this->RelationshipsUsers->get($id, [
            'contain' => ['Users', 'Relationships']
        ]);
        $this->set('relationshipsUser', $relationshipsUser);
        $this->set('_serialize', ['relationshipsUser']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relationshipsUser = $this->RelationshipsUsers->newEntity();
        if ($this->request->is('post')) {
            $relationshipsUser = $this->RelationshipsUsers->patchEntity($relationshipsUser, $this->request->data);
            if ($this->RelationshipsUsers->save($relationshipsUser)) {
                $this->Flash->success(__('The relationships user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships user could not be saved. Please, try again.'));
            }
        }
        $users = $this->RelationshipsUsers->Users->find('list', ['limit' => 200]);
        $relationships = $this->RelationshipsUsers->Relationships->find('list', ['limit' => 200]);
        $this->set(compact('relationshipsUser', 'users', 'relationships'));
        $this->set('_serialize', ['relationshipsUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Relationships User id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relationshipsUser = $this->RelationshipsUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $relationshipsUser = $this->RelationshipsUsers->patchEntity($relationshipsUser, $this->request->data);
            if ($this->RelationshipsUsers->save($relationshipsUser)) {
                $this->Flash->success(__('The relationships user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships user could not be saved. Please, try again.'));
            }
        }
        $users = $this->RelationshipsUsers->Users->find('list', ['limit' => 200]);
        $relationships = $this->RelationshipsUsers->Relationships->find('list', ['limit' => 200]);
        $this->set(compact('relationshipsUser', 'users', 'relationships'));
        $this->set('_serialize', ['relationshipsUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Relationships User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relationshipsUser = $this->RelationshipsUsers->get($id);
        if ($this->RelationshipsUsers->delete($relationshipsUser)) {
            $this->Flash->success(__('The relationships user has been deleted.'));
        } else {
            $this->Flash->error(__('The relationships user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}

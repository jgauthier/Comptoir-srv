<?php

namespace App\Controller\Api\V1;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Softwares Controller
 *
 * @property \App\Model\Table\SoftwaresTable $Softwares
 */
class SoftwaresController extends AppController
{

    public function initialize()
    {
        parent::initialize();

        $this->paginate = [
            'limit' => Configure::read('LIMIT'),
            'maxLimit' => Configure::read('LIMIT'),
            'order' => [
                'Softwares.softwarename' => Configure::read('ORDER')
            ],
            'contain' => ['Licenses', 'Reviews', 'Screenshots', 'Relationships']
        ];
        $this->loadModel("Users");
    }

    /**
     * Manage all rights for the controllers' actions.
     * Returns true if the user can use the currrent action, FALSE otherwise.
     * Returns true for add a project if the user is connected
     * Returns true for edit and delete action if the user is owner.
     * @param Array $user User informations
     * @return boolean
     */
    public function isAuthorized($user)
    {
        //If the user is connected
        if ($this->Auth->user('id')) {
            if (in_array($this->request->action, ["deleteUsersSoftware", "deleteServicesProviders"])) {
                return true;
            }
        }
        if ($this->request->action === 'add' && $this->Auth->user('id')) {
            return true;
        }

        if ($this->request->action === 'edit' && $this->Auth->user('id')) {
            return true;
        }

        if ($this->Auth->user('id')) {
            return true;
        }

        if ($this->request->is('post')) {
            switch ($this->request->action) {
                case 'usersSoftware' :
                    return $this->Users->isCompany($this->Auth->user('id')) ? false : true;
            }
        }
        return parent::isAuthorized($user);
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        if ($this->request->is('get')) {
            $this->Auth->allow(['index',
                'add',
                'view',
                'lastAdded',
                "getProjectsById",
                'usersSoftware',
                'reviewsSoftware',
                "servicesProviders",
                "workswellSoftwares",
                "screenshots",
                "tagged",
            ]);
        }
        parent::beforeFilter($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->request->allowMethod(['post', 'get']);

//      For sort
        if (!empty($this->request->data) && isset($this->request->data["order"])) {
            $sort = explode(".", $this->request->data["order"]);
            $this->request->query["sort"] = $sort[0];
            $this->request->query["direction"] = $sort[1];
        }

//       For filters
        $reviewed = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnReview"),
            "true" => __d("Forms", "Search.Filter.WithReview"),
        ];

        $screenCaptured = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnScreenshot"),
            "true" => __d("Forms", "Search.Filter.WithScreenshot"),
        ];

        $used = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnUser"),
            "true" => __d("Forms", "Search.Filter.WithUser"),
        ];

        $hasServiceProvider = [
            "false" => __d("Forms", "Search.Filter.NoFilterOnServiceProvider"),
            "true" => __d("Forms", "Search.Filter.ServiceProvider"),
        ];

        $order = [
            "softwarename.asc" => __d("Forms", "Search.Sort.softwarenameAsc"),
            "softwarename.desc" => __d("Forms", "Search.Sort.softwarenameDesc"),
            "created.asc" => __d("Forms", "Search.Sort.createdAsc"),
            "created.desc" => __d("Forms", "Search.Sort.createdDesc"),
            "modified.asc" => __d("Forms", "Search.Sort.modifiedAsc"),
            "modified.desc" => __d("Forms", "Search.Sort.modifiedDesc"),
            "average_review.asc" => __d("Forms", "Search.Sort.averageReviewsAsc"),
            "average_review.desc" => __d("Forms", "Search.Sort.averageReviewsDesc"),
        ];


        //Redifine paginate for the use case
        $this->paginate = [
            "Softwares" => [
                'sortWhitelist' => [
                    'softwarename',
                    'average_review',
                    'created',
                    'modified',
                ],

                'limit' => Configure::read('LIMIT'),
                'maxLimit' => Configure::read('LIMIT'),
                'order' => ['softwarename' => Configure::read('ORDER')],
                'contain' => ['Reviews'],
            ],
            'contain' => ['Tags']
        ];
        $softwares = $this->Softwares
            ->find(
                'search',
                [
                    'search' => $this->request->data,
                    "contain" => ["Reviews"],
                ])->select(['average_review' => $this->Softwares->query()->func()->coalesce([$this->Softwares->query()->func()->avg('Reviews.evaluation'), 0])]) // allow to sort by the virtual property "Average"
            ->leftJoinWith('Reviews')
            ->group(['Softwares.id'])
            ->autoFields(true);


        $this->set('softwares', $this->paginate($softwares));
        $this->set('reviewed', $reviewed);
        $this->set('used', $used);
        $this->set('hasServiceProvider', $hasServiceProvider);
        $this->set('screenCaptured', $screenCaptured);
        $this->set('order', $order);
        $this->set('_serialize', ['softwares']);
    }

    /**
     * View method
     *
     * @param string|null $id Software id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {

        // an id is specified => get details of the software
        if ($this->request->is('get') && $id != null) {

            $software = $this->Softwares->get($id, [
                'finder' => 'average',
                'contain' => [
                    'Licenses',
                    'Reviews' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Reviews.created' => 'DESC']);
                        },
                        'Users' => [
                            'UserTypes',
                            'fields' => [
                                'id',
                                'username',
                                'logo_directory',
                                'photo',
                                'description'
                            ]
                        ]
                    ],
                    'Screenshots',
                    'Userssoftwares' => [],
                    'Creatorssoftwares' => [],
                    'Providerssoftwares' => [],
                    'workswellsoftwares' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Softwares.softwarename' => 'ASC']);
                        },
                        'Softwares' => [
                            'Reviews',
                            'Tags',
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                                'description'
                            ]
                        ]
                    ],
                    'alternativeto' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Softwares.softwarename' => 'ASC']);
                        },
                        'Softwares' => [
                            'Reviews',
                            'Tags',
                            'fields' => [
                                'id',
                                'softwarename',
                                'logo_directory',
                                'photo',
                                'description',
                            ]
                        ]
                    ],
                    'Tags' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Tags.name' => 'ASC']);
                        },
                    ],
                ],
            ]);

            $this->set(compact(['software']));
            $this->set('_serialize', ['software']);
        }

        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => $software->softwarename,
            "description" => $software->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $software->logo_directory . DS . $software->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => $software->softwarename,
            "description" => $software->description,
            "image" => Configure::read('App.fullBaseUrl') . DS . $software->logo_directory . DS . $software->photo,
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);

        // no id specified => ALL software listed
        if ($id == null && $this->request->is('get') && $this->request->is('json')) {

            $softwares = $this->Softwares
                ->find('search', ['search' => $this->request->query]);

            $this->set('softwares', $this->paginate($softwares));
            $this->set('_serialize', ['softwares']);
        }
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        isset($this->request->data) && !empty($this->request->data) ? $software = $this->Softwares->newEntity($this->request->data, ['associated' => "Screenshots", "Tags"]) : $software = $this->Softwares->newEntity();

        $user = TableRegistry::get("Users")->get($this->Auth->user('id'));
        $software->user = $user;

        $message = "";
        if ($this->Auth->user('id')) {
            if (!empty($this->request->data)) {

                if ($this->Softwares->save($software)) {
                    $message = "Success";
                    $this->request->is('json') ? null : $this->Flash->success(__d("Forms", "Congratulations ! Thanks to you a new software is on the Comptoir du Libre !"));
                } else {
                    $message = "Error";
                    $this->request->is('json') ? $software = $software->errors() : null;
                    $this->request->is('json') ? null : $this->Flash->error(__d("Forms", "Your request to add a software failed, please follow rules in red."));

                }
            }
        }

        $this->ValidationRules->config('tableRegistry', "Softwares");
        $rules = $this->ValidationRules->get();
        $licenses = $this->Softwares->Licenses->find('list', ['limit' => 200]);
        $this->set(compact('software', 'licenses', 'rules', 'message'));
        $this->set('_serialize', ['software', 'licenses', 'rules', 'message']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Software id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $software = $this->Softwares->get($id, [
            'contain' => [
                'Screenshots',
                'Tags' => [
                    'strategy' => 'select',
                    'queryBuilder' => function ($q) {
                        return $q->order(['Tags.name' => 'ASC']);
                    },
                ],
            ]
        ]);

        $user = TableRegistry::get("Users")->get($this->Auth->user('id'));
        $software->user = $user;

        //TODO A refaire lors de la fusion
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (isset($this->request->data['screenshots'][0]['photo']['name'])) {
                $this->request->data['screenshots'][0]['photo']['name'] = time() . "_" . $this->request->data['screenshots'][0]['photo']['name'];
            }

            $software = $this->Softwares->patchEntity($software, $this->request->data, ['associated' => "Screenshots", "Tags"]);
            if ($this->Softwares->save($software)) {
                $message = "Success";
                $this->Flash->success(__d("Forms", "Edit.Congratulations.software.success"));
            } else {
                $message = "Error";
                $this->Flash->error(__d("Forms", "Edit.Congratulations.software.error"));
                $this->set('errors', $software);
            }
        }

        $this->ValidationRules->config('tableRegistry', "Softwares");
        $rules = $this->ValidationRules->get();
        $licenses = $this->Softwares->Licenses->find('list', ['limit' => 200]);
        $this->set(compact('software', 'licenses', 'rules', 'message'));
        $this->set('_serialize', ['software', 'licenses', 'rules', 'message', 'errors']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Software id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $software = $this->Softwares->get($id);
        if ($this->Softwares->delete($software)) {
            $this->Flash->success(__('The software has been deleted.'));
        } else {
            $this->Flash->error(__('The software could not be deleted. Please, try again.'));
        }
        return $this->redirect(["prefix" => false, 'action' => 'index']);
    }

    public function usersSoftware($id = null)
    {

        $this->request->allowMethod(['post', 'get']);

        try {
            $software = $this->Softwares->get($id,
                ['contain' => ['Userssoftwares']]);

        } catch (\Exception $e) {
            throw new \App\Network\Exception\SoftwareNotFoundException("The software with the id " . $id . " does not exist");
        }

        if ($this->request->is('post')) {
            $datas = [];
            $datas["software_id"] = $software->id;

            try {
                $user = $this->Users->get($this->Auth->user('id'));
                $datas["user_id"] = $user->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
            }

            try {
                $this->loadModel("Relationships");
                $relationship = $this->Relationships->find("all")->where(["cd" => "UserOf"])->first();
                $datas["relationship_id"] = $relationship->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\RelationshipNotFoundException("The relationship with the cd " . "UserOf" . " does not exist");
            }

            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $datas);

            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $software = $this->Softwares->get($id, ['contain' => 'Userssoftwares']);

                $this->Flash->success(__d("Forms", "Your are now known as user of."));
                $message = "Success";
                $this->request->is('json') ? null : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);

            } else {
                ($relSoftUser->errors("software_id")['_isUnique'] != null)
                    ? $this->Flash->error(__d("Forms", "You can not be declared twice as user of {0}.", $software->softwarename))
                    : $this->Flash->error(__d("Forms", "We can not respond to your request"));
                $message = "Error";
                $this->request->is('json') ? $software = $relSoftUser->errors() : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);

            }
        }
        $this->set(compact('software', 'message', 'softwareName'));
        $this->set('_serialize', ['software', 'message', 'softwareName']);
    }

    /**
     * @param null $id
     */
    public function deleteUsersSoftware($id = null)
    {
        $this->request->allowMethod(['delete']);
        $this->viewBuilder()->template("view");

        try {
            $this->loadModel("Relationships");
            $relationship = $this->Relationships->find("all")->where(["cd" => "UserOf"])->first();
        } catch (\Exception $e) {
            throw new \App\Network\Exception\RelationshipNotFoundException(__d("Forms", "The relationship with the cd " . "UserOf" . " does not exist"));
        }

        $conditionsToDelete = [
            "software_id" => $id,
            "user_id" => $this->Auth->user('id'),
            "relationship_id" => $relationship->id
        ];

        if ($this->Softwares->RelationshipsSoftwaresUsers->exists($conditionsToDelete)) {

            $entity = $this->Softwares->RelationshipsSoftwaresUsers->find()->where($conditionsToDelete)->first();

            $this->Softwares->RelationshipsSoftwaresUsers->delete($entity, $conditionsToDelete);

            $message = "Success";
            $this->Flash->success(__d("Forms", "Your are not know as user of anymore."));

        } else {
            $message = "No relationship in our data base.";
            $this->Flash->error(__d("Forms", "We can not respond to your request"));

        }
        $this->set(compact('message'));
        $this->set('_serialize', ['message']);
        $this->request->is('json') ? null : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);

    }

    /**
     * @param null $id
     */
    public function reviewsSoftware($id = null)
    {
        $software = $this->Softwares->get($id,
            ["contain" =>
                [
                    'Reviews' => [
                        'strategy' => 'select',
                        'queryBuilder' => function ($q) {
                            return $q->order(['Reviews.created' => 'DESC']);
                        },
                        'Users' => [
                            'UserTypes',
                            'fields' => [
                                'id',
                                'username',
                                'logo_directory',
                                'photo',
                                'description'
                            ]
                        ]
                    ]
                ]
            ]);

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);
    }

    /**
     * Get all alternative for a software
     * @param integer $id
     */
    public function alternativeTo($id = null)
    {
        $software = $this->Softwares->get($id,
            [
                'finder' => 'average',
                "contain" =>
                    [
                        'alternativeto' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ]
                    ]
            ]);

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);
    }

    /**
     * Get all services provider for a software
     * @param integer $id
     */
    public function servicesProviders($id = null)
    {
        try {
            $software = $this->Softwares->get($id,
                ['contain' => ['Providerssoftwares']]);

        } catch (\Exception $e) {
            throw new \App\Network\Exception\SoftwareNotFoundException("The software with the id " . $id . " does not exist");
        }

        if ($this->request->is('post')) {
            $datas = [];
            $this->loadModel("Users");

            $datas["software_id"] = $software->id;

            try {
                $user = $this->Users->get($this->Auth->user('id'));
                $datas["user_id"] = $user->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\UserNotFoundException("The user with the id " . $this->Auth->user('id') . " does not exist");
            }

            try {
                $this->loadModel("Relationships");
                $relationship = $this->Relationships->find("all")->where(["cd" => "ProviderFor"])->first();
                $datas["relationship_id"] = $relationship->id;
            } catch (\Exception $e) {
                throw new \App\Network\Exception\RelationshipNotFoundException("The relationship with the cd " . "ProviderFor" . " does not exist");
            }

            $relSoftUser = $this->Softwares->RelationshipsSoftwaresUsers->newEntity();
            $this->Softwares->RelationshipsSoftwaresUsers->patchEntity($relSoftUser, $datas);

            if ($this->Softwares->RelationshipsSoftwaresUsers->save($relSoftUser)) {
                $software = $this->Softwares->get($id, ['contain' => 'Providerssoftwares']);
                $message = "Success";
                $this->Flash->success(__d("Forms", "Your are now known as service providers for {0}.", $software->softwarename));
                $this->request->is('json') ? null : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);
            } else {
                ($relSoftUser->errors("software_id")['_isUnique'] != null)
                    ? $this->Flash->error(__d("Forms", "You can not be declared twice as service provider for {0}.", $software->softwarename))
                    : $this->Flash->error(__d("Forms", "We can not respond to your request."));
                $message = "Error";
                $this->request->is('json') ? $software = $relSoftUser->errors() : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);
            }
        }
        $this->set(compact('software', 'message'));
        $this->set('_serialize', ['software', 'message']);
    }

    /**
     * @param null $id
     */
    public function deleteServicesProviders($id = null)
    {
        $this->request->allowMethod(['delete']);
        $this->viewBuilder()->template("view");

        try {
            $this->loadModel("Relationships");
            $relationship = $this->Relationships->find("all")->where(["cd" => "ProviderFor"])->first();
        } catch (\Exception $e) {
            throw new \App\Network\Exception\RelationshipNotFoundException(__d("Forms", "The relationship with the cd " . "ProviderFor" . " does not exist"));
        }

        $conditionsToDelete = [
            "software_id" => $id,
            "user_id" => $this->Auth->user('id'),
            "relationship_id" => $relationship->id
        ];

        if ($this->Softwares->RelationshipsSoftwaresUsers->exists($conditionsToDelete)) {
            $entity = $this->Softwares->RelationshipsSoftwaresUsers->find()->where($conditionsToDelete)->first();
            $this->Softwares->RelationshipsSoftwaresUsers->delete($entity, $conditionsToDelete);

            $message = "Success";
            $this->Flash->success(__d("Forms", "Your are not know as service provider of anymore."));
        } else {
            $message = "No relationship in our data base.";
            $this->Flash->error(__d("Forms", "No relationship in our data base."));
        }
        $this->set(compact('message'));
        $this->set('_serialize', ['message']);

        $this->request->is('json') ? null : $this->redirect(["prefix" => false, "controller" => "Softwares", "action" => $id, "language" => $this->request->param("language"),]);
    }

    /**
     * Get all softwares working well with the current software
     * @param integer $id
     */
    public function workswellSoftwares($id = null)
    {
        $software = $this->Softwares->get($id,
            [
                'finder' => 'average',
                "contain" =>
                    [
                        'workswellsoftwares' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['Softwares.softwarename' => 'ASC']);
                            },
                            'Softwares' => [
                                'fields' => [
                                    'id',
                                    'softwarename',
                                    'logo_directory',
                                    'photo',
                                    'description'
                                ]
                            ]
                        ]
                    ]
            ]);

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);
    }

    /**
     * Get  all scrennshots for the current softa
     * @param integer $idplate
     */
    public function screenshots($id = null)
    {
        $software = $this->Softwares->get($id,
            ["contain" =>
                [
                    'Screenshots'
                ]
            ]);

        $this->set(compact('software'));
        $this->set('_serialize', ['software']);
    }

    /**
     * list of softwares with tag='pass' associate
     * send the list to the view
     *
     * @param string 'pass' are tags we looking for
     */
    public function tagged()
    {
        $tags = $this->request->params['pass'];
        $softwares = $this->Softwares->find('tagged', [
            'tags' => $tags
        ]);

        $this->set([
            'softwares' => $softwares,
            'tags' => $tags
        ]);
    }
}

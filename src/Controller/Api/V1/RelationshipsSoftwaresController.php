<?php
namespace App\Controller\Api\V1;
use App\Controller\AppController;

/**
 * RelationshipsSoftwares Controller
 *
 * @property \App\Model\Table\RelationshipsSoftwaresTable $RelationshipsSoftwares
 */
class RelationshipsSoftwaresController extends AppController
{


    public function isAuthorized($user) {

        //If the user is connected
        if ($this->Auth->user('id')) {
            if (in_array($this->request->action, ["add", "edit","delete"])) {
                return true;
            }
        }
        return parent::isAuthorized($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('relationshipsSoftwares', $this->paginate($this->RelationshipsSoftwares));
        $this->set('_serialize', ['relationshipsSoftwares']);
    }

    /**
     * View method
     *
     * @param string|null $id Relationships Software id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->get($id, [
            'contain' => []
        ]);
        $this->set('relationshipsSoftware', $relationshipsSoftware);
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->newEntity();
        if ($this->request->is('post')) {
            $relationshipsSoftware = $this->RelationshipsSoftwares->patchEntity($relationshipsSoftware, $this->request->data);
            if ($this->RelationshipsSoftwares->save($relationshipsSoftware)) {
                $this->Flash->success(__('The relationships software has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The relationships software could not be saved. Please, try again.'));
            }
        }
        $softwares = $this->RelationshipsSoftwares->Softwares->find('list', ['limit' => 200]);
        $relationships = $this->RelationshipsSoftwares->Relationships->find('list', ['limit' => 200]);
        $this->set(compact('relationships','softwares'));
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Relationships Software id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $relationshipsSoftware = $this->RelationshipsSoftwares->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $relationshipsSoftware = $this->RelationshipsSoftwares->patchEntity($relationshipsSoftware, $this->request->data);
            if ($this->RelationshipsSoftwares->save($relationshipsSoftware)) {
                $this->Flash->success(__('The relationships software has been saved.'));
            } else {
                $this->Flash->error(__('The relationships software could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('relationshipsSoftware'));
        $this->set('_serialize', ['relationshipsSoftware']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Relationships Software id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $relationshipsSoftware = $this->RelationshipsSoftwares->get($id);
        if ($this->RelationshipsSoftwares->delete($relationshipsSoftware)) {
            $this->Flash->success(__('The relationships software has been deleted.'));
        } else {
            $this->Flash->error(__('The relationships software could not be deleted. Please, try again.'));
        }
    }
}

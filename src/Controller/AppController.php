<?php //
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

use Cake\I18n\I18n;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    protected $availableLanguages = [
        'en' => 'en',
        'fr' => 'fr',
    ];

    protected $explicitLanguages = [
        'en' => 'english',
        'fr' => 'français',
    ];

    use \Crud\Controller\ControllerTrait;

    public $components = [
        'RequestHandler',
    ];


    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');

        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login',
                'prefix' => false,
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email']
                ]
            ],
            'unauthorizedRedirect' => false,

            'storage' => 'Session'
        ]);

        $this->loadComponent('Search.Prg', [
            // This is default config. You can modify "actions" as needed to make
            // the PRG component work only for specified methods.
            'actions' => ['index', 'search']
        ]);

        $this->loadComponent('ValidationRules');
    }

    /**
     * @param $user
     * @return bool
     *
     */
    public function isAuthorized($user)
    {
        //Allow all get action
        if ($this->request->is('get')) {
            return true;
        }

        // Admin peuvent accéder à chaque action
        if (isset($user['role']) && $user['role'] === 'admin') {
            return true;
        }
        $this->Flash->error(__('You are not allowed to do that.'));
        // Par défaut refuser
        return false;
    }


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->RequestHandler->renderAs($this, 'json');
            $this->set('_serialize', true);
        }
    }


    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['index', 'logout', 'view', 'request', 'search']);

        $this->setLocale();
        $this->setOpenGraph();
        parent::beforeFilter($event);
    }

    private function setOpenGraph()
    {
        //For Social MEDIAS => OPENGRAPH
        $openGraph = [
            "title" => Configure::read("OpenGraph.title"),
            "description" => __d("Layout", "opengraph.description"),
            "image" => Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $card = [
            "title" => Configure::read("OpenGraph.title"),
            "description" => __d("Layout", "opengraph.description"),
            "image" => Configure::read("OpenGraph.image"),
            "imageSize" => ['width' => '200', 'height' => '300']
        ];

        $this->set('openGraph', $openGraph);
        $this->set('card', $card);
    }


    /**
     * Sets the current locale based on url param and available languages
     *
     * @return void
     */
    protected function setLocale()
    {
        $selectedLanguage = I18n::locale();
        $lang = $this->request->param('language') ? $this->request->param('language') : preg_replace('/_\w*/', "", I18n::locale());

        if ($lang && isset($this->availableLanguages[$lang])) {
            I18n::locale($lang);
            $selectedLanguage = $this->availableLanguages[$lang];
        }
        $this->set('selectedLanguage', $selectedLanguage);
        $this->set('availableLanguages', $this->availableLanguages);
        $this->set('explicitLanguages', $this->explicitLanguages);

    }
}

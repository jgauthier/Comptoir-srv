# Comptoir-du-libre with Docker

## Usage

* [Install docker](https://docs.docker.com/engine/installation/linux/ubuntu/#install-using-the-repository)
* Clone the [`Comptoir-srv` repository](https://gitlab.adullact.net/Comptoir/Comptoir-srv.git) in your project directory
* Get into the `Docker` directory
* You can specify the branch you want to test, default develop ,by editing `branche argument` in the ./Docker/docker-compose.yml
* Run `docker-compose build --no-cache` the --no-cache option is not mandatory
* Run `docker-compose up -d`
* Check if `docker_postgres_comptoir` and `docker_comptoir` are running with `docker ps`
* Now go to [http://localhost:8080](http://localhost:8080)

Enjoy ! :)

## Coming soon

* ADD files like :
    * Screenshots
    * Avatar
    * Logos

# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class UsersCreateAccountAdministration(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 900))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_users_create_account_administration(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        self.assertTrue(self.is_element_present(By.LINK_TEXT, "Mentions légales"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            self.driver.save_screenshot("administration.error.message" + '.png')
            return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        self.driver.save_screenshot('image.png')
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

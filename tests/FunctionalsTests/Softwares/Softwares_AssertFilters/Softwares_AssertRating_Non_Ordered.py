# -*- coding: utf-8 -*-
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display


class SoftwaresAssertRatingNonOrdered(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_softwares_assert_rating_non_ordered(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("reviewed")).select_by_index(1)
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[2]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[3]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[4]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[5]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[6]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//div[@id='super-main']/main/section/ol/li[7]/div/div[2]/span[@class=\"glyphicon glyphicon-star rating-star\"]"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

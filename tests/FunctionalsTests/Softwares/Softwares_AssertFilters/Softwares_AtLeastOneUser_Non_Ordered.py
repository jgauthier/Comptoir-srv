# -*- coding: utf-8 -*-
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class SoftwaresAtLeastOneUserNonOrdered(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_softwares_at_least_one_user_non_ordered(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares")
        time.sleep(1)
        Select(driver.find_element_by_name("hasUser")).select_by_index(1)
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Authentik\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-Parapheur\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LibreOffice\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"SPIP\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-delibRE\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"openScrutin\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"eGroupware\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Pastell\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"As@lae\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"GRR - Gestion Réservation de...\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Jorani\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LimeSurvey\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Maarch Courrier\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenBiblio\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Nuxeo\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenADS\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"openCimetière\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"openMairie\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"BlueMind\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenRecensement\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"OpenReglement\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"OpenRésultat\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"OpenRésultat\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Typo3\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"S²LOW\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Xemelios\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"web-delib\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WebGFC\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Publik\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WordPress\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WebGFC\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"WordPress\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"XiVO\")]/ancestor::li"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

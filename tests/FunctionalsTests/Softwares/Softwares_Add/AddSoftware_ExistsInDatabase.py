# -*- coding: utf-8 -*-
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class AddSoftwareExistsInDatabase(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_add_software_exists_in_database(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.find_element_by_link_text("Logiciels").click()
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/')]//button").click()

        driver.find_element_by_id("softwarename").clear()
        driver.find_element_by_id("softwarename").send_keys("Asqatasun")

        driver.find_element_by_id("url-website").clear()
        driver.find_element_by_id("url-website").send_keys("http://nouveaulogiciel.fr")
        driver.find_element_by_id("url-repository").clear()
        driver.find_element_by_id("url-repository").send_keys("http://nouveaulogiciel.git")

        driver.find_element_by_id("photo").clear()
        driver.find_element_by_id("photo").click()
        driver.find_element_by_id("photo").send_keys("/var/www/html/tests/TestFiles/Softwares/photo/avatar/correctSoftwareLogo.jpg")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.error.error"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

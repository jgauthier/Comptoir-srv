# -*- coding: utf-8 -*-
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display


class DeclareAsServiceProviderForANDRemoveFromServiceProviderList(unittest.TestCase):

    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_declare_as_service_provider_for_a_n_d_remove_from_service_provider_list(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        driver.find_element_by_css_selector("div.message.success").click()
        time.sleep(1)
        driver.find_element_by_link_text("Logiciels").click()
        time.sleep(1)
        driver.get(self.base_url + "/softwares/29")
        time.sleep(2)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/services-providers/29')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        time.sleep(1)
        driver.find_element_by_xpath("//form[contains(@action,'/softwares/delete-services-providers/29')]//button").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

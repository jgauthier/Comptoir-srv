# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display
from selenium.webdriver.common.by import By


class SoftwaresAddATag(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_softwares_add_a_tag(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares/9")
        driver.find_element_by_link_text("Modifier").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.get(self.base_url + "/softwares/9")
        driver.find_element_by_link_text("Modifier").click()
        driver.find_element_by_id("tags").clear()
        driver.find_element_by_id("tags").send_keys("nouveau")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        driver.get(self.base_url + "/softwares/9")
        self.assertTrue(self.is_element_present(By.XPATH,"//div[@class='tagUnit'][contains(string(), 'nouveau')]"))

    def test_softwares_add_two_tags(self):
        driver = self.driver
        driver.get(self.base_url + "/softwares/9")
        driver.find_element_by_link_text("Modifier").click()
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        driver.get(self.base_url + "/softwares/9")
        driver.find_element_by_link_text("Modifier").click()
        driver.find_element_by_id("tags").clear()
        driver.find_element_by_id("tags").send_keys("new tag test")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        driver.get(self.base_url + "/softwares/9")
        self.assertTrue(self.is_element_present(By.XPATH,"//div[@class='tagUnit'][contains(string(), 'new')]"))
        self.assertTrue(self.is_element_present(By.XPATH,"//div[@class='tagUnit'][contains(string(), 'tag')]"))
        self.assertTrue(self.is_element_present(By.XPATH,"//div[@class='tagUnit'][contains(string(), 'test')]"))
        self.assertFalse(self.is_element_present(By.XPATH,"//div[@class='tagUnit'][contains(string(), 'nouveau')]"))


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

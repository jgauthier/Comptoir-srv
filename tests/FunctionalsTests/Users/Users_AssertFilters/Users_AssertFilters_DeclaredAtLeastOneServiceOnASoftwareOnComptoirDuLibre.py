# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class UsersAssertFiltersDeclaredAtLeastOneServiceOnASoftwareOnComptoirDuLibre(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_users_assert_filters_declared_at_least_one_service_on_a_software_on_comptoir_du_libre(self):
        driver = self.driver
        driver.get(self.base_url + "/")

        driver.find_element_by_xpath("//div[@id='navbar']/nav/ul/li[2]/a").click()
        Select(driver.find_element_by_name("IsSerivicesProvider")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()

        self.assertEqual(11, len(driver.find_elements_by_css_selector("div.stamp.badge")))

        self.assertEqual(1, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeAsso")))

        self.assertEqual(9, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeCompany")))

        self.assertEqual(1, len(driver.find_elements_by_css_selector("div.stamp.badge.badgeAdministration")))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

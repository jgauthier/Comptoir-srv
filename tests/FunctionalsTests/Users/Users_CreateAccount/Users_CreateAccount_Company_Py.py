# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from pyvirtualdisplay import Display

class UsersCreateAccountCompany(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 900))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_users_create_account_company(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("S'inscrire").click()
        time.sleep(1)
        driver.find_element_by_id("user_type_id-3").click()
        time.sleep(1)
        driver.find_element_by_id("username").send_keys("TEST-MyCompany")
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.ID,"email"))
        driver.find_element_by_id("email").send_keys("mp-presta@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        self.assertTrue(self.is_element_present(By.ID,"password"))
        driver.find_element_by_id("password").send_keys("TEST-MyCompany")
        time.sleep(1)
        driver.find_element_by_id("confirm_password").send_keys("TEST-MyCompany")
        # driver.find_element_by_id("photo").clear()
        # driver.find_element_by_id("photo").send_keys("/builds/Comptoir/Comptoir-srv/tests/TestFiles/Users/correctUserAvatar.jpg")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            self.driver.save_screenshot("company.error.message" + '.png')
            return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

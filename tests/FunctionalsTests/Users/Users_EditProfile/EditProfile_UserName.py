# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from pyvirtualdisplay import Display

class EditPofileUserName(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 900))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_edit_pofile_user_name(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_link_text("Se connecter").click()
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)

        profileMenu = driver.find_element_by_link_text("TEST-Administration")
        profileLink = driver.find_element_by_xpath("//a[contains(@href, '/users/edit/159')]")
        ActionChains(driver).move_to_element(profileMenu).click_and_hold().move_to_element(profileLink).double_click().perform()
        time.sleep(1)

        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("TEST-AdministrationNew")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        time.sleep(1)

        profileMenu = driver.find_element_by_link_text("TEST-AdministrationNew")
        logoutLink = driver.find_element_by_xpath("//a[contains(@href, '/users/logout')]")
        ActionChains(driver).move_to_element(profileMenu).click_and_hold().move_to_element(logoutLink).double_click().perform()

        driver.find_element_by_link_text("Se connecter").click()
        time.sleep(1)
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_id("email").clear()
        time.sleep(1)
        driver.find_element_by_id("email").send_keys("mp-collectivite@comptoir-du-libre.org")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        time.sleep(1)
        self.assertEqual("TEST-AdministrationNew", driver.find_element_by_xpath("//a[contains(text(),'TEST-AdministrationNew')]").text)

        profileMenu = driver.find_element_by_link_text("TEST-AdministrationNew")
        profileLink = driver.find_element_by_xpath("//a[contains(@href, '/users/edit/159')]")
        ActionChains(driver).move_to_element(profileMenu).click_and_hold().move_to_element(profileLink).double_click().perform()
        time.sleep(1)

        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("TEST-Administration")
        time.sleep(1)
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(1)
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, "div.message.success"))
        time.sleep(1)
        profileMenu = driver.find_element_by_link_text("TEST-Administration")
        logoutLink = driver.find_element_by_xpath("//a[contains(@href, '/users/logout')]")
        ActionChains(driver).move_to_element(profileMenu).click_and_hold().move_to_element(logoutLink).double_click().perform()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

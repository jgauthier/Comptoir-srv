# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class SearchKeyWordLibreFilterAtLeastOneComment(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_search_key_word_libre_filter_at_least_one_comment(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("search").clear()
        driver.find_element_by_id("search").send_keys("libre")
        driver.find_element_by_xpath("//button[@type='submit']").click()
        self.assertTrue(self.is_element_present(By.XPATH, u"//h1[contains(text(),\"33 résultats pour\")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//h2[contains(text(),\"21 logiciels pour \")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//h2[contains(text(),\"12 utilisateurs pour \")]"))
        Select(driver.find_element_by_name("reviewed")).select_by_visible_text("Oui")
        driver.find_element_by_xpath("(//button[@type='submit'])[3]").click()
        self.assertTrue(self.is_element_present(By.XPATH, u"//h1[contains(text(),\"17 résultats pour\")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//h2[contains(text(),\"5 logiciels pour \")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//h2[contains(text(),\"12 utilisateurs pour \")]"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Alfresco\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"i-Parapheur\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"LibreOffice\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"Lutèce\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Publik\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"2i2l\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Adullact\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Libriciel SCOP\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Linagora (92)\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Maarch\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Matthieu FAURE\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"MutuaLibre et Ligue de...\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Open-DSI\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Roland Mas\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Savoir-faire Linux\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, u"//*[contains(text(),\"Syloé\")]/ancestor::li"))
        self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),\"Teclib\")]/ancestor::li"))

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

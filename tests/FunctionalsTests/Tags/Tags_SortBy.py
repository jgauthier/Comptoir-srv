# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import time
import os
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest
from pyvirtualdisplay import Display

class TagsSortBy(unittest.TestCase):
    def setUp(self):
        self.display = Display(visible=0, size=(1920, 1080))
        self.display.start()
        profile = webdriver.FirefoxProfile()
        # setting the locale french : ‘fr’
        profile.set_preference("intl.accept_languages","fr")
        self.driver = webdriver.Firefox(profile)
        self.driver.set_window_size(1920,1080)
        self.driver.implicitly_wait(20)
        self.base_url = "http://192.168.10.31:8080"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_tags_sort_by_name_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text, "new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text, "tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text, "test")

    def test_tags_sort_by_name_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('name.desc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,"test")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,"tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,"new")

    def test_tags_sort_by_occurences_asc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('used_tag_number.asc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        time.sleep(5)
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,"test")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,"new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,"tag")

    def test_tags_sort_by_occurences_desc(self):
        driver = self.driver
        driver.get(self.base_url + "/tags")
        Select(driver.find_element_by_name("order")).select_by_value('used_tag_number.desc')
        driver.find_element_by_xpath("(//button[@type='submit'])[2]").click()
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li/a/div").text,"tag")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[2]/a/div").text,"new")
        self.assertEqual(driver.find_element_by_xpath("//div[@id='super-main']/main/section[2]/ul/li[3]/a/div").text,"test")

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()

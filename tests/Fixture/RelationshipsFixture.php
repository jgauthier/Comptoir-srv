<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipsFixture
 *
 */
class RelationshipsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'text', 'length' => 25, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'cd' => ['type' => 'text', 'length' => 25, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'relationship_type_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_indexes' => [
            'idx_26159_fk_relationships_relationship_types_idx' => ['type' => 'index', 'columns' => ['relationship_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_relationships_relationship_types' => ['type' => 'foreign', 'columns' => ['relationship_type_id'], 'references' => ['relationship_types', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//          "id" => 1,
            'name' => 'CreatorOf',
            'created' => 1460127669,
            'modified' => 1460127669,
            'cd' => 'CreatorOf',
            'relationship_type_id' => 1
        ],
        [
//          "id" => 2,
            'name' => 'UserOf',
            'created' => 1460127669,
            'modified' => 1460127669,
            'cd' => 'UserOf',
            'relationship_type_id' => 2
        ],
        [
//          "id" => 3,
            'name' => 'ProviderFor',
            'created' => 1460127669,
            'modified' => 1460127669,
            'cd' => 'ProviderFor',
            'relationship_type_id' => 2
        ],
        [
//          "id" => 4,
            'name' => 'Works well with',
            'created' => 1460127669,
            'modified' => 1460127669,
            'cd' => 'WorksWellWith',
            'relationship_type_id' => 2
        ],
    ];
}

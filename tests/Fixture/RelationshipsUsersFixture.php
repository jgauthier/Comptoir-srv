<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipsUsersFixture
 *
 */
class RelationshipsUsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'user_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'recipient_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'relationship_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'idx_26507_fk_entities_relationships_entities_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'idx_26507_fk_entities_relationships_users_recipient_idx' => ['type' => 'index', 'columns' => ['recipient_id'], 'length' => []],
            'idx_26507_fk_entities_relationships_relationtion_types_idx' => ['type' => 'index', 'columns' => ['relationship_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_entities_relationships_relationtion_types' => ['type' => 'foreign', 'columns' => ['relationship_id'], 'references' => ['relationships', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_entities_relationships_users' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_entities_relationships_users_recipient' => ['type' => 'foreign', 'columns' => ['recipient_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'user_id' => 1,
            'recipient_id' => 1,
            'relationship_id' => 1,
            'created' => 1487084567,
            'modified' => 1487084567
        ],
    ];
}

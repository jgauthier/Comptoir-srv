<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserTypesFixture
 *
 */
class UserTypesFixture extends TestFixture {

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'cd' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'name' => 'Association',
            'created' => 1487084570,
            'modified' => 1487084570,
            'cd' => 'Asso'
        ],
        [
//            'id' => 2,
            'name' => 'Company',
            'created' => 1487084570,
            'modified' => 1487084570,
            'cd' => 'Company'
        ],
        [
//            'id' => 3,
            'name' => 'Administration',
            'created' => 1487084570,
            'modified' => 1487084570,
            'cd' => 'Administration'
        ],
        [
//            'id' => 4,
            'name' => 'Person',
            'created' => 1487084570,
            'modified' => 1487084570,
            'cd' => 'Person'
        ],

    ];
}

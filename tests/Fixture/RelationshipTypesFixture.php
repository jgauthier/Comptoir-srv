<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipTypesFixture
 *
 */
class RelationshipTypesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'cd' => ['type' => 'string', 'length' => 25, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'idx_26513_cd_unique' => ['type' => 'unique', 'columns' => ['cd'], 'length' => [25]],
            'idx_26513_name_unique' => ['type' => 'unique', 'columns' => ['name'], 'length' => [25]],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [

        [
            'name' => 'UserToUser',
            'created' => 1460130183,
            'modified' => 1460130183,
            'cd' => 'UserToUser'
        ],
        [
            'name' => 'UserToSoftware',
            'created' => 1460130183,
            'modified' => 1460130183,
            'cd' => 'UserToSoftware'
        ],
        [
            'name' => 'SoftwareToSoftware',
            'created' => 1460130183,
            'modified' => 1460130183,
            'cd' => 'SoftwareToSoftware'
        ],
    ];
}

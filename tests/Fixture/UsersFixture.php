<?php
namespace App\Test\Fixture;

use Cake\Auth\DigestAuthenticate;
use Cake\TestSuite\Fixture\TestFixture;

/**
 * UsersFixture
 *
 */
class UsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'username' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'logo_directory' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'url' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'user_type_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'description' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'role' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null],
        'password' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null],
        'email' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'photo' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'active' => ['type' => 'boolean', 'length' => null, 'default' => 0, 'null' => true, 'comment' => null, 'precision' => null],
        'digest_hash' => ['type' => 'text', 'length' => null, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'token' => ['type' => 'uuid', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'idx_26549_fk_users_user_types_idx' => ['type' => 'index', 'columns' => ['user_type_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_user_user_type' => ['type' => 'foreign', 'columns' => ['user_type_id'], 'references' => ['user_types', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];

    public function init()
    {
        $digestAdmin = DigestAuthenticate::password("fake-email-admin@comptoir-du-libre.org", "fake-password" , 'srv.comptoir-du-libre.org');
        $digestCompany = DigestAuthenticate::password("fake-email-company@comptoir-du-libre.org", "fake-password" , 'srv.comptoir-du-libre.org');
        $digestAdministration = DigestAuthenticate::password("fake-email-administration@comptoir-du-libre.org", "fake-password" , 'srv.comptoir-du-libre.org');
        $digestPerson = DigestAuthenticate::password("fake-email-person@comptoir-du-libre.org", "fake-password" , 'srv.comptoir-du-libre.org');

        $this->records = [
            [
//            "id" => 1,
                "username" => "Admin User",
                "created" => 1487084570,
                "logo_directory" => TESTS . "TestFiles" . DS . "Users/photo/id1/avatar",
                "url" => "http://www.adullact.org/",
                "user_type_id" => 1,
                "description" => "ADULLACT a pour objectifs de soutenir et coordonner l action des Administrations et Collectivités territoriales dans le but de promouvoir, développer et maintenir un patrimoine de logiciels libres utiles aux missions de service public.",
                "modified" => 1487084570,
                "role" => "admin",
                "password" => "fake-password",
                "email" => "fake-email-admin@comptoir-du-libre.org",
                "photo" => "correctUserAvatar.jpg",
                "active" => false,
                "digest_hash" =>  $digestAdmin,
                "token" => null
            ],
            [
//            "id" => 2,
                "username" => "Avencall Company",
                "created" => 1487084570,
                "logo_directory" => TESTS . "TestFiles" . DS . "Users/photo/id2/avatar",
                "url" => "http://www.avencall.com/",
                "user_type_id" => 2,
                "description" => "Avencall transforme l’industrie des télécommunications d’entreprise et son modèle économique en donnant un accès universel à des solutions de téléphonie qui assurent à ses clients liberté, évolutivité et créativité au service de leur métier. Une réussite rendue possible par la collaboration et le savoir-faire de ses équipes.",
                "modified" => 1487084570,
                "role" => "user",
                "password" => "fake-password",
                "email" => "fake-email-company@comptoir-du-libre.org",
                "photo" => "correctUserAvatar.jpg",
                "active" => false,
                "digest_hash" => $digestCompany,
                "token" => null
            ],
            [
//            "id" => 3,
                "username" => "Administration",
                "created" => 1487084570,
                "logo_directory" => TESTS . "TestFiles" . DS . "Users/photo/id3/avatar",
                "url" => "http://www.avencall.com/",
                "user_type_id" => 3,
                "description" => "description",
                "modified" => 1487084570,
                "role" => "user",
                "password" => "fake-password",
                "email" => "fake-email-administration@comptoir-du-libre.org",
                "photo" => "correctUserAvatar.jpg",
                "active" => false,
                "digest_hash" => $digestAdministration,
                "token" => null
            ],
            [
//            "id" => 4,
                "username" => "Person",
                "created" => 1487084570,
                "logo_directory" => TESTS . "TestFiles" . DS . "Users/photo/id4/avatar",
                "url" => "http://www.avencall.com/",
                "user_type_id" => 4,
                "description" => "",
                "modified" => 1487084570,
                "role" => "user",
                "password" => "fake-password",
                "email" => "fake-email-person@comptoir-du-libre.org",
                "photo" => "correctUserAvatar.jpg",
                "active" => false,
                "digest_hash" =>  $digestPerson,
                "token" => null
            ]
        ];
        parent::init();
    }
}

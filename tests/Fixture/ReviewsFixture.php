<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReviewsFixture
 *
 */
class ReviewsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'comment' => ['type' => 'text', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null],
        'title' => ['type' => 'text', 'length' => 255, 'default' => null, 'null' => false, 'collate' => null, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'user_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'software_id' => ['type' => 'integer', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'evaluation' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'idx_26522_fk_review_user_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'idx_26522_fk_review_software_idx' => ['type' => 'index', 'columns' => ['software_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_review_software' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_review_user' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'comment' => 'Commentaire sur le soft id = 1 par utilisateur id = 1',
            'title' => 'Titre de commentaire',
            'created' => 1487084567,
            'user_id' => 1,
            'software_id' => 1,
            'evaluation' => 3,
            'modified' => 1487084567
        ],
        [
//            'id' => 2,
            'comment' => "Commentaire sur le soft id = 1 par utilisateur id = 2",
            'title' => 'Titre de commentaire 2 ',
            'created' => 1487084567,
            'user_id' => 2,
            'software_id' => 1,
            'evaluation' => 5,
            'modified' => 1487084567
        ],
        [
//            'id' => 3,
            'comment' => 'Commentaire sur le soft id = 1 par utilisateur id = 3',
            'title' => 'Titre de commentaire 3',
            'created' => 1487084567,
            'user_id' => 3,
            'software_id' => 1,
            'evaluation' => 2,
            'modified' => 1487084567
        ],
        [
//            'id' => 4,
            'comment' => 'Commentaire sur le soft id = 2 par utilisateur id = 3',
            'title' => 'Titre de commentaire 4',
            'created' => 1487084567,
            'user_id' => 3,
            'software_id' => 2,
            'evaluation' => 3,
            'modified' => 1487084567
        ],
    ];
}

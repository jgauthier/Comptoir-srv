<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipsSoftwaresUsersFixture
 *
 */
class RelationshipsSoftwaresUsersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'software_id' => ['type' => 'integer', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'relationship_id' => ['type' => 'biginteger', 'length' => 20, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'idx_26501_fk_entity_software_relationship_entity_idx' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'idx_26501_fk_entity_software_relationship_relationship_type_idx' => ['type' => 'index', 'columns' => ['relationship_id'], 'length' => []],
            'idx_26501_fk_user_software_relationship_software' => ['type' => 'index', 'columns' => ['software_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_entity_software_relationship_entity' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_user_software_relationship_relationship_type' => ['type' => 'foreign', 'columns' => ['relationship_id'], 'references' => ['relationships', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_user_software_relationship_software' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
       [
            "software_id"=> 1,
            "user_id"=> 3,
            "relationship_id"=> 2,
            "created"=> 1487084568,
            "modified"=> 1487084568,
        ],
        [
            "software_id"=> 1,
            "user_id"=> 1,
            "relationship_id"=> 1,
            "created"=> 1487084568,
            "modified"=> 1487084568,
        ],
        [
            "software_id"=> 2,
            "user_id"=> 3,
            "relationship_id"=> 3,
            "created"=> 1487084568,
            "modified"=> 1487084568,
        ],
        [
            "software_id"=> 2,
            "user_id"=> 3,
            "relationship_id"=> 3,
            "created"=> 1487084568,
            "modified"=> 1487084568,
        ]
    ];
}

<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RawMetricsSoftwaresFixture
 *
 */
class RawMetricsSoftwaresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'software_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'name' => ['type' => 'string', 'length' => 255, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'last_commit_age' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'high_committer_percent' => ['type' => 'float', 'length' => null, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'number_of_contributors' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'declared_users' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'average_review_score' => ['type' => 'decimal', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'screenshots' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'code_gouv_label' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'metrics_date' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'project_age' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'delta_commit_one_month' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'delta_commit_twelve_month' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'commit_activity_one_month' => ['type' => 'float', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'commit_activity_twelve_month' => ['type' => 'float', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'raw_metrics_softwares_software_id_fkey' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'cascade', 'cascade' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'software_id' => 3,
            'name' => 'Asalae',
            'created' => 1487084564,
            'modified' => 1487084564,
            'last_commit_age' => 175,
            'high_committer_percent' => 62.9,
            'number_of_contributors' => 7,
            'declared_users' => 5,
            'average_review_score' => 1.5,
            'screenshots' => 15,
            'code_gouv_label' => 0,
            'metrics_date' => 1487084564,
            'project_age' => 2482,
            'delta_commit_one_month' => 0,
            'delta_commit_twelve_month' => 485,
            'commit_activity_one_month' => 0,
            'commit_activity_twelve_month' => 485
        ],
        [
//            'id' => 2,
            'software_id' => 1,
            'name' => 'Lutèce',
            'created' => 1487084564,
            'modified' => 1487084564,
            'last_commit_age' => 74,
            'high_committer_percent' => 24,
            'number_of_contributors' => 5,
            'declared_users' => 4,
            'average_review_score' => 3.5,
            'screenshots' => 4,
            'code_gouv_label' => 0,
            'metrics_date' => 1487084564,
            'project_age' => 3195,
            'delta_commit_one_month' => 193,
            'delta_commit_twelve_month' => 235,
            'commit_activity_one_month' => 5,
            'commit_activity_twelve_month' => 50
        ]
    ];
}

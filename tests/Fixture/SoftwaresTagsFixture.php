<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SoftwaresTagsFixture
 *
 */
class SoftwaresTagsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'software_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'tag_id' => ['type' => 'integer', 'length' => 10, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'softwares_tags_software_id' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'softwares_tags_tag_id' => ['type' => 'foreign', 'columns' => ['tag_id'], 'references' => ['tags', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */

    public function init()
    {
        $this->records = [
            [
                'software_id' => 1,
                'tag_id' => 1
            ],
            [
                'software_id' => 3,
                'tag_id' => 1
            ],
            [
                'software_id' => 3,
                'tag_id' => 3
            ],
            [
                'software_id' => 2,
                'tag_id' => 2
            ],
            [
                'software_id' => 3,
                'tag_id' => 2
            ],
            [
                'software_id' => 1,
                'tag_id' => 5
            ],
        ];
        parent::init();
    }
}

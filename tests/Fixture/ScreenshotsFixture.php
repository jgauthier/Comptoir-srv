<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ScreenshotsFixture
 *
 */
class ScreenshotsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => 20, 'autoIncrement' => true, 'default' => null, 'null' => false, 'comment' => null, 'precision' => null, 'unsigned' => null],
        'software_id' => ['type' => 'integer', 'length' => 20, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null, 'unsigned' => null, 'autoIncrement' => null],
        'url_directory' => ['type' => 'text', 'length' => 255, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'default' => null, 'null' => true, 'comment' => null, 'precision' => null],
        'name' => ['type' => 'text', 'length' => 255, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        'photo' => ['type' => 'text', 'length' => 255, 'default' => null, 'null' => true, 'collate' => null, 'comment' => null, 'precision' => null],
        '_indexes' => [
            'idx_26531_fk_screens_software_idx' => ['type' => 'index', 'columns' => ['software_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_screens_software' => ['type' => 'foreign', 'columns' => ['software_id'], 'references' => ['softwares', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
//            'id' => 1,
            'software_id' => 1,
            'url_directory' => TESTS . "TestFiles/Screenshots/photo/",
            'created' => 1487084568,
            'modified' => 1487084568,
            'name' => '',
            'photo' => '1469540737_Screenshot_spip_forum.png'
        ],
        [
//            'id' => 2,
            'software_id' => 1,
            'url_directory' => TESTS . "TestFiles/Screenshots/photo/",
            'created' => 1487084568,
            'modified' => 1487084568,
            'name' => '',
            'photo' => '1469540737_Screenshot_spip_forum.png'
        ],
        [
//            'id' => 3,
            'software_id' => 2,
            'url_directory' => TESTS . "TestFiles/Screenshots/photo/",
            'created' => 1487084568,
            'modified' => 1487084568,
            'name' => '',
            'photo' => '1469540763_Screenshot_spip_stat.png'
        ],
    ];
}

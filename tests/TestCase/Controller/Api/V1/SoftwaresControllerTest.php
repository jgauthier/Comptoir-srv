<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\SoftwaresController;
use Cake\Event\EventList;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\SoftwaresController Test Case
 * @property \App\Model\Table\SoftwaresTable $Softwares
 */
class SoftwaresControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */

    public $fixtures = [
        'app.softwares',
        'app.users',
        'app.user_types',
        'app.relationships_softwares_users',
        'app.licenses',
        'app.licence_types',
        'app.reviews',
        'app.screenshots',
        'app.relationships',
        'app.relationship_types',
        'app.tags',
        'app.softwares_tags'
    ];

    public $rules;

    public $correctFile;
    public $wrongFile;
    public $exceedFile;

    public function setUp()
    {

        parent::setUp();


        $this->correctFile = [
            'name' => 'correctAvatarLogo.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . "correctAvatarLogo.jpg",
            'type' => 'image/jpeg',
            'size' => 8407,
            'error' => 0
        ];
        $this->exceedFile = [
            'name' => 'exceedPhpSizeLimitUserAvatar.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . 'exceedPhpSizeLimitUserAvatar.jpg',
            'type' => 'image/jpeg',
            'size' => 0,
            'error' => 1
        ];
        $this->wrongFile = [
            'name' => 'wrongAvatarLogo.jpg',
            'tmp_name' => TESTS . "TestFiles" . DS . "Common" . DS . 'wrongAvatarLogo.jpg',
            'type' => 'image/jpeg',
            'size' => 1132550,
            'error' => 0
        ];

        $this->licenses = [
            "1" => "CeCill V2",
            "2" => "MIT",
            "3" => "BSD",
            "4" => "LGPLv3",
            "5" => "GNU GPLv2"
        ];

        $this->rules = [
            "id" => [
                "presenceRequired" => false,
                "allowEmpty" => "create"
            ],
            "softwarename" => [
                "presenceRequired" => true,
                "allowEmpty" => false
            ],
            "url_repository" => [
                "presenceRequired" => "create",
                "allowEmpty" => false,
                "url" => [
                    "rule" => "url",
                    "message" => null,
                    "limit" => false
                ]
            ],
            "description" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "logo_directory" => [
                "presenceRequired" => false,
                "allowEmpty" => true
            ],
            "photo" => [
                "presenceRequired" => "create",
                "allowEmpty" => "update",
                "file" => [
                    "rule" => "mimeType",
                    "message" => "This format is not allowed. Please use one in this list: JPEG, PNG, GIF, SVG.",
                    "limit" => [
                        "image/jpeg",
                        "image/png",
                        "image/gif",
                        "image/svg+xml"
                    ]
                ],
                "fileBelowMaxSize" => [
                    "rule" => "isBelowMaxSize",
                    "message" => "The maximum weight allowed is 1MB, please use a file less than 1MB.",
                    "limit" => 1048576
                ],
                "fileBelowMaxHeight" => [
                    "rule" => "isBelowMaxHeight",
                    "message" => "The size of your image is too big, please use an image fitting a 350x350px size.",
                    "limit" => 350
                ],
                "fileBelowMaxWidth" => [
                    "rule" => "isBelowMaxWidth",
                    "message" => "The size of your image is too big, please use an image fitting a 350x350px size.",
                    "limit" => 350
                ],
            ],
        ];


        $config = TableRegistry::exists('Softwares') ? [] : ['className' => 'App\Model\Table\SoftwaresTable'];
        $this->Softwares = TableRegistry::get('Softwares', $config);

        $this->Softwares->eventManager()->setEventList(new EventList());

        $this->Softwares->Reviews->eventManager()->setEventList(new EventList());

        $this->Softwares->Screenshots->eventManager()->setEventList(new EventList());


    }

    /**
     * Test add a software with a correct image.
     */
    public function testAddWithGoodLogo()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.mynewsoftware.com/",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                "url_website" => null,
                'photo' => $this->correctFile,
                "tag_string" => "test aze great",
            ];

        $this->post('api/v1/softwares/add.json', $data);

        $this->assertEventFired('Model.Software.created', $this->Softwares->eventManager());

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $this->assertContains("Success", $this->_response->body());

        //Check in files directory
        $this->assertFileExists(WWW_ROOT . 'img' . DS . "files/Softwares/My new software/avatar/" . $this->correctFile["name"]);

        //Delete the file after testing
        unlink(WWW_ROOT . 'img' . DS . "files/Softwares/My new software/avatar/" . $this->correctFile["name"]);
        $this->assertFileNotExists(WWW_ROOT . 'img' . DS . "files/Softwares/My new software/avatar/" . $this->correctFile["name"]);
    }

    /**
     * Test add a software with a correct image.
     */
    public function testAddWithWrongLogo()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.mynewsoftware.com/",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
                "url_website" => null,
                'photo' => $this->wrongFile,
            ];

        $this->post('api/v1/softwares/add.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $expected = [
            "software" => [
                "photo" => [
                    "fileBelowMaxSize" => "The maximum weight allowed is 1MB, please use a file less than 1MB.",
                    "fileBelowMaxHeight" => "The size of your image is too big, please use an image fitting a 350x350px size."
                ]
            ],
            "licenses" => $this->licenses,
            "rules" => $this->rules,
            'message' => "Error",
        ];

        $this->assertContains("Error", $this->_response->body());

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test if an user can be declared as user of a software
     * Test usersSoftware method
     */
    public function testDeclareAsUserOf()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/usersSoftware/2.json');

        $expected =
            [
                "software" => [
                    "id" => 2,
                    "softwarename" => "Lutèce",
                    "url_repository" => "http://www.fake-repo-lutece.git",
                    "description" => "Lutèce description",
                    "licence_id" => 1,
                    "created" => "2017-02-14T15:02:48+00:00",
                    "modified" => "2017-02-14T15:02:48+00:00",
                    "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                    "photo" => "correctSoftwareLogo.jpg",
                    "url_website" => "http://www.fake-lutece.com",
                    "userssoftwares" => [],
                    "average_review" => 0,
                    "tag_string" => "",
                ],
                "message" => "Success",
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $RSU_Table->exists(["software_id" => 1, "user_id" => 3]);

        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test if an user can be undeclared as user of a software
     * Test deleteServicesProviders method
     */
    public function testFallBackAsUserOf()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteUsersSoftware/1.json');

        $expected =
            [
                "message" => "Success",
            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $RSU_Table = TableRegistry::get('RelationshipsSoftwaresUsers');
        $this->assertFalse($RSU_Table->exists(["software_id" => 1, "user_id" => 3]));

        $this->assertResponseOk();

        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     */
    public function testAddreview()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];


        $this->post('api/v1/softwares/3/reviews.json', $data);

        $expected =
            [
                "message" => "Success",
                "review" => [
                    "comment" => "My comment",
                    "title" => "My title",
                    "evaluation" => 4,
                    "software_id" => 3,
                    "user_id" => 1,
                    "created" => "2016-10-11T09:50:52+00:00",
                    "modified" => "2016-10-11T09:50:52+00:00",
                    "id" => 2
                ]

            ];

        $this->assertResponseOk();
        $this->assertContains($expected["message"], $this->_response->body());
        $this->assertContains($expected['review']['comment'], $this->_response->body());
        $this->assertContains($expected['review']['title'], $this->_response->body());
        $this->assertContains((string)$expected['review']['software_id'], $this->_response->body());

        $this->assertEventFired('Model.Review.created', $this->Softwares->Reviews->eventManager());
    }

    public function testAddreviewNotAuthorizedUser()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];

        $this->post('api/v1/softwares/3/reviews.json', $data);

        $this->assertResponseCode(403);
    }


    /**
     */
    public function testAddreviewTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);


        $data = [
            "comment" => "My comment",
            "title" => "My title",
            "evaluation" => 4,
            "software_id" => 3,
            "user_id" => 1
        ];

        $this->post('api/v1/softwares/2/reviews.json', $data);
        $this->post('api/v1/softwares/2/reviews.json', $data);

        $expected =
            [
                "message" => "Error",
                "review" => [
                    "software_id" => [
                        "_isUnique" => "You can not post more than one review for a software."
                    ]
                ]
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertResponseOk();

        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     *
     */
    public function testDeclareAsUserOfTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/usersSoftware/1.json');

        $expected =
            [
                "software" => [
                    "software_id" => [
                        "_isUnique" => "You can not be declare twice for the same relationships."
                    ]
                ],
                "message" => "Error",
            ];

        $this->post('api/v1/softwares/usersSoftware/1.json');

        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertResponseOk();

        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     */
    public function testDeclareAsProviderFor()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/servicesProviders/1.json');

        $expected =
            [
                "software" => [
                    "id" => 1,
                    "softwarename" => "Asalae",
                    "url_repository" => "http://www.fake-repo-asalae.git",
                    "description" => "Asalae description",
                    "licence_id" => 1,
                    "created" => "2017-02-14T15:02:48+00:00",
                    "modified" => "2017-02-14T15:02:48+00:00",
                    "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                    "photo" => "correctSoftwareLogo.jpg",
                    "url_website" => "http://www.fake-asalae.com",

                    "providerssoftwares" => [],
                    "average_review" => 0,
                    "tag_string" => "",

                ],
                "message" => "Success",

            ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertResponseOk();
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     */
    public function testDeclareAsProviderForTwice()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->post('api/v1/softwares/servicesProviders/1.json');

        $this->assertResponseOk();

        $expected =
            [
                "software" => [
                    "software_id" => [
                        "_isUnique" => "You can not be declare twice for the same relationships."
                    ]
                ],
                "message" => "Error",
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->post('api/v1/softwares/servicesProviders/1.json');

        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method
     *Don't test the license inserrt. Problem in fixture of License of lisense_types
     * @return void
     */
    public function testAddAuthenticatedTrue()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "http://www.url.fr",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
            ];

        $this->post('api/v1/softwares.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $this->assertContains("Error", $this->_response->body());
        $this->assertContains("This field is required", $this->_response->body());
    }

    /**
     * Test add method
     *Don't test the license inserrt. Problem in fixture of License of lisense_types
     * @return void
     */
    public function testAddAuthenticatedFalse()
    {
        $this->post('api/v1/softwares.json');

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "url",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
            ];

        $this->post('api/v1/softwares/add.json', $data);


        $this->assertResponseCode(302);
    }

    /**
     * Test add method
     *  Test when bad url was given for url_repository
     * @return void
     */
    public function testAddBadUrlOnUrlRepository()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data =
            [
                'softwarename' => 'My new software',
                'url_repository' => "url",
                'description' => 'This is my new software and...',
                'licence_id' => 1,
                'created' => "2017-02-14T15:02:48+00:00",
                'modified' => "2017-02-14T15:02:48+00:00",
            ];

        $this->post('api/v1/softwares.json', $data);

        $this->assertContains('"url": "The provided value is invalid"', $this->_response->body());
    }

    /**
     * Test add Relationships Softwares users
     *
     * @return void
     */
    public function testusersSoftware()
    {
        $this->post('api/v1/softwares/usersSoftware/3.json', []);
        $this->assertResponseCode('302');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "software_id" => 3,
                "user_id" => 1,
                "relationship_id" => 2,
                "created" => null,
                "modified" => null,

            ];

        $this->post('api/v1/softwares/usersSoftware/3.json', $data);

        // Vérifie que le code de réponse est 200
        $this->assertResponseOk();

        $expected = [
            "software" => [
                "id" => 3,
                "softwarename" => "Soft 3",
                "url_repository" => "http://www.fake-repo-soft3.git",
                "description" => "Lutèce description",
                "licence_id" => 1,
                "created" => "2017-02-14T15:02:48+00:00",
                "modified" => "2017-02-14T15:02:48+00:00",
                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                "photo" => "correctSoftwareLogo.jpg",
                "url_website" => "http://www.fake-soft3.com",
                "userssoftwares" => [],
                "average_review" => 0,
                "tag_string" => "",
            ],
            "message" => "Success"
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add Relationships Softwares users
     *
     * @return void
     */
    public function testusersSoftwareWithBadDatas()
    {
        $this->post('api/v1/softwares/usersSoftware/1.json', []);
        $this->assertResponseCode('302');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);


        $this->post('api/v1/softwares/usersSoftware/10.json', []);

        // Vérifie que le code de réponse est 200
        $this->assertResponseCode('404');
    }

    public function testDeleteUsersSoftwareUserNotMatch()
    {

        $this->post('api/v1/softwares/deleteUsersSoftware/1.json', []);
        $this->assertResponseCode('302');

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteUsersSoftware/2.json', []);

        $this->assertResponseOk();

        $expected = [
            "message" => "No relationship in our data base."
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    public function testDeleteUsersSoftwareUser()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "software_id" => 1,
                "user_id" => 3,
                "relationship_id" => 2,
                "created" => null,
                "modified" => null,

            ];

        $this->post('api/v1/softwares/usersSoftware/1.json', $data);


        $this->delete('api/v1/softwares/deleteUsersSoftware/1.json', []);

        $this->assertResponseOk();

        $expected = [
            "message" => "Success"
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test the deleteServicesProvider method
     * A user can roll back himself of the list of service providers for a software
     * But if he does no in the list an error message will be sent.
     */
    public function testDeleteServicesProvidersNotMatch()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteServicesProviders/1.json', []);

        $expected = [
            "message" => "No relationship in our data base."
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test the deleteServicesProvider method
     * A user can roll back himself of the list of service providers for a software
     */
    public function testDeleteServicesProviders()
    {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $this->delete('api/v1/softwares/deleteServicesProviders/2.json', []);

        $this->assertResponseOk();

        $expected = [
            "message" => "Success"
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftware()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                ]
            ]
        ]);

        $data =
            [
                "softwarename" => "toto",
            ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        $this->Softwares->exists(["id" => 2, "softwarename" => "toto"]);

        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software
     * All connected users can modify a software as he wants.
     * If the user is not connected then return error code => 401 = Unauthorized
     */
    public function testEditSoftwareNotConnectedUser()
    {
        $data =
            [
                "softwarename" => "toto",
            ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseCode(302);
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software => EDIT THE LOGO => fields 'logo_directory' and 'photo'
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareLogo()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "photo" => $this->correctFile
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        $this->assertEventFired('Model.Software.modified', $this->Softwares->eventManager());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 2, "logo_directory" => "files/Softwares/Lutèce/avatar", "photo" => $this->correctFile["name"]]);

        $this->assertEquals(1, $query->count());
        //Check in files directory
        $this->assertFileExists(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar/" . $this->correctFile["name"]);

        //Delete the file after testing
        unlink(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar/" . $this->correctFile["name"]);
        rmdir(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar");
        rmdir(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce");
        $this->assertFileNotExists(WWW_ROOT . 'img' . DS . "files/Softwares/Lutèce/avatar/" . $this->correctFile["name"]);
    }


    /**
     * Test the edit method
     * Only a connected user can edit a software => EDIT THE LOGO WRONG FILE => fields 'logo_directory' and 'photo'
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareLogoDoesNotRespectTheRules()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "photo" => $this->wrongFile
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        $this->assertContains("errors", $this->_response->body());

        //Value of message
        $this->assertContains("error", $this->_response->body());

        //Check in the database
        $query = $this->Softwares->find()->where(["id" => 2, "logo_directory" => "files/Softwares/2/photo/avatar", "photo" => $this->wrongFile["name"]]);
        $this->assertEquals(0, $query->count());

        $this->assertFileNotExists(WWW_ROOT . 'img' . DS . "files/Softwares/2/photo/avatar/" . $this->wrongFile["name"]);
    }

    /**
     * Test the edit method
     * Only a connected user can edit a software => ADD A SCREENSHOT
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareAddAScreenshot()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "screenshots" => [
                "0" => [
                    "photo" => $this->correctFile]
            ]
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();

        //Check in the database
        $query = $this->Softwares->Screenshots->find()->where(["software_id" => 2, "url_directory" => "files/Screenshots", "photo" => time() . "_" . $this->correctFile["name"]]);
        $this->assertEventFired('Model.Screenshot.created', $this->Softwares->Screenshots->eventManager());

        $this->assertEquals(1, $query->count());
        //Check in files directory
        $this->assertFileExists(WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->correctFile["name"]);

        //Delete the file after testing
        unlink(WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->correctFile["name"]);
        $this->assertFileNotExists(WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->correctFile["name"]);
    }


    /**
     * Test the edit method
     * Only a connected user can edit a software => ADD A WRONG SCREENSHOT
     * All connected users can modify a software as he wants.
     */
    public function testEditSoftwareAddAWrongScreenshot()
    {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 2,
                    "user_type_id" => 2,
                ]
            ]
        ]);

        $data = [
            "softwarename" => "Lutèce",
            "screenshots" => [
                "0" => [
                    "photo" => $this->wrongFile]
            ]
        ];

        $this->put('api/v1/softwares/2.json', $data);
        $this->assertResponseOk();
        $this->assertContains("errors", $this->_response->body());

        //Check in the database
        $query = $this->Softwares->Screenshots->find()->where(["software_id" => 2, "url_directory" => "files/Screenshots", "photo" => time() . "_" . $this->wrongFile["name"]]);

        $this->assertEquals(0, $query->count());
        $this->assertFileNotExists(WWW_ROOT . 'img' . DS . "files/Screenshots/" . time() . "_" . $this->wrongFile["name"]);
    }
}

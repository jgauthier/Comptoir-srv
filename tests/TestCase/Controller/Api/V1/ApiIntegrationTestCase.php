<?php

namespace App\Test\TestCase\Controller\Api\V1;

use Cake\Auth\DefaultPasswordHasher;
use Cake\TestSuite\IntegrationTestCase;
use Cake\ORM\TableRegistry;


abstract class ApiIntegrationTestCase extends IntegrationTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->configRequest(["headers" => ["Accept-Language"=>"en"]]);

        // Update all users with the password "goo-password"
        $UsersTable = TableRegistry::get('Users');
        $password = (new DefaultPasswordHasher)->hash("good-password");
        $UsersTable->updateAll(['password' => $password], []);
    }


    public function login($email, $password) {
        $this->post("users/login",["email"=>$email,"password"=>$password]);

        $this->session([
            'Auth' => [
                'User' => $this->_controller->Auth->identify()
            ]
        ]);
    }


}

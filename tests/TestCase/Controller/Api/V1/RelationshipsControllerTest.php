<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RelationshipsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RelationshipsController Test Case
 */
class RelationshipsControllerTest extends IntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships',
        'app.relationship_types',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.reviews',
        'app.users',
        'app.user_types',
//        'app.relationships_users',
//        'app.relationships_softwares_users',
        'app.screenshots',
//        'app.relationships_softwares'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete() {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

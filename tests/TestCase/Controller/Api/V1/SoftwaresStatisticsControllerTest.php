<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\SoftwaresStatisticsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SoftwaresStatisticsController Test Case
 */
class SoftwaresStatisticsControllerTest extends IntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.softwares_statistics',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.reviews',
        'app.users',
        'app.user_types',
        'app.relationships_softwares',
        'app.relationships',
        'app.relationship_types',
        'app.screenshots',
        'app.relationships_softwares_users',
        'app.relationships_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete() {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

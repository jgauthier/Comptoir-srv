<?php
namespace App\Test\TestCase\Controller\Api\V1;




/**
 * App\Controller\SoftwaresTagsController Test Case
 */
class SoftwaresTagsControllerTest extends ApiIntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.licence_types',
        'app.licenses',
        'app.softwares',
        'app.tags',
        'app.softwares_tags',

        'app.relationships_softwares',

        'app.users',
        'app.user_types',
        'app.relationships_softwares_users',
        'app.relationships',
        'app.relationship_types',
        'app.softwares_statistics'
    ];


    /**
     * Test delete method of Softwares. If software id=2 is deleted, Softwares_Tags relationships must be delete too.
     * delete method is allow only if you'r an authentified user.
     * '"software_id": 2' have to NOT be found.
     *
     * @param
     * @return void
     */
    public function testDeleteRelationshipAfterDeleteSoftware()
    {

        $this->login("fake-email-administration@comptoir-du-libre.org","good-password");


        $this->delete('api/v1/softwares/2.json');

        $this->get('/api/v1/softwares-tags.json');

        $this->assertNotContains('"software_id": 2',$this->_response->body());
        $this->assertResponseOk();
    }



    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

}

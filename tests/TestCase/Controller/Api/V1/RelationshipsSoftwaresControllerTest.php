<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RelationshipsSoftwaresController;
use Cake\ORM\TableRegistry;


/**
 * App\Controller\RelationshipsSoftwaresController Test Case
 * @property \App\Model\Table\RelationshipsSoftwaresTable $RelationshipsSoftwares
 */
class RelationshipsSoftwaresControllerTest extends ApiIntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_softwares',
        'app.reviews',
        'app.softwares',
        'app.users',
        'app.user_types',
        'app.licenses',
        'app.licence_types',
        'app.screenshots',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_softwares_users'
    ];

    public $RelationshipsSoftwares ;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelationshipsSoftwares') ? [] : ['className' => 'App\Model\Table\RelationshipsSoftwaresTable'];
        $this->RelationshipsSoftwares = TableRegistry::get('RelationshipsSoftwares', $config);
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     * Should return a json containing a relationshipsSoftware.
     * @return void
     */
    public function testView() {

        $this->get("api/v1/relationshipsSoftwares/view/1.json");
        $this->assertResponseOk();

        $relationship = [
            "relationshipsSoftware"=> [
                "id" => 1,
                'software_id' => 1,
                'relationship_id' => 1,
                'recipient_id' => 2,
                'created' => "2016-04-08T15:44:26+00:00",
                'modified' => "2016-04-08T15:44:26+00:00"
            ]
        ];

        $relationship = json_encode($relationship, JSON_PRETTY_PRINT);

        $this->assertEquals($relationship,$this->_response->body());
    }

    /**
     * Test view method
     * Should return a 404 http code.
     * @return void
     */
    public function testViewNotExists() {

        $this->get("api/v1/relationshipsSoftwares/view/99.json");

        $this->assertResponseCode(404);
    }

    /**
     * Test add method
     * Should known as an admin user
     * @return void
     */
    public function testAddAsAdmin() {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);

        $relationship = [
            "software_id" => 1,
            "recipient_id" =>2,
            "relationship_id" => 4,
        ];

        $this->post("api/v1/relationshipsSoftwares.json",$relationship);
        $this->assertResponseCode(302); // if success then redirect

        $this->RelationshipsSoftwares->exists($relationship);

    }


    /**
     * Test add method
     * Should known as an administration user
     * @return void
     */
    public function testAddAsAdministrationUser() {

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                    'role'=>"User",
                ]
            ]
        ]);

        $relationship = [
            "software_id" => 1,
            "recipient_id" =>2,
            "relationship_id" => 4,
        ];

        $this->post("api/v1/relationshipsSoftwares.json",$relationship);
        $this->assertResponseCode(302); // If success then redirect

        $this->RelationshipsSoftwares->exists($relationship);
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditAsAdmin() {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);

        $relationship = [
            "software_id" => 1,
            "recipient_id" =>3,
            "relationship_id" => 2,
        ];

        $this->put("api/v1/relationshipsSoftwares/edit/1.json",$relationship);
        $this->assertResponseOk();

        $this->RelationshipsSoftwares->exists($relationship);
        $this->assertCount(2,$this->RelationshipsSoftwares->find("list")->toArray());

    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAddAsUnknownUser() {

        $relationship = [
            "software_id" => 1,
            "recipient_id" =>2,
            "relationship_id" => 4,
        ];
        $this->post("api/v1/relationshipsSoftwares/add.json",$relationship);
        $this->assertResponseCode(302);
        $this->assertFalse($this->RelationshipsSoftwares->exists($relationship));

    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEditAsUnknownUser() {

        $relationship = [
            "software_id" => 1,
            "relationship_id" => 2,
            "recipient_id" =>3,
        ];

        $this->put("api/v1/relationshipsSoftwares/edit/1.json",$relationship);
        $this->assertResponseCode(302);
        $this->assertFalse($this->RelationshipsSoftwares->exists($relationship));
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteAsAdminUser() {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 1,
                    "user_type_id" => 1,
                    'role'=>"admin",
                ]
            ]
        ]);
        $this->delete("api/v1/relationshipsSoftwares/delete/2.json");
        $this->assertResponseOk();

        $this->assertFalse(
            $this->RelationshipsSoftwares->exists(["id"=>2])
        );
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteAsAdministrationUser() {
        $this->session([
            'Auth' => [
                'User' => [
                    'id' => 3,
                    "user_type_id" => 3,
                    'role'=>"User",
                ]
            ]
        ]);

        $this->delete("api/v1/relationshipsSoftwares/delete/2.json");
        $this->assertResponseOk();

        $this->assertFalse(
            $this->RelationshipsSoftwares->exists(["id"=>2])
        );
    }


    /**
     * Test delete method
     *
     * @return void
     */
    public function testDeleteAsUnknownUser() {

        $this->delete("api/v1/relationshipsSoftwares/delete/2.json");
        $this->assertResponseCode(302);

        $this->assertTrue(
            $this->RelationshipsSoftwares->exists(["id"=>2])
        );

    }
}

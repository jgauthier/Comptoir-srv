<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\TagsController;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestCase;


/**
 * App\Controller\TagsController Test Case
 */
class TagsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags',
        'app.softwares',
        'app.softwares_tags',
        'app.licenses',
        'app.licence_types',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',

    ];

    /**
     * Test index method of Tags controller. Show us all tags existing. If no one is missing test is Ok.
     *
     * @param : '$expected' is the array expect on result.
     * @return void
     */
    public function testIndex()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);
        $this->get('/api/v1/tags/');

        $expected = [
            "tags" =>
                [
                    [
                        "used_tag_number"=> 1,
                        "id" => 5,
                        "name" => "adullact",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 1
                    ],

                    [
                        "used_tag_number"=> 2,
                        "id" => 2,
                        "name" => "pizza",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 2
                    ],
                    [
                        "used_tag_number"=> 1,
                        "id" => 3,
                        "name" => "soleil",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 1
                    ],
                    [
                        "used_tag_number"=> 2,
                        "id" => 1,
                        "name" => "totoro",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 2
                    ]
                ]
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test view method of Tags controller. Show us all data of one tag, here tag id=4.
     *
     * @param : '$expected' is the array expect on result.
     * @return void
     */
    public function testView()
    {
        $this->get('/api/v1/tags/4.json');

        $expected = [
            "tag" =>
                ["id" => 4, "name" => "tortue", "created" => null, "modified" => null, "softwares" => [], "usedTagNumber" => 0],
        ];
        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method of Tags controller, with new tag "christian" but without Software relationship.
     * If 'christian' is found in result, test is OK.
     *
     * @return void
     */
    public function testAddTagWithoutSoftware()
    {
        $data = [
            'name' => "christian",
            'created' => null,
            'modified' => null,
        ];
        $this->post('/api/v1/tags', $data);

        $this->assertResponseSuccess();
        $tag = TableRegistry::get('Tags');
        $query = $tag->find()->where(['name' => $data['name']]);
        $this->assertEquals(1, $query->count());
    }

    /**
     * Test edit method of Tags controller, with name modification of tag id=4 (adullact -> Libriciel).
     * If the new tag is found, test is OK.
     *
     * @param : '$expected' is data of the new tag we expect to found.
     * @return void
     */
    public function testEdit()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $change = [
            'name' => 'Libriciel',
            'modified' => "2017-01-23T14:32:40+00:00",
        ];

        $this->put('/api/v1/tags/4.json', $change);
        $this->get('/api/v1/tags/4.json');

        $expected =
            [
                "tag" =>
                    [
                        "id" => 4,
                        "name" => "Libriciel",
                        "created" => null,
                        "modified" => "2017-01-23T14:32:40+00:00",
                        "softwares" => [],
                        "usedTagNumber" => 0
                    ]
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test edit method of Tags controller, with adding a new relationship with a soft of tag id=5
     *
     * @param : $addData' is data added.
     *          '$expected' is the array we expect on result.
     * @return void
     */
    public function testEditAddRelationshipWithSoftware()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $addData = [
            "software" => [
                "id" => 1,
                "softwarename" => "Soft 1",
                "url_repository" => "git_url",
                "description" => "A description",
                "licence_id" => 1,
                "created" => null,
                "logo_directory" => "logo_directory",
                "photo" => "url_photo"
            ]
        ];

        $this->post('/api/v1/tags/5.json', $addData);
        $this->get('/api/v1/tags/5.json');

        $expected =
            [
                "tag" => [
                    "id" => 5,
                    "name" => "adullact",
                    "created" => null,
                    "modified" => null,

                    "softwares" => [
                        [
                            "id" => 1,
                            "softwarename" => "Asalae",
                            "url_repository" => "http://www.fake-repo-asalae.git",
                            "description" => "Asalae description",
                            "licence_id" => 1,
                            "created" => "2017-02-14T15:02:48+00:00",
                            "modified" => "2017-02-14T15:02:48+00:00",
                            "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                            "photo" => "correctSoftwareLogo.jpg",
                            "url_website" => "http://www.fake-asalae.com",
                            "tags" => [
                                [
                                    "id" => 1,
                                    "name" => "totoro",
                                    "created" => null,
                                    "modified" => null,
                                    "usedTagNumber" => 0
                                ],
                                [
                                    "id" => 5,
                                    "name" => "adullact",
                                    "created" => null,
                                    "modified" => null,
                                    "usedTagNumber" => 0
                                ]
                            ],
                            "average_review" => 0,
                            "tag_string" => "totoro adullact"
                        ]
                    ],
                    "usedTagNumber" => 1
                ]
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test delete method of Tags controller, for tags id=4 without relationship with any Software.
     * Tag id=4 and his data must be not found as result.
     *
     * @param : '$expected' is the array FROM tags table we finally expect. It's compare to $this->assertEquals. If no differences, test is OK.
     * @return void
     */
    public function testDeleteIfNoRelationshipBetweenSoftwareAndTag()
    {
        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);

        $this->delete('/api/v1/tags/4');

        $this->get('/api/v1/tags.json');

        $expected = [
            "tags" =>
                [
                    [
                        "used_tag_number"=> 1,
                        "id" => 5,
                        "name" => "adullact",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 1
                    ],
                    [
                        "used_tag_number"=> 2,
                        "id" => 2,
                        "name" => "pizza",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            ["id" => 2,
                                "softwarename" => "Lutèce",
                                "url_repository" => "http://www.fake-repo-lutece.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id2/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-lutece.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 2
                    ],
                    [
                        "used_tag_number"=> 1,
                        "id" => 3,
                        "name" => "soleil",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            ["id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 1
                    ],
                    [
                        "used_tag_number"=> 2,
                        "id" => 1,
                        "name" => "totoro",
                        "created" => null,
                        "modified" => null,
                        "softwares" => [
                            [
                                "id" => 1,
                                "softwarename" => "Asalae",
                                "url_repository" => "http://www.fake-repo-asalae.git",
                                "description" => "Asalae description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id1/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-asalae.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ],
                            [
                                "id" => 3,
                                "softwarename" => "Soft 3",
                                "url_repository" => "http://www.fake-repo-soft3.git",
                                "description" => "Lutèce description",
                                "licence_id" => 1,
                                "created" => "2017-02-14T15:02:48+00:00",
                                "modified" => "2017-02-14T15:02:48+00:00",
                                "logo_directory" => TESTS . "TestFiles" . DS . "Softwares/photo/id3/avatar",
                                "photo" => "correctSoftwareLogo.jpg",
                                "url_website" => "http://www.fake-soft3.com",
                                "average_review" => 0,
                                "tag_string" => ""
                            ]],
                        "usedTagNumber" => 2
                    ],
                ]
        ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);

        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test delete method of Tags controller, for tag id=5 linked to software id="1"
     * Tag id=5 and his data must be NOT found as result.
     *
     * @return void
     */
    public function testDeleteTagLinkedToSoftware()
    {
        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);

        $this->delete('/api/v1/tags/5');

        $this->get('/api/v1/softwares-tags.json');

        $this->assertNotContains('"tag_id": 5', $this->_response->body());
        $this->assertResponseOk();
    }


    /**
     * Test add method from Tags controller, if someone really clever try to create a Tag longer than 100 value.
     * If "Provided value is too long (max : 100)." is returned, everything is under controll.
     *
     * @return void
     */
    public function testCreateTagTooLong()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => 'AAAAAAAAAAAAAAaaaaaaaaaaaaaaaaaaaCCCCCCCCccccccccccccccccccccccccccccDDDDDDDDDDDDDDDDddddddddddddddCCCCCCCCCCCCCCCcccccccccccccccccccccccccc',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["maxLength" => "Provided value is too long (max : 100)."]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }

    /**
     * Test add method from Tags controller, if someone try to set any value out of "A~Z a~z 0~9 - _",
     * this error "Name must be an alphaNumeric value. You provided an invalid value." welcome him.
     *
     * @return void
     */
    public function testCreateTagWithForbiddenValue()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => 'ààààà#çççéééé',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["alphaNumeric" => "Name must be an alphaNumeric value. You provided an invalid value."]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test add method from Tags controller, if someone try, God only knows why, to create a Tag without name.
     *
     * @return void
     */
    public function testCreateTagWithoutValue()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => '',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["_empty" => "This field cannot be left empty"]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }


    /**
     * Test add method from Tags controller, if someone try to create a new Tag, but the name is already registered.
     *
     * @return void
     */
    public function testCreateTagWhichAlreadyExist()
    {
        $this->configRequest(['headers' => ['Accept' => 'application/json']]);

        $data = [
            'name' => 'totoro',
        ];

        $this->post('/api/v1/tags.json', $data);

        $expected =
            [
                "tag" =>
                    [
                        "name" => ["_isUnique" => "A tag with this name already exists in Comptoir du libre."]
                    ],
                "message" => "Error"
            ];

        $expected = json_encode($expected, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $this->_response->body());
    }
    /**
     * - Peut-être suis-je vieux et fatigué mais je crois que les chances de savoir ce qui se passe réellement sont si ridiculement minces que la seule chose à faire
     * c'est de renoncer à chercher et de chercher à s'occuper. Je préfère mille fois être heureux qu'être dans le vrai.
     * - Et vous êtes heureux ?
     * - Euh... Non. C'est là que la théorie s'effondre bien sûr.
     *
     * cf H2G2
     */

}

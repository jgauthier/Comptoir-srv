<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RelationshipsSoftwaresUsersController;


/**
 * App\Controller\RelationshipsSoftwaresUsersController Test Case
 */
class RelationshipsSoftwaresUsersControllerTest extends ApiIntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_softwares_users',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
//        'app.reviews',
        'app.users',
        'app.user_types',
//        'app.screenshots',
        'app.relationships',
        'app.relationship_types',
//        'app.relationships_softwares',
//        'app.relationships_users',
//
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView() {
        $this->markTestIncomplete('Not implemented yet.');
    }



//    /**
//     * Test edit method
//     * Edit the software ID and the relationships id
//     * @return void
//     */
//    public function testEdit()
//    {
//        $data =
//           [
//            "software_id"=> 2,
//            "user_id"=> 1,
//            "relationship_id"=> 2,
//            "created"=> "2016-04-15T09:22:32+0000",
//            "modified"=> "2016-04-15T09:22:32+0000",
//
//            ];
//
//        $this->put('api/v1/RelationshipsSoftwaresUsers/edit/1.json',$data);
//
//        // Vérifie que le code de réponse est 200
//        $this->assertResponseOk();
//
//        $expected = [
//            'message' =>  "Success",
//            'relationshipsSoftwaresUser' => [
//                'id' =>1,
//                "software_id"=> 2,
//                "user_id"=> 1,
//                "relationship_id"=> 2,
//                "created"=> "2016-04-15T09:22:32+0000",
//                "modified"=> "2016-04-15T09:22:32+0000",
//            ]];
//        $expected = json_encode($expected, JSON_PRETTY_PRINT);
//        $this->assertEquals($expected, $this->_response->body());
//    }
//
//    /**
//     * Test delete method
//     *
//     * @return void
//     */
//    public function testDelete()
//    {
//        $this->markTestIncomplete('Not implemented yet.');
//    }
//
//
//    /**
//     *
//     */
//    public function testgetUsersOfRelationshipsBySoftwareId(){
//        $this->configRequest([
//            'headers' => ['Accept' => 'application/json']
//        ]);
//
//        $result = $this->get('api/v1/RelationshipsSoftwaresUsers/getUsersOfRelationshipsBySoftwareId/2.json');
//
//        // Vérifie que le code de réponse est 200
//        $this->assertResponseOk();
//
//
//        $expected =
//            [
//                "relationshipsSoftwaresUser"=> [
//                [
//                    "id"=> 4,
//                    "software_id"=> 2,
//                    "user_id"=> 4,
//                    "relationship_id"=> 7,
//                    "created"=> "2016-04-15T09=>22=>21+0000",
//                    "modified"=> "2016-04-15T09=>22=>21+0000",
//                    "user"=> [
//                        "id"=> 4,
//                        "username"=> "Centre de Gestion de la Vend\u00e9e (CDG85)",
//                        "created"=> "2016-04-15T09:19:09+0000",
//                        "logo_directory"=> null,
//                        "url"=> null,
//                        "user_type_id"=> 2,
//                        "description"=> null,
//                        "modified"=> "2016-04-15T09:19:09+0000",
//                        "role"=> "user",
//                        "password"=> "8ie9nxz5",
//                        "email"=> "",
//                        "photo"=> null
//                    ],
//                    "_matchingData"=> [
//                        "Relationships"=> [
//                            "id"=> 7,
//                            "name"=> "UserOf",
//                            "created"=> "2016-04-15T09:15:41+0000",
//                            "modified"=> "2016-04-15T09:15:41+0000",
//                            "cd"=> "UserOf",
//                            "relationship_type_id"=> 5
//                        ]
//                    ]
//                ],
//                [
//                    "id"=> 5,
//                    "software_id"=> 2,
//                    "user_id"=> 5,
//                    "relationship_id"=> 7,
//                    "created"=> "2016-04-15T09:22:32+0000",
//                    "modified"=> "2016-04-15T09:22:32+0000",
//                    "user"=> [
//                        "id"=> 5,
//                        "username"=> "Centre de Gestion de l\u0027Is\u00e8re (CDG38)",
//                        "created"=> "2016-04-15T09:20:14+0000",
//                        "logo_directory"=> null,
//                        "url"=> null,
//                        "user_type_id"=> 2,
//                        "description"=> null,
//                        "modified"=> "2016-04-15T09:20:14+0000",
//                        "role"=> "user",
//                        "password"=> "8ie9nxz5",
//                        "email"=> "contact@cgdg38.fr",
//                        "photo"=> null
//                    ],
//                    "_matchingData"=> [
//                        "Relationships"=> [
//                            "id"=> 7,
//                            "name"=> "UserOf",
//                            "created"=> "2016-04-15T09:15:41+0000",
//                            "modified"=> "2016-04-15T09:15:41+0000",
//                            "cd"=> "UserOf",
//                            "relationship_type_id"=> 5
//                        ]
//                    ]
//                ],
//                    "id"=> "2"
//                ]
//            ];
//        $expected = json_encode($expected, JSON_PRETTY_PRINT);
//        $this->assertEquals($expected, $this->_response->body());
//    }
//
//    public function testGetRelationshipsByUserId(){
//
//
//        $this->configRequest([
//            'headers' => ['Accept' => 'application/json']
//        ]);
//
//        $result = $this->get('api/v1/RelationshipsSoftwaresUsers/getRelationshipsByUserId.json?id=4&filter=CreatorOf');
//
//        // Vérifie que le code de réponse est 200
//        $this->assertResponseOk();
//
//
//
//        $expected = [
//    "relationshipsSoftwaresUsers"=> [
//        [
//            "id"=> 7,
//            "software_id"=> 2,
//            "user_id"=> 6,
//            "relationship_id"=> 3,
//            "created"=> "2016-04-18T13:39:25+0000",
//            "modified"=> "2016-04-18T13:39:25+0000",
//            "relationship"=> [
//                "id"=> 3,
//                "name"=> "CreatorOf",
//                "created"=> "2016-04-11T09:50:07+0000",
//                "modified"=> "2016-04-11T09:50:07+0000",
//                "cd"=> "CreatorOf",
//                "relationship_type_id"=> 5
//            ],
//            "software"=> [
//                "id"=> 2,
//                "softwarename"=> "Pastell",
//                "url_repository"=> "https:\/\/scm.adullact.net\/anonscm\/svn\/pastell\/trunk",
//                "description"=> "Pastell est une plateforme d\u0027\u00e9change de donn\u00e9es s\u00e9curis\u00e9e assurant des fonctions =>\r\n- de routages de documents\r\n- de pilotage de flux\r\n- de cr\u00e9ation et de saisie de formulaires\r\n- connexion au TDT",
//                "licence_id"=> 5,
//                "created"=> "2016-04-08T15:55:11+0000",
//                "modified"=> "2016-04-13T15:37:40+0000",
//                "logo_directory"=> "webroot\/files\/Softwares\/2\/photo\/avatar",
//                "photo"=> "logo.jpg"
//            ],
//            "user"=> [
//                "id"=> 6,
//                "username"=> "Adullact Projet",
//                "created"=> "2016-04-18T09:16:10+0000",
//                "logo_directory"=> null,
//                "url"=> null,
//                "user_type_id"=> 2,
//                "description"=> null,
//                "modified"=> "2016-04-18T09:16:10+0000",
//                "role"=> "user",
//                "password"=> "8ie9nxz5",
//                "email"=> "contact@adullact-projet.coop",
//                "photo"=> null
//            ]
//        ],
//        [
//            "id"=> 9,
//            "software_id"=> 4,
//            "user_id"=> 6,
//            "relationship_id"=> 3,
//            "created"=> "2016-05-30T15:02:22+0000",
//            "modified"=> "2016-05-30T15:02:22+0000",
//            "relationship"=> [
//                "id"=> 3,
//                "name"=> "CreatorOf",
//                "created"=> "2016-04-11T09:50:07+0000",
//                "modified"=> "2016-04-11T09:50:07+0000",
//                "cd"=> "CreatorOf",
//                "relationship_type_id"=> 5
//            ],
//            "software"=> [
//                "id"=> 4,
//                "softwarename"=> "i-Parapheur - serveur",
//                "url_repository"=> "https:\/\/scm.adullact.net\/anonscm\/svn\/paraphelec\/trunk",
//                "description"=> "Parapheur \u00e9lectronique (i-Parapheur) de l\u0027ADULLACT. Le GTC (Groupe de Travail Collaboratif) d\u00e9di\u00e9 fait vivre la feuille de route du projet.\r\nR\u00e9alis\u00e9 par Adullact-Projet et ses partenaires, bas\u00e9 sur Alfresco (http:\/\/www.alfresco.com\/fr\/).",
//                "licence_id"=> 5,
//                "created"=> "2016-04-13T09:41:07+0000",
//                "modified"=> "2016-04-13T09:42:49+0000",
//                "logo_directory"=> "webroot\/files\/Softwares\/4\/photo\/avatar",
//                "photo"=> "logo.jpg"
//            ],
//            "user"=> [
//                "id"=> 6,
//                "username"=> "Adullact Projet",
//                "created"=> "2016-04-18T09:16:10+0000",
//                "logo_directory"=> null,
//                "url"=> null,
//                "user_type_id"=> 2,
//                "description"=> null,
//                "modified"=> "2016-04-18T09:16:10+0000",
//                "role"=> "user",
//                "password"=> "8ie9nxz5",
//                "email"=> "contact@adullact-projet.coop",
//                "photo"=> null
//            ]
//        ],
//        [
//            "id"=> 10,
//            "software_id"=> 15,
//            "user_id"=> 6,
//            "relationship_id"=> 3,
//            "created"=> "2016-05-30T15:02:33+0000",
//            "modified"=> "2016-05-30T15:02:33+0000",
//            "relationship"=> [
//                "id"=> 3,
//                "name"=> "CreatorOf",
//                "created"=> "2016-04-11T09:50:07+0000",
//                "modified"=> "2016-04-11T09:50:07+0000",
//                "cd"=> "CreatorOf",
//                "relationship_type_id"=> 5
//            ],
//            "software"=> [
//                "id"=> 15,
//                "softwarename"=> "i-delibRE",
//                "url_repository"=> "svn:\/\/scm.adullact.net\/svn\/idelibre\/trunk",
//                "description"=> "Le projet i-delibRE est le porte-document nomade des \u00e9lus pour le suivi des s\u00e9ances d\u00e9lib\u00e9rantes de la collectivit\u00e9.",
//                "licence_id"=> 5,
//                "created"=> "2016-04-28T12:33:12+0000",
//                "modified"=> "2016-04-28T12:33:12+0000",
//                "logo_directory"=> null,
//                "photo"=> null
//            ],
//            "user"=> [
//                "id"=> 6,
//                "username"=> "Adullact Projet",
//                "created"=> "2016-04-18T09:16:10+0000",
//                "logo_directory"=> null,
//                "url"=> null,
//                "user_type_id"=> 2,
//                "description"=> null,
//                "modified"=> "2016-04-18T09:16:10+0000",
//                "role"=> "user",
//                "password"=> "8ie9nxz5",
//                "email"=> "contact@adullact-projet.coop",
//                "photo"=> null
//            ]
//        ]
//    ],
//    "paging"=> [
//        "RelationshipsSoftwaresUsers"=> [
//            "finder"=> "all",
//            "page"=> 1,
//            "current"=> 3,
//            "count"=> 3,
//            "perPage"=> 20,
//            "prevPage"=> false,
//            "nextPage"=> false,
//            "pageCount"=> 1,
//            "sort"=> null,
//            "direction"=> false,
//            "limit"=> null,
//            "sortDefault"=> false,
//            "directionDefault"=> false
//        ]
//    ]
//];
//        $expected = json_encode($expected, JSON_PRETTY_PRINT);
//        $this->assertEquals($expected, $this->_response->body());
//
//    }
//
//
//    public function testGetServicesProviders(){
//
//
//        $this->configRequest([
//            'headers' => ['Accept' => 'application/json']
//        ]);
//
//        $this->get('api/v1/RelationshipsSoftwaresUsers/getServicesProviders.json?Relationships=CreatorOf');
//
//        // Vérifie que le code de réponse est 200
//        $this->assertResponseOk();
//
//        $expected = [
//            "relationshipsSoftwaresUsers"=> [
//                "message" => "Success",
//                "Relationships"=> [
//                    [
//                        "id"=> 1,
//                        "software_id"=> 2,
//                        "user_id"=> 1,
//                        "relationship_id"=> 1,
//                        "created"=> "2016-04-15T09:22:21+0000",
//                        "modified"=> "2016-04-15T09:22:21+0000",
//                        "relationship"=> [
//                            "id"=> 1,
//                            "name"=> "CreatorOf",
//                            "created"=> "2016-04-08T15:01:09+0000",
//                            "modified"=> "2016-04-08T15:01:09+0000",
//                            "cd"=> "CreatorOf",
//                            "relationship_type_id"=> 1
//                        ],
//                        "user"=> [
//                            "id"=> 1,
//                            "username"=> "A test name ",
//                            "created"=> null,
//                            "logo_directory"=> "",
//                            "url"=> "url",
//                            "user_type_id"=> 1,
//                            "description"=> "A description",
//                            "modified"=> "2016-06-16T12:08:10+0000",
//                            "role"=> "Admin",
//                            "password"=> "passwd",
//                            "email"=> "name2@adullact.org",
//                            "photo"=> "url"
//                        ]
//                    ]
//                ]
//            ]
//    ];
//
//
//        $expected = json_encode($expected, JSON_PRETTY_PRINT);
//        $this->assertEquals($expected, $this->_response->body());
//    }
}

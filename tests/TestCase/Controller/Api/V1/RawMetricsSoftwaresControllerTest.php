<?php

namespace App\Test\TestCase\Controller\Api\V1;

use App\Controller\RawMetricsSoftwaresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\RawMetricsSoftwaresController Test Case
 */
class RawMetricsSoftwaresControllerTest extends IntegrationTestCase {

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.licence_types',
        'app.licenses',
        'app.softwares',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships_softwares',
        'app.relationships',
        'app.relationship_types',
        'app.screenshots',
        'app.softwares_statistics',
        'app.relationships_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit() {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete() {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SoftwaresTagsTable Test Case
 */
class SoftwaresTagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SoftwaresTagsTable
     */
    public $SoftwaresTags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.softwares_tags',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
        'app.usedsoftwares',
        'app.backedsoftwares',
        'app.createdsoftwares',
        'app.contributionssoftwares',
        'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.tags',
        'app.userssoftwares',
        'app.backerssoftwares',
        'app.creatorssoftwares',
        'app.contributorssoftwares',
        'app.providerssoftwares',
        'app.workswellsoftwares',
        'app.alternativeto'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SoftwaresTags') ? [] : ['className' => 'App\Model\Table\SoftwaresTagsTable'];
        $this->SoftwaresTags = TableRegistry::get('SoftwaresTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SoftwaresTags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;


use Cake\Auth\DigestAuthenticate;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.user_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'App\Model\Table\UsersTable'];
        $this->UsersTable = TableRegistry::get('Users', $config);

        $this->digestAdmin = DigestAuthenticate::password("fake-email-admin@comptoir-du-libre.org", "fake-password", 'srv.comptoir-du-libre.org');
        $this->digestCompany = DigestAuthenticate::password("fake-email-company@comptoir-du-libre.org", "fake-password", 'srv.comptoir-du-libre.org');
        $this->digestAdministration = DigestAuthenticate::password("fake-email-administration@comptoir-du-libre.org", "fake-password", 'srv.comptoir-du-libre.org');
        $this->digestPerson = DigestAuthenticate::password("fake-email-person@comptoir-du-libre.org", "fake-password", 'srv.comptoir-du-libre.org');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test Find by type when type is specified
     */
    public function testFindByTypeAdministration()
    {
        $usersByTypeQuery = $this->UsersTable->find("byType", ['user_type' => 'Administration']);
        $this->assertInstanceOf('Cake\ORM\Query', $usersByTypeQuery);
        $usersBytype = $usersByTypeQuery->hydrate(false)->toArray();

        $expected = [
            [
                "id" => 3,
                "username" => "Administration",
                "created" => Time::createFromTimestamp(1487084570),
                "logo_directory" => TESTS . "TestFiles" . DS . "Users/photo/id3/avatar",
                "url" => "http://www.avencall.com/",
                "user_type_id" => 3,
                "description" => "description",
                "modified" => Time::createFromTimestamp(1487084570),
                "role" => "user",
                "password" => "fake-password",
                "email" => "fake-email-administration@comptoir-du-libre.org",
                "photo" => "correctUserAvatar.jpg",
                "active" => false,
                "digest_hash" => $this->digestAdministration,
                "token" => null,
                "user_type" => [
                    'id' => 3,
                    'name' => 'Administration',
                    'created' => Time::createFromTimestamp(1487084570),
                    'modified' => Time::createFromTimestamp(1487084570),
                    'cd' => 'Administration'
                ],
            ]];

        $this->assertEquals($expected, $usersBytype);
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StatisticsAveragesTable Test Case
 */
class StatisticsAveragesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StatisticsAveragesTable
     */
    public $StatisticsAverages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.statistics_averages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StatisticsAverages') ? [] : ['className' => 'App\Model\Table\StatisticsAveragesTable'];
        $this->StatisticsAverages = TableRegistry::get('StatisticsAverages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StatisticsAverages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

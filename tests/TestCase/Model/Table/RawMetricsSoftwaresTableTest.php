<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RawMetricsSoftwaresTable Test Case
 */
class RawMetricsSoftwaresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RawMetricsSoftwaresTable
     */
    public $RawMetricsSoftwares;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.raw_metrics_softwares',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships_softwares',
        'app.relationships',
        'app.relationship_types',
        'app.screenshots',
        'app.softwares_statistics',
        'app.relationships_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RawMetricsSoftwares') ? [] : ['className' => 'App\Model\Table\RawMetricsSoftwaresTable'];
        $this->RawMetricsSoftwares = TableRegistry::get('RawMetricsSoftwares', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RawMetricsSoftwares);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

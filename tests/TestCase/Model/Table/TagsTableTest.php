<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TagsTable Test Case
 */
class TagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TagsTable
     */
    public $Tags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.raw_metrics_softwares',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.reviews',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares',
        'app.relationships_users',
        'app.usedsoftwares',
        'app.backedsoftwares',
        'app.createdsoftwares',
        'app.contributionssoftwares',
        'app.providerforsoftwares',
        'app.screenshots',
        'app.softwares_statistics',
        'app.userssoftwares',
        'app.backerssoftwares',
        'app.creatorssoftwares',
        'app.contributorssoftwares',
        'app.providerssoftwares',
        'app.workswellsoftwares',
        'app.alternativeto',
        'app.softwares_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Tags') ? [] : ['className' => 'App\Model\Table\TagsTable'];
        $this->Tags = TableRegistry::get('Tags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Tags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

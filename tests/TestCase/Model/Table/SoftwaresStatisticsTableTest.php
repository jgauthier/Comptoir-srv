<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SoftwaresStatisticsTable Test Case
 */
class SoftwaresStatisticsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SoftwaresStatisticsTable
     */
    public $SoftwaresStatistics;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.softwares_statistics',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.reviews',
        'app.users',
        'app.user_types',
        'app.relationships_softwares',
        'app.relationships',
        'app.relationship_types',
        'app.screenshots',
        'app.relationships_softwares_users',
        'app.relationships_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SoftwaresStatistics') ? [] : ['className' => 'App\Model\Table\SoftwaresStatisticsTable'];
        $this->SoftwaresStatistics = TableRegistry::get('SoftwaresStatistics', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SoftwaresStatistics);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

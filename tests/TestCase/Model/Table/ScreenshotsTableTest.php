<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScreenshotsTable Test Case
 */
class ScreenshotsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ScreenshotsTable
     */
    public $Screenshots;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.screenshots',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.user_types',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Screenshots') ? [] : ['className' => 'App\Model\Table\ScreenshotsTable'];
        $this->Screenshots = TableRegistry::get('Screenshots', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Screenshots);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

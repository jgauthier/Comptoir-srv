<?php
namespace App\Test\TestCase\Model\Table;


use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RelationshipsSoftwaresTable Test Case
 */
class RelationshipsSoftwaresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RelationshipsSoftwaresTable
     */
    public $RelationshipsSoftwares;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.relationships_softwares',
        'app.softwares',
        'app.licenses',
        'app.licence_types',
        'app.relationships',
        'app.relationship_types',
        'app.relationships_softwares_users',
        'app.users',
        'app.user_types',
        'app.relationships_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('RelationshipsSoftwares') ? [] : ['className' => 'App\Model\Table\RelationshipsSoftwaresTable'];
        $this->RelationshipsSoftwares = TableRegistry::get('RelationshipsSoftwares', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RelationshipsSoftwares);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method when try to declare a relationship twice
     * Should return an error on software_id
     * @return void
     */
    public function testBuildRulesRelationshipBetweenTwoSoftwaresCantBeDeclaredTwice()
    {
        $this->RelationshipsSoftwares->save($this->RelationshipsSoftwares->newEntity([
            'software_id' => 1,
            'recipient_id' =>1,
            'relationship_id'=>3,
        ]));

        $worksWellWith = $this->RelationshipsSoftwares->newEntity([
            'software_id' => 1,
            'recipient_id' =>1,
            'relationship_id'=>3,
        ]);

        $result = $this->RelationshipsSoftwares->checkRules($worksWellWith);
        $this->assertFalse($result);

        $expectedError = [
            'software_id' => [
                '_isUnique' => 'You can not declare twice the same relationships.'
            ]
        ];

        $this->assertEquals($expectedError,$worksWellWith->errors());
    }

    /**
     * Test buildRules method when try to declare a relationship with a software not exists in the softwares table
     * Should return an error on software_id => _existsIn and recipient_id => _existsIn
     * @return void
     */
    public function testBuildRulesRelationshipWhenSoftwareNotExists()
    {
        $relationship = $this->RelationshipsSoftwares->newEntity([
            'software_id' => 999,
            'recipient_id' =>999,
            'relationship_id'=>3,
        ]);

        $result = $this->RelationshipsSoftwares->checkRules($relationship);
        $this->assertFalse($result);

        $expectedError = [
            'software_id' => [
                '_existsIn' => 'This value does not exist'
            ],
            'recipient_id' => [
                '_existsIn' => 'This value does not exist'
            ]
        ];

        $this->assertEquals($expectedError,$relationship->errors());
    }


    /**
     * Test buildRules method when try to declare a relationship with a relationship not exists in the relationships table
     * Should return an error on relationship_id => _existsIn
     * @return void
     */
    public function testBuildRulesRelationshipWhenRelationshipNotExists()
    {
        $relationship = $this->RelationshipsSoftwares->newEntity([
            'software_id' => 1,
            'recipient_id' =>2,
            'relationship_id'=>999,
        ]);

        $result = $this->RelationshipsSoftwares->checkRules($relationship);
        $this->assertFalse($result);

        $expectedError = [
            'relationship_id' => [
                '_existsIn' => 'This value does not exist'
            ]
        ];

        $this->assertEquals($expectedError,$relationship->errors());
    }

    /**
     * Test buildRules method when declare a relationship with good data
     * Should return assert True
     * @return void
     */
    public function testBuildRulesAddARelationship()
    {
        $relationship = $this->RelationshipsSoftwares->newEntity([
            'software_id' => 1,
            'recipient_id' =>2,
            'relationship_id'=>3,
        ]);

        $result = $this->RelationshipsSoftwares->checkRules($relationship);
        $this->assertTrue($result);


    }


}
